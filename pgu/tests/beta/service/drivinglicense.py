#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.driver.engine import DriverEngine
from pgu.tests.conftest import step, testcase, skipif, issue, feature, story

from pgu.pages.esia import EsiaHomePage
from pgu.pages.beta.core import BetaHomePage, CategoryPage, PersonalCabinetPage
from pgu.pages.beta.service.drivinglicense import ApplicationPage, ReceptionPage, Vars


@feature("Услуги")
@story("Водительское удостоверение")
@issue("PGU-2")
@testcase("http://pgu-uat-beta.test.gosuslugi.ru/10056/5/form")
class DrivingLicenceSanitySuite:
    """ Sanity-тестирование услуги "Получение водительского удостоверения" """

    @skipif(False, reason="Ошибка при открытии главной страницы портала ПГУ")
    def case_driving_license_positive(self, url: str, engine: DriverEngine, screenshot):

        with step("Переход на страницу ЕСИА - авторизация"):
            BetaHomePage.click_login_into_esia()

        with step("Выбираем авторизацию по логину и паролю"):
            if EsiaHomePage.page_error():
                raise AssertionError("Ошибка входа в ЕСИА")
            EsiaHomePage.Login(Vars.esia_email, Vars.esia_pass).by_email_or_phone()

        with step("Переход на сервис 'Получение ВУ'"):
            BetaHomePage.Goto.driving_license()
            ReceptionPage.goto_driver_license_claim_subservice()

        with step("Тип услуги: онлайн"):
            ReceptionPage.online_claim_type()

        with step("Новый черновик, если требуется"):
            ApplicationPage.create_new_draft()

        with step("Проверка на невидимость дисклеймеров 'Заявка подана' и 'Ведомство недоступно', иначе - падение кейса"):
            if ApplicationPage.form_error():
                raise AssertionError(ApplicationPage.form_error.ERROR_TEXT)
            if ApplicationPage.form_error_department():
                raise AssertionError(ApplicationPage.form_error_department.ERROR_TEXT)

        with step("Выбор категории ВУ"):
            ApplicationPage.driving_category(["A", "B", "C"])

        with step("Ввод номера телефона и домашнего адреса"):
            ApplicationPage.personal_data("70001234567")
            ApplicationPage.home_address_form(2, u"Москва Хуторская ")

        with step("Ввод сведений о автошколе, сертификате"):
            ApplicationPage.autoschool_info_and_certificate()
            ApplicationPage.medical_certificate([1, 2, 3])

        with step("Выбор адреса расположения госавтоинспекции на я.картах"):
            ApplicationPage.gosautoinspection_department_address()
            ApplicationPage.yamaps_setlocation()

        with step("Выбор времени и даты визита"):
            ApplicationPage.date_and_time_of_visit(time_select=None)

        with step("Подтверждение и отправка формы"):
            screenshot("Перед отправкой формы")
            ApplicationPage.agree_and_submit_form()

        with step("Подтверждение отправки формы в ведомство"):
            PersonalCabinetPage.AcceptedHeader.is_visible(wait=30)
            assert PersonalCabinetPage.AcceptedHeader.is_exists(), "Не удалось найти заголовок отправки заявления"
            PersonalCabinetPage.AcceptedHeader.get_text()
            assert PersonalCabinetPage.check_submittext_existence(), "Не удалось найти текст об отправке введомство"
            engine.refresh()
            assert PersonalCabinetPage.check_submittext_existence(), "Не удалось найти текст об отправке введомство"
