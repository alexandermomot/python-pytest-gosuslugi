#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.driver.engine import DriverEngine
from pgu.tests.conftest import step, testcase, skipif, issue, feature, story

from pgu.pages.esia import EsiaHomePage
from pgu.pages.beta.core import BetaHomePage, CategoryPage, PersonalCabinetPage
from pgu.pages.beta.service.domicleregistration import ReceptionPage, ApplicationPage, Vars


@feature("Услуги")
@story("Регистрация по месту жительства и пребывания")
@issue("PGU-3")
@testcase("http://pgu-uat-beta.test.gosuslugi.ru/10050/1/form")
class DomicileRegistraionSanitySuite:
    """ Sanity-тестирование услуги "Регистрация по месту жительства и пребывания" """

    @skipif(False, reason="Ошибка при открытии главной страницы портала ПГУ")
    def case_domicile_registration_positive(self, url: str, engine: DriverEngine, screenshot):

        with step("Авторизация в ЕСИА"):
            BetaHomePage.click_login_into_esia()
            if EsiaHomePage.page_error():
                raise AssertionError("Esia login failed")
            EsiaHomePage.Login(Vars.esia_email, Vars.esia_pass).by_email_or_phone().as_person()

        with step("Выбираем тип услуги"):
            BetaHomePage.CategoryBottomBtn.click()
            CategoryPage.Goto.service_by_name("Регистрация граждан")
            ReceptionPage.choose_option_type()

        with step("Выбираем онлайн заявку"):
            ReceptionPage.online_claim_type()

        with step("Новый черновик"):
            ApplicationPage.create_new_draft()

        with step("Проверка появления главного локатора формы услуги, если не появилась - падение кейса"):
            if ApplicationPage.form_error():
                assert ApplicationPage.form_error.ERROR_TEXT
            if ApplicationPage.form_error_department():
                raise AssertionError(ApplicationPage.form_error_department.ERROR_TEXT)

        with step("Вводим номер телефона"):
            ApplicationPage.personal_data()

        with step("Вводим место рождения"):
            ApplicationPage.Step3_BirthCityInput.send_keys_by_xpath(u"Москва")
            ApplicationPage.Step3_DisctrictInput.send_keys_by_xpath(u"Новогиреево")
            ApplicationPage.Step3_RegionInput.send_keys_by_xpath(u"Москва и область")
            ApplicationPage.Step3_PassportData_Country_ClickZone.click()
            ApplicationPage.Step3_PassportData_Country_InputZone.clear_backspace()
            ApplicationPage.Step3_PassportData_Country_InputZone.send_keys_by_xpath(u"Россия")
            ApplicationPage.Step3_PassportData_Country_ListElement.click()

        with step("Вводим информацию о о рагистрации по месту жительства"):
            ApplicationPage.Step4_OwnHouseRegistration.click()

        with step("Вводим новый адрес регистрации"):
            ApplicationPage.Step5_NewRegistrationAddress.click()
            ApplicationPage.Step5_NewRegistrationAddress.send_keys_by_xpath(u"Москва Хуторская ")
            ApplicationPage.Step5_NewRegistrationAddress_Select.click()
            ApplicationPage.Step5_NewRegistrationAddress_AdditionalStreet.send_keys_by_xpath(u"Мирлэнд")
            ApplicationPage.Step5_NewRegistrationAddress_House.send_keys_by_xpath(u"5")
            ApplicationPage.Step5_NewRegistrationAddress_Apartment.send_keys_by_xpath(u"38")
            ApplicationPage.Step5_NewRegistrationAddress_Index.clear()
            ApplicationPage.Step5_NewRegistrationAddress_Index.send_keys_by_xpath("222333")

        with step("Вводим сведения об основании для проживания"):
            ApplicationPage.Step9_LegalBasisOfLiving_Select.click()
            ApplicationPage.Step9_LegalBasisOfLiving_Option.click()
            ApplicationPage.Step9_LegalBasisOfLiving_Series.send_keys_by_xpath("77")
            ApplicationPage.Step9_LegalBasisOfLiving_Number.send_keys_by_xpath("23812611")
            ApplicationPage.Step9_LegalBasisOfLiving_IssueDate.send_keys_by_xpath("20112011")
            ApplicationPage.Step9_LegalBasisOfLiving_Issuer.send_keys_by_xpath(u"ОВД")

        with step("Вводим причину переезда, место работы, должность и т.д."):
            ApplicationPage.Step11_ReasonOfChange.click()
            ApplicationPage.Step11_ReasonOfChangeList.click()
            ApplicationPage.Step11_CarrerTypeOfJob.click()
            ApplicationPage.Step11_CarrerTypeList.click()
            ApplicationPage.Step11_TypeOfJob.click()
            ApplicationPage.Step11_TypeOfJobList.click()
            ApplicationPage.Step11_Socialinsurance.click()
            ApplicationPage.Step11_SocialinsuranceList.click()
            ApplicationPage.Step11_EducationLevel.click()
            ApplicationPage.Step11_EducationLevelList.click()
            ApplicationPage.Step11_MarriedState.click()
            ApplicationPage.Step11_MarriedStateList.click()

        with step("Центр выдачи документов, ввод адреса"):
            ApplicationPage.Step13_DocumentsApplyAddress.clear()
            ApplicationPage.Step13_DocumentsApplyAddress.send_keys_by_xpath(u"Москва Хуторская ")
            ApplicationPage.Step13_DocumentsApplyAddress_Select.click()
            ApplicationPage.Step13_DocumentsApplyAddress_AdditionalStreet.send_keys_by_xpath(u"Мирлэнд")
            ApplicationPage.Step13_DocumentsApplyAddress_Apartment.send_keys_by_xpath(u"55")
            ApplicationPage.Step13_DocumentsApplyAddress_House.send_keys_by_xpath("3")

        with step("Место на я.картах, выбор подразделения"):
            ApplicationPage.yamaps_setlocation()

        with step("Флажки согласен с условиями и отправка формы"):
            ApplicationPage.StepFinal_CheckBoxApproveLaw1.click()
            ApplicationPage.StepFinal_CheckBoxApproveLaw2.click()
            screenshot("Перед отправкой формы")
            ApplicationPage.StepFinal_SubmitFormButton.click()

        with step("Подтверждение отправки формы в ведомство"):
            PersonalCabinetPage.AcceptedHeader.is_visible(wait=30)
            assert PersonalCabinetPage.AcceptedHeader.is_exists(), "Не удалось найти заголовок отправки заявления"
            assert PersonalCabinetPage.check_submittext_existence(), "Не удалось найти текст об отправке введомство"
            engine.refresh()
            assert PersonalCabinetPage.check_submittext_existence(), "Не удалось найти текст об отправке введомство"

