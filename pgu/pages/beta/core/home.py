#!/usr/bin/python
# coding=utf-8

__author__ = 'alexander-momot'

from assets.objects.elements import Button, Text, Element
from pgu.pages import BasePage


class BetaHomePage(BasePage):

    __url__ = "/"

    PersonalCabinetButton = Button("//li[contains(@class,'login')]/a[contains(.,'Личный кабинет')]")
    EnterButton = Button("//a[contains(text(), 'Войти')]")
    LoggedUserCabinet = Button("//span[@id='login-name']")

    GosuslugiMainHeader = Element("//div[@id='header']")
    CategoryTopBtn = Button(GosuslugiMainHeader + "//li/a[contains(text(), 'Каталог услуг')]")
    CategoryBottomBtn = Button("//a[contains(text(), 'Все услуги')]")

    CategoryPopup = Element(GosuslugiMainHeader + "//div[@class='catalog-on-main']")

    @classmethod
    def click_to_profile_cabinet(cls):
        cls.PersonalCabinetButton.click()

    @classmethod
    def click_login_into_esia(cls):
        cls.EnterButton.click()

    @classmethod
    def hover_to_catalogue_and_choose_service(cls, name):
        cls.CategoryBottomBtn.hover()
        # TODO: choose

    class Goto:

        def __init__(self):
            raise Exception("This is an action class, use it like this: Page.Goto.some_service_by_name('name')")

        ServiceName = "//div[@id='content-wrapper']//div[contains(text(),'%s')]/ancestor::a"
        ServiceHeader = Text("//h1[text()='Популярные услуги']/..")

        ShtrafGibdd = Button("//a[@href='/10001']")
        NalogovayaZadolzhnost = Button("//a[@href='/10002']")
        SudebnayaZadolzhnost = Button("//a[@href='/10003']")

        ZapisKVrachu = Button("//a[@href='/10066']")
        IzveshenieOSostoyaniiLicevogoSchetaPFR = Button("//a[@href='/10042']")
        ZagranPassport = Button("//a[@href='/10051']")

        PassportGrazhdanina = Button("//a[@href='/10052']")
        RegistraciyaTransportnogoSredstva = Button("//a[@href='/10059']")
        VoditelskoeUdostoverenieButton = Button("//a[@href='/10056']")

        # NalogoviyUchetFizLiz = Button("//span[@href='/10053']")
        # PodachaNalogovoyDeclaracii = Button("//span[@href='/10054/1']")
        # OformleniePredprinimatelskoyDeyatelnosti = Button("//span[@href='/10058']")
        # ZapisVDetsad = Button("//span[@href='/10999']")
        # OplataZhKKh = Button("//span[@href='/10373']")

        @classmethod
        def service_by_name(cls, name):
            """
            Full russian name of service to visit on beta portal, for example "Налоговый учет физических лиц"
            :type name: str
            :return: None
            """
            Service = Button(cls.ServiceName % name)
            Service.click()

        @classmethod
        def traffic_police_fines(cls):
            cls.ShtrafGibdd.click()

        @classmethod
        def taxes_debts(cls):
            cls.NalogovayaZadolzhnost.click()

        @classmethod
        def judical_debts(cls):
            cls.SudebnayaZadolzhnost.click()

        @classmethod
        def doctor_appointment(cls):
            cls.ZapisKVrachu.click()

        @classmethod
        def pfr_bill(cls):
            cls.IzveshenieOSostoyaniiLicevogoSchetaPFR.click()

        @classmethod
        def foreign_passport(cls):
            cls.ZagranPassport.click()

        @classmethod
        def domestic_passport(cls):
            cls.PassportGrazhdanina.click()

        @classmethod
        def amts_register(cls):
            cls.RegistraciyaTransportnogoSredstva.click()

        @classmethod
        def driving_license(cls):
            cls.VoditelskoeUdostoverenieButton.click()