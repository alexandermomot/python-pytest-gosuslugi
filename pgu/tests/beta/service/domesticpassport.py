#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import time
from assets.driver.engine import DriverEngine
from pgu.tests.conftest import step, testcase, skipif, issue, feature, story

from pgu.pages.esia import EsiaHomePage
from pgu.pages.beta.core import BetaHomePage, CategoryPage, PersonalCabinetPage
from pgu.pages.beta.service.domesticpassport import ReceptionPage, ApplicationPage, Vars
from vars import SystemVars


@feature("Услуги")
@story("Получение паспорта гражданина РФ")
@issue("PGU-4")
@testcase("http://pgu-uat-beta.test.gosuslugi.ru/10052/1/form")
class DomesticPassportSanitySuite:
    """ Sanity-тестирование услуги "Получение паспорта гражданина РФ" """

    @skipif(False, reason="Ошибка при открытии главной страницы портала ПГУ")
    def case_rus_passport_positive(self, url: str, engine: DriverEngine, screenshot):

        with step("Переход в авторизацию ЕСИА"):
            BetaHomePage.click_login_into_esia()
            if EsiaHomePage.page_error():
                raise AssertionError("Не удалось авторизоваться в ЕСИА")

        with step("Авторизация в ЕСИА"):
            EsiaHomePage.Login(Vars.esia_email, Vars.esia_pass).by_email_or_phone()

        with step("Переход в услугу 'Заграничный пасспорт'"):
            BetaHomePage.Goto.domestic_passport()

        with step("Паспорт РФ - онлайн заявка"):
            ReceptionPage.find_and_click_category("Замена паспорта РФ в связи с достижением возраста 20 или 45 лет")
            ReceptionPage.online_claim_type()

        with step("Новый черновик - если требуется"):
            ApplicationPage.create_new_draft()

        with step("Проверка появления главного локатора формы услуги, если не появилась - падение кейса"):
            if ApplicationPage.form_error():
                raise AssertionError(ApplicationPage.form_error.ERROR_TEXT)
            if ApplicationPage.form_error_department():
                raise AssertionError(ApplicationPage.form_error_department.ERROR_TEXT)

        with step("Ввод номера телефона"):
            ApplicationPage.personal_data()

        with step("Проверка требований к фото"):
            ApplicationPage.Step3_PhotoRequirementsButton.click()
            assert ApplicationPage.Step3_PhotoRequirementsPopUp.is_exists()
            ApplicationPage.Step3_PhotoRequirementsCross.click()
            assert not ApplicationPage.Step3_PhotoRequirementsPopUp.is_exists()

        with step("Загружаем фото - делаем скриншот"):
            ApplicationPage.Step3_PhotoFileInput.upload_file(SystemVars.system.DATA_ROOT + "passport1.jpg")
            ApplicationPage.Step3_TextPhotoIsOk.is_visible(wait=15)
            assert ApplicationPage.Step3_TextPhotoIsOk.is_exists(), "Не найден текст об удачной загрузке фото"
            screenshot(engine.title)

        with step("Вводим данные прошлого паспорта"):
            ApplicationPage.Step4_PassportReplace_Series.clear_backspace()
            ApplicationPage.Step4_PassportReplace_Series.send_keys_by_xpath("3342")
            ApplicationPage.Step4_PassportReplace_Number.clear_backspace()
            ApplicationPage.Step4_PassportReplace_Number.send_keys_by_xpath("111111")
            ApplicationPage.Step4_PassportReplace_IssueDate.clear_backspace()
            ApplicationPage.Step4_PassportReplace_IssueDate.send_keys_by_xpath("25072005")
            ApplicationPage.Step4_PassportReplace_DeptCode.clear_backspace()
            ApplicationPage.Step4_PassportReplace_DeptCode.send_keys_by_xpath("123321")
            ApplicationPage.Step4_PassportReplace_Issuer.clear_backspace()
            ApplicationPage.Step4_PassportReplace_Issuer.send_keys_by_xpath(u"ОВД")

        with step("Заполение формы 'наличие загранпаспорта'"):
            ApplicationPage.Step5_IsForeignPassportExist.click()
            time.sleep(3)

        with step("Заполение формы 'наличие иностранного гражданства'"):
            ApplicationPage.Step6_IsForeignCitizenship.click()

        with step("Заполение формы семейного положения"):
            ApplicationPage.Step7_IsMarried.click()

        with step("Заполнение формы 'наличие детей'"):
            ApplicationPage.Step8_Childrens.click()

        with step("Заполение информации о родителях"):
            ApplicationPage.Step9_ParentsInfoMale.click()
            ApplicationPage.Step9_ParentsInfoFemale.click()
            time.sleep(5)

        with step("Центр выдачи документов, ввод адреса"):
            ApplicationPage.documents_apply_address()

        with step("Место на я.картах, выбор подразделения"):
            ApplicationPage.yamaps_setlocation()

        with step("Флажки согласия с условиями"):
            ApplicationPage.StepFinal_CheckBoxApproveLaw1.click()
            ApplicationPage.StepFinal_CheckBoxApproveLaw2.click()

        with step("Скриншот до отправки формы и отправка формы"):
            screenshot("Перед отправкой формы")
            ApplicationPage.StepFinal_SubmitFormButton.click()

        with step("Подтверждение отправки формы в ведомство"):
            PersonalCabinetPage.AcceptedHeader.is_visible(wait=30)
            assert PersonalCabinetPage.AcceptedHeader.is_exists(), u"Не найден текст заголовка"
            assert PersonalCabinetPage.check_submittext_existence(), u"Не найден текст статуса 'Поставлен в очередь на обработку'"
            engine.refresh()
            assert PersonalCabinetPage.check_submittext_existence(), u"Не найден текст статуса 'Принято'"
