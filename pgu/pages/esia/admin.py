# #!/usr/bin/python
# # coding=utf-8
# # -*- coding: utf-8 -*-
# __author__ = 'dmitry.khaschinin'
#
# from assets.objects.elements import (
#     Button, Input,
#     Text )
#
# from pgu.pages import BasePage
#
#
# class EsiaAdminPage(BasePage):
#
#     #url = Url("http://esia-uat.test.gosuslugi.ru/admin")
#
#     @staticmethod
#     def ChooseEmpFromTableAdmin(emp=u"Эксплуатация Инфраструктуры Электронного Правительства"):
#         Button("//span[contains(., '" + emp + "')]").click()
#
#     TabAdminOrg = Button("//a[@title='Организации']")
#
#     ColNameFilter = Button("//div[@id='orglist']//th[@data-title='Наименование']//a")  # Фильтр по наименованию
#     ColOgrnFilter = Button("//div[@id='orglist']//th[@data-title='ОГРН']//a")  # Фильтр по огрн
#     ColInnFilter = Button("//div[@id='orglist']//th[@data-title='ИНН']//a")  # Фильтр по инн
#
#     NextPageAdmin = Button("//a[@title='Следующая страница']")
#
#     NameFilter = Input("//div[contains(., 'Задайте критерий поиска')]//input")
#
#     SubmitFilter = Button("//div[contains(., 'Задайте критерий поиска')]//button[contains(., 'Применить')]")
#
#     Org = Button("//div[@id='orglist']//td//a[contains(text(), 'Эксплуатация ИЭП')]")
#
#     ChangeOrgType = Button("//form[@method='POST']//button[contains(., 'признак ОГВ')]")
#     RevokeOrgIsRa = Button("//form[contains(., 'на выдачу ПЭП')]//button")
#     DeleteRule = Button("//form[contains(., 'Операторы системы подтверждения личности')]//button")
#
#     IsRa = Input("//input[@id='is_ra']")  # Уполномочена на выдачу ПЭП(RA)
#     IsCsg = Input("//input[@id='is_csg']")  # Уполномочена на создание системных групп(CSG)
#     IsScp = Input("//input[@id='is_scp']")  # Ведение разрешений (scope)
#     AcsToGroup = Input(
#         "//input[@id='acs_to_group']")  # Имеет доступ к группе "Операторы системы подтверждения личности"
#
#     # Карточка ОГВ
#
#     OgvCard = Button("//li//a[contains(text(), 'Карточка ОГВ')]")
#
#     OrgOgrn = Input("//input[@id='org_ogrn']")  # ОГРН
#     OrgInn = Input("//input[@id='org_inn']")  # ИНН
#     OrgKpp = Input("//input[@id='org_kpp']")  # КПП
#     OrgInf = Input("//input[@id='org_inf']")  # Описание
#     RspFio = Input("//input[@id='rsp_fio']")  # ФИО
#     RspPos = Input("//input[@id='rsp_pos']")  # Должность
#     RspCorpPhn = Input("//input[@id='rsp_corp_phn']")  # Рабочий телефон
#     RspMbtPhn = Input("//input[@id='rsp_mbt_phn']")  # Мобильный телефон
#     RspEmail = Input("//input[@id='rsp_email']")  # E-mail
#
#     SaveOgvCard = Button("//button[contains(text(), 'Сохранить')]")
#     BackInOrg = Button("//a[contains(text(), 'Вернуться назад')]")
#
#     @staticmethod
#     def EditFormOgvCard(clearall=False, OrgOgrn="0000000000000", OrgInn="0401802932", OrgKpp="000000000",
#                         OrgInf="123123", RspFio=u"Хащинин Дмитрий Игоревич",
#                         RspPos=u"Царь", RspCorpPhn="1234567890", RspMbtPhn="0987654321",
#                         RspEmail="dmitry.khaschinin@rtlabs.ru"):
#         if clearall:
#             EsiaAdminPage.OrgOgrn.clear()
#             EsiaAdminPage.OrgInn.clear()
#             EsiaAdminPage.OrgKpp.clear()
#             EsiaAdminPage.OrgInf.clear()
#             EsiaAdminPage.RspFio.clear()
#             EsiaAdminPage.RspPos.clear()
#             EsiaAdminPage.RspCorpPhn.clear()
#             EsiaAdminPage.RspMbtPhn.clear()
#             EsiaAdminPage.RspEmail.clear()
#         else:
#             EsiaAdminPage.OrgOgrn.send_keys_by_xpath(OrgOgrn)
#             EsiaAdminPage.OrgInn.send_keys_by_xpath(OrgInn)
#             EsiaAdminPage.OrgKpp.send_keys_by_xpath(OrgKpp)
#             EsiaAdminPage.OrgInf.send_keys_by_xpath(OrgInf)
#             EsiaAdminPage.RspFio.send_keys_by_xpath(RspFio)
#             EsiaAdminPage.RspPos.send_keys_by_xpath(RspPos)
#             EsiaAdminPage.RspCorpPhn.send_keys_by_xpath(RspCorpPhn)
#             EsiaAdminPage.RspMbtPhn.send_keys_by_xpath(RspMbtPhn)
#             EsiaAdminPage.RspEmail.send_keys_by_xpath(RspEmail)
#
#             # Системы
#
#     ItSys = Button("//li//a[contains(text(), 'Системы')]")
#
#     AddItSys = Button("//a[contains(text(), 'Добавить систему')]")
#
#     ItSysName = Input("//input[@id='itsys_full_name']")  # Название ИС
#     ItSysSysName = Input("//input[@id='itsys_sysname']")  # Мнемоника ИС
#     ItSysFgisId = Input("//input[@id='itsys_fgis_id']")  # ID в ФГИС
#     ItSysFgisId2 = Input("//input[@id='itsys_fgis_idl']")  # ID в ФГИС
#
#     ItSysIsVis = Input("//input[@id='is_vis']")  # Имеет доступ к изменению контактов пользователя без запроса пароля
#     ItSysIsAas = Input("//input[@id='is_aas']")  # Имеет доступ к сервисам OAuth 2.0(AAS)
#
#     DeleteItSys = Button("//button[contains(text(), 'Удалить')]")
#
#     # Отображение системы для пользователей
#
#     ItSysPubName = Input("//input[@id='it_sys_pub_nam']")  # Название
#     ItSysHomeUrl = Input("//input[@id='it_sys_hom_url']")  # URL
#     ItSysPubInf = Input("//textarea[@id='it_sys_pub_inf']")  # ID в ФГИС
#
#     SaveAddItSys = Button("//dl/dd/button[contains(text(), 'Добавить')]")
#     SaveEditItSys1 = Button("//form[contains(., 'Мнемоника ИС')]//dl/dd/button[contains(text(), 'Сохранить')]")
#     SaveEditItSys2 = Button("//form[contains(., 'Логотип')]//dl/dd/button[contains(text(), 'Сохранить')]")
#
#     # Сервис провайдер
#
#     AddEntityId = Button("//a[contains(text(), 'Добавить сервис-провайдера')]")
#
#     EntityId = Input("//input[@id='entity_id']")
#     SaveEntityId = Button("//form[contains(., 'Entity ID')]//dl/dd/button[contains(text(), 'Сохранить')]")
#
#     @staticmethod
#     def DeleteEntityIdFromTable(entityid):
#         Button("//div[@id='splist']//tr[contains(., '" + entityid + "')]//a[@href='#']").click()
#
#     # Системная группа
#
#     AddGrpMtd = Button("//a[contains(text(), 'Добавить группу')]")
#
#     GrpSysName = Input("//input[@id='sysname']")  # Мнемоника
#     GrpName = Input("//input[@id='name']")  # Название
#     GrpInfo = Input("//textarea[@id='info']")  # Описание
#
#     SaveGrpMtd = Button("//form[contains(., 'Мнемоника ')]//dl/dd/button[contains(text(), 'Сохранить')]")
#
#     @staticmethod
#     def FillFormGrpMtd(clearall=False, grpsysname="MNMNK123", grpname=u"Тестовое название",
#                        grpinfo=u"Тестовое описание"):
#         if clearall:
#             EsiaAdminPage.GrpSysName.clear()
#             EsiaAdminPage.GrpName.clear()
#             EsiaAdminPage.GrpInfo.clear()
#         else:
#             EsiaAdminPage.GrpSysName.send_keys_by_xpath(grpsysname)
#             EsiaAdminPage.GrpName.send_keys_by_xpath(grpname)
#             EsiaAdminPage.GrpInfo.send_keys_by_xpath(grpinfo)
#
#     @staticmethod
#     def EditGrpMtdFromTable(grpsysname):
#         Button("//div[@id='grpmtd_list']//tr//a[contains(text(), '" + grpsysname + "')]").click(method='classic')
#
#     @staticmethod
#     def DeleteGrpMtdFromTable(grpsysname):
#         Button("//div[@id='grpmtd_list']//tr[contains(., '" + grpsysname + "')]//a[@href='#']").click()
#
#     @staticmethod
#     def FillFormItSys(clearall=False, itsysname=u"Тестовая система", itsyssysname="1234567", itsysfgisid="123456789"):
#         if clearall:
#             EsiaAdminPage.ItSysName.clear()
#             EsiaAdminPage.ItSysSysName.clear()
#             EsiaAdminPage.ItSysFgisId.clear()
#         else:
#             EsiaAdminPage.ItSysName.send_keys_by_xpath(itsysname)
#             EsiaAdminPage.ItSysSysName.send_keys_by_xpath(itsyssysname)
#             EsiaAdminPage.ItSysFgisId.send_keys_by_xpath(itsysfgisid)
#
#     @staticmethod
#     def EditFormItSys(clearall=False, itsysname=u"Тестовая система", itsyssysname="1234567", itsysfgisid="123456789",
#                       itsyspubname=u"Тестовое название",
#                       itsyshomeurl="http://esia-uat.test.gosuslugi.ru/admin/org/1000298920/itsys/1000322258/",
#                       itsyspubinf=u"Тестовый комментарий"):
#         if clearall:
#             EsiaAdminPage.ItSysName.clear()
#             EsiaAdminPage.ItSysSysName.clear()
#             EsiaAdminPage.ItSysFgisId2.clear()
#             EsiaAdminPage.ItSysPubName.clear()
#             EsiaAdminPage.ItSysHomeUrl.clear()
#             EsiaAdminPage.ItSysPubInf.clear()
#         else:
#             EsiaAdminPage.ItSysName.send_keys_by_xpath(itsysname)
#             EsiaAdminPage.ItSysSysName.send_keys_by_xpath(itsyssysname)
#             EsiaAdminPage.ItSysFgisId2.send_keys_by_xpath(itsysfgisid)
#             EsiaAdminPage.ItSysPubName.send_keys_by_xpath(itsyspubname)
#             EsiaAdminPage.ItSysHomeUrl.send_keys_by_xpath(itsyshomeurl)
#             EsiaAdminPage.ItSysPubInf.send_keys_by_xpath(itsyspubinf)
#             EsiaAdminPage.ItSysIsVis.click()
#             EsiaAdminPage.ItSysIsAas.click()
#
#     @staticmethod
#     def ChooseItSysFromTableAdmin(itsys="Тестовая система"):
#         Button("//tbody//a[contains(., '" + itsys + "')]").click()
#
#     @staticmethod
#     def DeleteItSysFromTableAdmin(itsys="Тестовая система"):
#         Button("//tr[contains(., '" + itsys + "')]//a[contains(., 'Удалить')]").click()
#
#     # Администраторы
#
#     Adm = Button("//li//a[contains(text(), 'Администраторы')]")
#
#     AddAdm = Button("//a[contains(text(), 'Добавить администратора')]")
#
#     AdmSnils = Input("//input[@id='snils']")  # Снилс админа
#     AdmCorpEmail = Input("//input[@id='corp_email']")  # Снилс админа
#
#     SaveAdm1 = Button("//button[contains(text(), 'Продолжить')]")
#     SaveAdm2 = Button("//button[contains(text(), 'Продолжить')]")
#
#     @staticmethod
#     def DeleteAdmFromTable(admsnils):
#         Button("//div[@id='admlist']//tr[contains(., '" + admsnils + "')]//a[@href='#']").click()
#
#     # Центры регистрации
#
#     Ra = Button("//li//a[contains(text(), 'Центры регистрации')]")
#
#     AddRa = Button("//h5//a[contains(text(), 'Добавить центр регистрации')]")
#
#     RaName = Input("//input[@id='ra_name']")  # Название
#     RaAddress = Input("//input[@id='ra_address']")  # Адрес
#     RaAddressHouse = Input("//input[@id='ra_address_house']")  # Дом
#     RaAddressFrame = Input("//input[@id='ra_address_frame']")  # Корпус
#     RaAddressBuild = Input("//input[@id='ra_address_build']")  # Строение
#     RaZipCode = Input("//input[@id='ra_zip_code']")  # Индекс
#     RaLongitude = Input("//input[@id='ra_longitude']")  # Долгота
#     RaLatitude = Input("//input[@id='ra_latitude']")  # Широта
#     RaContacts = Input("//input[@id='ra_contacts']")  # Время работы
#     RaOperationTime = Input("//input[@id='ra_operation_time']")  # Контакты
#     RaAdditionalInfo = Input("//input[@id='ra_additional_info']")  # Дополнительная информация
#
#     RaFtsReg = Button("//input[@id='ra_fts_reg']")  # Регистрация
#     RaFtsCfm = Button("//input[@id='ra_fts_cfm']")  # Подтверждение личности
#     RaFtsRec = Button("//input[@id='ra_fts_rec']")  # Восстановление доступа
#
#     StatusList = Button("//span[@class='k-select']")  # Статус
#     StatusListN = Button(
#         "//div[@id='get_ra_stu-list']//ul[@id='get_ra_stu_listbox']//li[contains(text(),'Не действующий')]")  # Вкладка Системы
#
#     SaveAddRa = Button("//dl//dd//button[contains(text(), 'Добавить')]")
#     SaveEditRa = Button("//dl//dd//button[contains(text(), 'Сохранить')]")
#
#     @staticmethod
#     def ChooseRaFromTableAdmin(admra):
#         Button("//div[@id='ralist']//tr//td//a[contains(., '" + admra + "')]").click()
#
#     @staticmethod
#     def DeleteRaFromTableAdmin(admra):
#         Button("//div[@id='ralist']//tr[contains(., '" + admra + "')]//a[contains(text(), 'Удалить')]").click()
#         Button("//button[contains(text(), 'Удалить')]").click()
#
#     @staticmethod
#     def FillFormRaAdm(clearall=False, raname=u"Тестовый Центр", raaddress=u"Воронежская область", raaddresshouse="1",
#                       raaddressframe="2", raaddressbuild="3", razipcode="123456", ralongitude="2.863180",
#                       ralatitude="12.863180", racontacts=u"Контакты", raoperationtime="18:00-20:00",
#                       raadditionalinfo=u"Тестовый комментарий"):
#         if clearall:
#             EsiaAdminPage.RaName.clear()
#             EsiaAdminPage.RaAddress.clear()
#             EsiaAdminPage.RaAddressHouse.clear()
#             EsiaAdminPage.RaAddressFrame.clear()
#             EsiaAdminPage.RaAddressBuild.clear()
#             EsiaAdminPage.RaZipCode.clear()
#             EsiaAdminPage.RaLongitude.clear()
#             EsiaAdminPage.RaLatitude.clear()
#             EsiaAdminPage.RaContacts.clear()
#             EsiaAdminPage.RaOperationTime.clear()
#             EsiaAdminPage.RaAdditionalInfo.clear()
#             EsiaAdminPage.RaFtsCfm.click()
#         else:
#             EsiaAdminPage.RaName.send_keys_by_xpath(raname)
#             EsiaAdminPage.RaAddress.send_keys_by_xpath(raaddress)
#             EsiaAdminPage.RaAddressHouse.send_keys_by_xpath(raaddresshouse)
#             EsiaAdminPage.RaAddressFrame.send_keys_by_xpath(raaddressframe)
#             EsiaAdminPage.RaAddressBuild.send_keys_by_xpath(raaddressbuild)
#             EsiaAdminPage.RaZipCode.send_keys_by_xpath(razipcode)
#             EsiaAdminPage.RaLongitude.send_keys_by_xpath(ralongitude)
#             EsiaAdminPage.RaLatitude.send_keys_by_xpath(ralatitude)
#             EsiaAdminPage.RaContacts.send_keys_by_xpath(racontacts)
#             EsiaAdminPage.RaOperationTime.send_keys_by_xpath(raoperationtime)
#             EsiaAdminPage.RaAdditionalInfo.send_keys_by_xpath(raadditionalinfo)
#             EsiaAdminPage.StatusList.click()
#             EsiaAdminPage.StatusListN.click()
#             EsiaAdminPage.RaFtsReg.click()
#             EsiaAdminPage.RaFtsCfm.click()
#             EsiaAdminPage.RaFtsRec.click()
#
#     # Пользователи
#
#     TabAdminPerson = Button("//a[@title='Пользователи']")
#
#     SearchCbSnils = Button("//label[@for='search_cb_snils']")  # поиск по СНИЛС
#     InputSnils = Input("//input[@id='input_snils']")
#
#     SearchCbEmail = Button("//label[@for='search_cb_e_mail']")  # поиск по почте
#     InputEmail = Input("//input[@id='input_e_mail']")
#
#     SearchcbMobilePhone = Button("//label[@for='search_cb_mobile_phone']")  # поиск по телефону
#     InputMobilePhone = Input("//input[@id='input_mobile_phone']")
#
#     Find = Button("//button[contains(text(), 'Найти')]")
#
#     PersonProfile = Button("//dl[contains(., 'ФИО')]//a")
#
#     BackToSearch = Button("//a[contains(., 'Вернуться к поиску')]")
#
#     @staticmethod
#     def SearchBySnilsAdmin(snils):
#         EsiaAdminPage.SearchCbSnils.click()
#         EsiaAdminPage.InputSnils.clear_backspace()
#         EsiaAdminPage.InputSnils.send_keys_by_xpath(snils)
#         EsiaAdminPage.Find.click()
#
#     @staticmethod
#     def SearchByEmailAdmin(email):
#         EsiaAdminPage.SearchCbEmail.click()
#         EsiaAdminPage.InputEmail.clear_backspace()
#         EsiaAdminPage.InputEmail.send_keys_by_xpath(email)
#         EsiaAdminPage.Find.click()
#
#     @staticmethod
#     def SearchByPhoneAdmin(phone):
#         EsiaAdminPage.SearchcbMobilePhone.click()
#         EsiaAdminPage.InputMobilePhone.clear_backspace()
#         EsiaAdminPage.InputMobilePhone.send_keys_by_xpath(phone)
#         EsiaAdminPage.Find.click()
#
#     # ARM CPP
#
#     SearchcbOgrn = Button("//label[@for='search_cb_ogrn']")
#     SearchcbInn = Button("//label[@for='search_cb_inn']")
#
#     InputOgrn = Input("//input[@id='input_ogrn']")  # поиск организации
#     InputInn = Input("//input[@id='input_inn']")  # поиск организации
#     InputBirthDate = Input("//input[@id='input_birth_date']")  # поиск организации
#
#     OrgProfile = Button("//dl[contains(., 'Название организации')]//a")
#
#     InputSecretAnswer = Input("//input[@id='secret_answer']")  # секретный ответ
#     SubmitSecretAnswer = Button("//button[contains(text(), 'Проверить')]")
#     MessageGoodSecretAnswer = Text("//p[@class='message good']")
#
#     PersonEvent = Button("//a[contains(., 'Посмотреть события')]")
#     FirstEvent = Button("//div[@id='eventlist']//tbody/tr")
#
#     BackToProfile = Button("//a[contains(., 'Вернуться к данным пользователя')]")
#
#     @staticmethod
#     def SearchByOgrnCpp(ogrn):
#         EsiaAdminPage.SearchcbOgrn.click()
#         EsiaAdminPage.InputOgrn.clear_backspace()
#         EsiaAdminPage.InputOgrn.send_keys_by_xpath(ogrn)
#         EsiaAdminPage.Find.click()
#
#     @staticmethod
#     def SearchByInnCpp(inn):
#         EsiaAdminPage.SearchcbInn.click()
#         EsiaAdminPage.InputInn.clear_backspace()
#         EsiaAdminPage.InputInn.send_keys_by_xpath(inn)
#         EsiaAdminPage.Find.click()
#
#     @staticmethod
#     def SearchBySnilsCpp(snils, birthdate):
#         EsiaAdminPage.SearchCbSnils.click()
#         EsiaAdminPage.InputSnils.clear_backspace()
#         EsiaAdminPage.InputSnils.send_keys_by_xpath(snils)
#         EsiaAdminPage.Find.click()
#         EsiaAdminPage.InputBirthDate.clear_backspace()
#         EsiaAdminPage.InputBirthDate.send_keys_by_xpath(birthdate)
#         EsiaAdminPage.Find.click()
#
#     @staticmethod
#     def SearchByEmailCpp(email, birthdate):
#         EsiaAdminPage.SearchCbEmail.click()
#         EsiaAdminPage.InputEmail.clear_backspace()
#         EsiaAdminPage.InputEmail.send_keys_by_xpath(email)
#         EsiaAdminPage.Find.click()
#         EsiaAdminPage.InputBirthDate.clear_backspace()
#         EsiaAdminPage.InputBirthDate.send_keys_by_xpath(birthdate)
#         EsiaAdminPage.Find.click()
#
#     @staticmethod
#     def SearchByPhoneCpp(phone, birthdate):
#         EsiaAdminPage.SearchcbMobilePhone.click()
#         EsiaAdminPage.InputMobilePhone.clear_backspace()
#         EsiaAdminPage.InputMobilePhone.send_keys_by_xpath(phone)
#         EsiaAdminPage.Find.click()
#         EsiaAdminPage.InputBirthDate.clear_backspace()
#         EsiaAdminPage.InputBirthDate.send_keys_by_xpath(birthdate)
#         EsiaAdminPage.Find.click()
