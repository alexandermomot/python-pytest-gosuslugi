#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-

import re

from assets.objects.elements import Button, Input, Text
from pgu.pages.beta.service import BaseFormPage


class ApplicationPage(BaseFormPage):

    Step1_Reason20YO = Button("//div[@id='form.FormStep1.Panel1.change1']//a")
    Step1_Reason45YO = Button("//div[@id='form.FormStep1.Panel1.change2']//a")

    Step2_Phone = Input("//input[@id='form.FormStep2.Panel1.Panel17.PHONEedit']")
    Step2_PassportData_Country_ClickZone = Button("//div[@id='form.FormStep2.Panel1.Panel18.BIRTHCOUNTRY']/div[@class='PGU-ComboBox PGU-FieldBody search PGU-context-search_show']")
    Step2_PassportData_Country_InputZone = Input("//div[@id='form.FormStep2.Panel1.Panel18.BIRTHCOUNTRY']//input[@name='search_query' and @type='text']")
    Step2_PassportData_Country_ListElement = Button("//div[@id='form.FormStep2.Panel1.Panel18.BIRTHCOUNTRY']//div[contains(@class,'PGU-ComboBoxOption') and @data-value!='']")
    Step2_PassportData_PlaceOfBirth = Input("//input[@id='form.FormStep2.Panel1.Panel19.BIRTHPLACE']")

    Step3_PhotoRequirementsButton = Button("//div[@id='form.FormStep3']//a[contains(text(),'Все требования')]")
    Step3_PhotoRequirementsPopUp = Text("//div[@id='form.FormStep3']//div[@class='PGU-Details js-PGU-Details']")
    Step3_PhotoRequirementsCross = Button("//div[@id='form.FormStep3']//div[@class='PGU-Details js-PGU-Details']//a[contains(@class,'close')]")
    Step3_PhotoFileInput = Input("//div[@id='form.FormStep3']//div[contains(@id, 'photo')]//input[@type='file']")
    Step3_TextPhotoIsOk = Text('//*[@id="imageEditorImageView"]/div[2]/span[contains(text(), "фотография успешно загружена")]')

    Step4_PassportReplace_Series = Input("//div[@id='form.FormStep4']//input[contains(@id,'PASP_SERIE')]")
    Step4_PassportReplace_Number = Input("//div[@id='form.FormStep4']//input[contains(@id,'PASP_NUMBER')]")
    Step4_PassportReplace_IssueDate = Input("//div[@id='form.FormStep4']//input[contains(@id,'PASP_DATE')]")
    Step4_PassportReplace_DeptCode = Input("//div[@id='form.FormStep4']//input[contains(@id,'PASP_CODE')]")
    Step4_PassportReplace_Issuer = Input("//div[@id='form.FormStep4']//input[contains(@id,'PASP_BY')]")

    Step5_IsForeignPassportExist = Button("//div[@id='form.FormStep5']//div[contains(@id,'FieldRadio2')]//a")

    Step6_IsForeignCitizenship = Button("//div[@id='form.FormStep6']//div[contains(@id,'HASOTHERCITIZENSHIP1')]//a")

    Step7_IsMarried = Button("//div[@id='form.FormStep7.Panel2_M.MARSTATUS2']//a")

    Step8_Childrens = Button("//div[@id='form.FormStep8']//div[contains(@id,'CHILD2')]//a")

    Step9_ParentsInfoMale = Button("//div[@id='form.FormStep9']//div[contains(@id,'CHECK_FATHER')]//a")
    Step9_ParentsInfoFemale = Button("//div[@id='form.FormStep9']//div[contains(@id,'CHECK_Mather')]//a")

    Step10_DocumentsApplyAddress = Input("//div[@id='form.FormStep15']//input[@class='PGU-FiasDropdown-Search-Input js-PGU-FiasDropdown-Search-Input']")
    Step10_DocumentsApplyAddress_Select = Button("(//div[@id='form.FormStep15']//div[@class='js-PGU-Fias-Option PGU-Fias-Option'])[2]")
    Step10_DocumentsApplyAddress_AdditionalArea = Input("//div[@id='form.FormStep15']//div[@class='row-Fias row-Fias-additionalStreet']//input")
    Step10_DocumentsApplyAddress_House = Input("//div[@id='form.FormStep15']//div[@class='row-Fias row-Fias-house']//input")
    Step10_DocumentsApplyAddress_Apartment = Input("//div[@id='form.FormStep15']//div[@class='row-Fias row-Fias-apartment']//input")

    Step11_YaMapsAnyLocation = Button("//div[@id='form.FormStep10_1']//div[contains(@class,'PGU-FieldMapWithListItemWrap')]")
    Step11_YaMapsPopupButton = Button("//div[@id='form.FormStep10_1']//div[contains(@class,'baloonContent-wrap')]//div[contains(@class,'Item')]/a")

    StepFinal_CheckBoxApproveLaw1 = Button("//div[@id='form.FormStep16']//div[contains(@id,'FieldCheckbox1')]/a")
    StepFinal_CheckBoxApproveLaw2 = Button("//div[@id='form.FormStep16']//div[contains(@id,'FieldCheckbox2')]/a")
    StepFinal_SubmitFormButton = Button("//div[@id='form.FormStep16']//a[contains(@id,'ButtonFinal')]")

    @classmethod
    def personal_data(cls, phonenum="70001234567"):
        cls.Step2_Phone.send_keys_by_xpath_delay(phonenum, 0.2)
        i = 0
        while re.sub("[^0-9]", "", cls.Step2_Phone.get_value()) != phonenum and i < 5:
            cls.Step2_Phone.get_value()
            cls.Step2_Phone.clear_backspace()
            cls.Step2_Phone.send_keys_by_xpath_delay(phonenum, 0.2)
            i += 1

    @classmethod
    def documents_apply_address(cls):
        cls.Step10_DocumentsApplyAddress.clear()
        cls.Step10_DocumentsApplyAddress.send_keys_by_xpath_delay("Москва Хуторская ", 0.3)
        cls.Step10_DocumentsApplyAddress_Select.click()
        cls.Step10_DocumentsApplyAddress_AdditionalArea.clear()
        cls.Step10_DocumentsApplyAddress_AdditionalArea.send_keys_by_xpath("Мирлэнд")
        cls.Step10_DocumentsApplyAddress_House.clear()
        cls.Step10_DocumentsApplyAddress_House.send_keys_by_xpath("5")
        cls.Step10_DocumentsApplyAddress_Apartment.clear()
        cls.Step10_DocumentsApplyAddress_Apartment.send_keys_by_xpath("38")

    @classmethod
    def passport_data(cls, country="Росс", birthplace="Москва"):
        cls.Step2_PassportData_Country_ClickZone.click()
        cls.Step2_PassportData_Country_InputZone.click()
        cls.Step2_PassportData_Country_InputZone.clear()
        cls.Step2_PassportData_Country_InputZone.clear_backspace()
        cls.Step2_PassportData_Country_InputZone.send_keys_by_xpath_delay(country)
        cls.Step2_PassportData_Country_ListElement.click()
        cls.Step2_PassportData_PlaceOfBirth.clear()
        cls.Step2_PassportData_PlaceOfBirth.clear_backspace()
        cls.Step2_PassportData_PlaceOfBirth.send_keys_by_xpath_delay(birthplace)

    @classmethod
    def photo_upload(cls):
        cls.Step3_PhotoFileInput.upload_file("")
