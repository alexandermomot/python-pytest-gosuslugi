#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import (
    Button)

from pgu.pages.beta.service import BaseReceptionPage, BaseClaimTypePage


class ReceptionPage(BaseReceptionPage, BaseClaimTypePage):

    @classmethod
    def find_and_click_to_name(cls, subservice):
        """
        Full russian name of subservice available to visit, for example "Постановка на учет и выдача документов"
        :param subservice: String
        :return: None
        """
        ChooseService = Button("//li[contains(@data-ng-repeat, 'pageInfo.links')]/a[contains(text(), '{subservice}')]".format(subservice=subservice))
        ChooseService.click()


