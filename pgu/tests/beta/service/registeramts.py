#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.driver.engine import DriverEngine
from pgu.tests.conftest import step, testcase, skipif, issue, feature, story, fail

from pgu.pages.esia import EsiaHomePage
from pgu.pages.beta.core import BetaHomePage, CategoryPage, PersonalCabinetPage
from pgu.pages.beta.service.amts import ReceptionPage, ApplicationPage, Vars


@feature("Услуги")
@story("Регистрация АМТС")
@issue("PGU-6")
@testcase("http://pgu-uat-beta.test.gosuslugi.ru/10059/3/form")
class RegisterAmtsSanitySuite:
    """ Sanity-тестирование услуги "Регистрация транспортного средства" """

    @skipif(False, reason="Ошибка при открытии главной страницы портала ПГУ")
    def case_register_amts_positive(self, url: str, engine: DriverEngine, screenshot):

        with step("Переход на страницу ЕСИА"):
            BetaHomePage.click_login_into_esia()

        with step("Выбираем авторизацию и вводим логин/пароль"):
            if EsiaHomePage.page_error():
                engine.screenshot("Esia login failed")
                fail("Esia login failed")
            EsiaHomePage.Login(Vars.esia_email, Vars.esia_pass).by_email_or_phone()

        with step("Переход на сервис 'Регистрация транспортного средства'"):
            BetaHomePage.Goto.amts_register()

        with step("Переход на сервис 'Постановка на учет и выдача документов'"):
            ReceptionPage.find_and_click_to_name("Постановка на учет и выдача документов")

        with step("Переход на сервис 'Постановка на учет автотранспорта'"):
            ReceptionPage.find_and_click_to_name("Постановка на учет автотранспорта")

        with step("Выбор электронного заявления"):
            ReceptionPage.online_claim_type()

        with step("Кликаем на 'Новый черновик', если существует запрос"):
            ApplicationPage.create_new_draft()

        with step("Проверка появления главного локатора формы услуги, если не появилась - падение кейса"):
            if ApplicationPage.form_error():
                fail(ApplicationPage.form_error.ERROR_TEXT)

        with step("Проверка на невидимость дисклеймеров 'Заявка подана' и 'Ведомство недоступно', иначе - падение кейса"):
            if ApplicationPage.form_error_department():
                fail(ApplicationPage.form_error_department.ERROR_TEXT)

        with step("Выбор транспортного средства"):
            ApplicationPage.transport_type(2)

        with step("Выбор типа собственности"):
            ApplicationPage.property_type()

        with step("Ввод персональных данных - номер телефона"):
            ApplicationPage.personal_data()

        with step("Ввод персональных данных - проживание"):
            ApplicationPage.home_address(selectinput_element=2)

        with step("Требуется ли получение номера"):
            ApplicationPage.obtain_state_licenseplate()

        with step("Выбор новое ТС"):
            ApplicationPage.new_passport_of_vehicle_required()

        with step("Ввод данных - категория ТС"):
            ApplicationPage.vehicle_category_type()

        with step("Описание ТС"):
            ApplicationPage.vehicle_description()

        with step("Ввод данных о ТС"):
            ApplicationPage.vehicle_data()
            #ApplicationPage.Step_VehicleDescriptionChassis.get_value()

        with step("Выбор локации ГАИ для оформления"):
            ApplicationPage.location_of_registration()

        with step("Выбор позиции Я.Карт"):
            ApplicationPage.yamaps_setlocation()
            ApplicationPage.date_of_visit(time_visit=None)

        with step("Клик 'согласен' и отправка данных формы"):
            screenshot(u"Перед отправкой формы")
            ApplicationPage.click_agree_and_submit_form()

        with step("Проверка Подтверждение в ЛК об отправленной заявке"):
            PersonalCabinetPage.AcceptedHeader.is_visible(wait=30)
            assert PersonalCabinetPage.AcceptedHeader.is_exists(), u"Не найден текст заголовка"
            assert PersonalCabinetPage.check_submittext_existence(), u"Не найден текст статуса 'Поставлен в очередь на обработку'"
            engine.refresh()
            assert PersonalCabinetPage.check_submittext_existence(), u"Не найден текст статуса 'Принято'"

