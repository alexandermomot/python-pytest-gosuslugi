#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import logging
from assets.objects.elements.element import Element
from assets.utils.timer import Timer


class Loader(Element):

    __timer = Timer()

    def __init__(self, xpath):
        Element.__init__(self, xpath)

    # TODO: loader should try to "catch" element, wait, but not except if it isn't found
    # элемент loader использовать не рекомендуется, если заранее неизвестно
    # будет ли использован на странице элемент  загрузки. Иначе, он будет ждать появления элемента
    # и если он не появится - кидать фейл теста. Нужна  доделка driver_engine

    def wait_for_disappear(self, sec = 30):
        logging.info("Waiting [%s] seconds for a loading" % (sec))
        self.__timer.start_timer()
        self.driver.find_element_by_xpath_and_wait_until_becomes_invisible(self.xpath, sec)
        self.__timer.stop_timer()
        logging.info("Loader done after [%s] seconds" % (self.__timer.elapsed_sec_str()))

    def wait_for_appear(self, sec = 30):
        logging.info("Waiting [%s] seconds until loader appears" % (sec))
        self.__timer.start_timer()
        self.driver.find_element_by_xpath_and_wait_until_becomes_visible(self.xpath, sec)
        self.__timer.stop_timer()
        logging.info("Loader appeared after [%s] seconds" % (self.__timer.elapsed_sec_str()))
