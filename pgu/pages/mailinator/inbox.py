#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import time

from assets.objects.elements import Button, Text
from pgu.pages import BasePage
from pgu.pages.mailinator import LetterPage

class InboxPage(BasePage):

    __emailAtDomain = "@mailinator.com"
    __inboxUrlByParameter = "http://mailinator.com/inbox.jsp?to={inbox}"
    __inboxUrlBySubdomain = "http://{inbox}.mailinator.com"

    __mailName = None
    __mailPrefix = None
    __mailPostfix = None
    __mailTimestamp = None

    __EmailAddress = None

    ShowFirstMailButton = Button("//a[contains(@onclick, 'showmail')]")
    MailBody = Text("//div[@id='mailshowdivbody']/iframe")

    def __init__(self, xpath, inbox="dummy", prefix=None, postfix=None, delimiter="."):
        """
            :param inbox: String. Name of the email
            :param prefix: String. Defines email prefix, example: "prefix-%inboxname%-%time%@mailinator.com"
            :param postfix: String. Defines email postfix, example: "%inboxname%-%time%-postfix@mailinator.com
            :param delimiter: String. Defines email delimiter, example: "foo.bar@..." or "foo-bar@..."
            """
        BasePage.__init__(self, xpath)
        self.__mailName = inbox
        self.__mailPrefix = prefix
        self.__mailPostfix = postfix
        self.__EmailAddress = ""
        self.__mailTimestamp = str(int(time.time()))
        if prefix: self.__EmailAddress += str(prefix) + delimiter
        self.__EmailAddress += self.__mailName + delimiter + self.__mailTimestamp
        if postfix: self.__EmailAddress += delimiter + str(postfix)
        self.__inboxUrlBySubdomain = self.__inboxUrlBySubdomain.format(inbox=self.__EmailAddress)
        self.__inboxUrlByParameter = self.__inboxUrlByParameter.format(inbox=self.__EmailAddress)

    def open_inbox(self, by_subdomain=False):
        if by_subdomain is True:
            __inboxUrl = self.__inboxUrlBySubdomain # http://asd123.mailinator.com
        elif by_subdomain is False:
            __inboxUrl = self.__inboxUrlByParameter # http://mailinator.com/inbox.jsp?to=asd123
        else:
            raise Exception("Wrong type of subdomain parameter: only true/false accepted")
        self.driver.get(__inboxUrl)
        assert self.driver.current_url() in self.__inboxUrlByParameter, "URL is not mailinator.com"

    def try_to_find_letter(self, refresh_times = 10):
        i = 0
        while i < refresh_times:
            if self.ShowFirstMailButton.is_exists():
                self.ShowFirstMailButton.click()
                break
            else:
                i += 1
                self.driver.refresh()
        if self.MailBody.is_exists():
            return LetterPage("")

    def get_email_address(self):
        return self.__EmailAddress + self.__emailAtDomain # example@example.com

    def get_inbox_url(self):
        return self.__inboxUrlByParameter