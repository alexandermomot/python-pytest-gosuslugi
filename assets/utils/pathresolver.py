#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import os


class PathMap(dict):

    def __init__(self, *args, **kwargs):
        super(PathMap, self).__init__(*args, **kwargs)
        for arg in args:
            if isinstance(arg, dict):
                for k, v in arg.items():
                    if v is not None:
                        if v.endswith("/"):
                            self[k] = os.path.normpath(str(v)) + os.path.sep
                        else:
                            self[k] = os.path.normpath(str(v))
                    else:
                        self[k] = None

        if kwargs:
            for k, v in kwargs.items():
                self[k] = v

    def __getattr__(self, item):
        print("getattr: " + str(item))
        return os.path.normpath(self.__dict__.get(item))

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        super(PathMap, self).__setitem__(key, value)
        self.__dict__.update({key: value})

    def __delattr__(self, item):
        self.__delitem__(item)

    def __delitem__(self, key):
        super(PathMap, self).__delitem__(key)
        del self.__dict__[key]