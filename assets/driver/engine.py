#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import time, logging, datetime, codecs, requests, io
from urllib.parse import urlparse
from http.cookiejar import CookieJar, Cookie
from PIL import Image
from collections import OrderedDict
from selenium.common.exceptions import (
    NoSuchElementException, StaleElementReferenceException,
    TimeoutException, WebDriverException)
from selenium.webdriver.support.expected_conditions import (
    visibility_of_element_located,
    invisibility_of_element_located)
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

from assets.driver.tools import Reader
from assets.driver.expected_conditions import (document_settle_condition)
from vars import SystemVars


class DriverEngine(object):

    def __init__(self, webdriver=None, explicit_timeout=10, implicit_timeout=5, pool_frequency=0.5):
        if not issubclass(webdriver.__class__, WebDriver):
            raise IndentationError("Class: " + str(webdriver) + " is not a subclass of " + str(WebDriver.__class__))
        self.webdriver = webdriver
        """ :type: WebDriver """
        self.pages = OrderedDict()
        self.EXPLICIT_TIMEOUT = explicit_timeout  # Applies to expected conditions
        self.IMPLICIT_TIMEOUT = implicit_timeout  # Applies to every element not found
        self.POLL_FREQUENCY = pool_frequency  # How long to sleep inbetween calls to the method

    def _decorator(self):
        # todo: experiments with class decorators http://stackoverflow.com/questions/1263451/python-decorators-in-classes
        def selfwrapper(*args, **kwargs):
            try:
                return selfwrapper(*args, **kwargs)
            except StaleElementReferenceException:
                logging.critical("Element not found in the cache - perhaps the page has changed since it was looked up")
                logging.critical("Try again")
                return selfwrapper(*args, **kwargs)
        return self

    def document_ready(self, _driver):
        logging.debug("[ARGS] _driver %s" % (_driver, ))
        s = document_settle_condition()
        if s(_driver):
            return _driver.find_element_by_xpath("//body")
        return None

    def find_lambda_element_xpath(self, _driver, xpath, vis):
        logging.debug("[ARGS] _driver %s, xpath %s, vis %s" % (_driver, xpath, vis))
        if vis:
            v = visibility_of_element_located((By.XPATH, xpath))
        else:
            v = invisibility_of_element_located((By.XPATH, xpath))
        if v(_driver):
            return _driver.find_element_by_xpath(xpath)
        return None

    def print_html(self):
        logging.debug("[ARGS]")
        file_name = "page_%s.html" % datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
        logging.critical("[HTML] Save page content to file %s" % file_name)
        sfile = codecs.open(file_name, 'w', 'utf-8')
        sfile.write(self.webdriver.page_source)
        sfile.close()

    def center_view_on_element(self, xpath):  # experimental, ie not supported yet
        logging.debug("[ARGS] xpath %s" % xpath)
        self.webdriver.execute_script(Reader.Javascript.center_view(xpath))
        time.sleep(0.1)  # wait between view change and perform following action

    def find_element_by_xpath(self, xpath: str,
                              ignore_exception: bool=False,
                              visibility: bool=True,
                              editable: bool=True) -> WebElement:
        """
        :param xpath: xpath points to the element
        :param ignore_exception: false, to ignore errors regarding to element
        """
        logging.debug("[ARGS] xpath %s, ignore_exception %s, visibility %s" % (xpath, ignore_exception, visibility))
        element = None
        status = None

        # TODO: experimental timer - should check urls in background
        # def foo():
        #     print(time.ctime())
        #     threading.Timer(5, foo).start()
        # foo()
        # del foo

        counter = time.time()
        while status != "complete":
            status = self.webdriver.execute_script("return document.readyState")
            time.sleep(self.POLL_FREQUENCY)
            if time.time() > (counter + self.EXPLICIT_TIMEOUT):
                break

        try:
            element = WebDriverWait(self.webdriver, self.EXPLICIT_TIMEOUT, self.POLL_FREQUENCY).until(
                lambda x: self.find_lambda_element_xpath(x, xpath, visibility))
            if visibility:
                if editable:
                    if not element.is_displayed() or not element.is_enabled():
                        err_msg = "Error! Element is_displayed: %s, is_enabled: %s" % (element.is_displayed(), element.is_enabled())
                        element = None
                        raise NoSuchElementException(err_msg)
                else:
                    if not element.is_displayed():
                        err_msg = "Error! Element is_displayed: %s" % (element.is_displayed())
                        element = None
                        raise NoSuchElementException(err_msg)
                self.center_view_on_element(xpath)
        except (NoSuchElementException, TimeoutException) as e:
            if not ignore_exception:
                exc_type = e.__class__
                logging.critical("Can't find element by xpath %s" % xpath)
                raise exc_type("Элемент не найден за отведённое время")
        finally:  # URL detector
            if self.webdriver.current_url not in self.pages.values():
                stamp = int(round(time.time() * 1000))
                self.pages[stamp] = self.webdriver.current_url
                logging.info("Browser has changed URL: %s" % (self.pages[stamp],))

        return element

    def advanced_move_and_click(self, element: WebElement) -> None:
        logging.debug("[ARGS] element %s" % element.id)
        methods = {
            "over": ActionChains(self.webdriver).move_to_element(element).click(element).perform,
            "hold": ActionChains(self.webdriver).move_to_element(element).click_and_hold(element).release(element).perform
        }
        attempts = 0
        for method_name, action in methods.items():
            try:
                action()
            except WebDriverException as e:
                attempts += 1
                if attempts < methods.__len__():
                    continue
                else:
                    raise WebDriverException("Невозможно кликнуть на элемент: [%s] exception: %s" % (element.id, e.msg))
            break

    def editable(self, xpath, ignore_exception=False):
        elem = self.find_element_by_xpath(xpath, ignore_exception, editable=False)
        edit_status = None
        if elem:
            edit_status = elem.get_attribute("readonly") or elem.get_attribute("disabled")
        return True if edit_status is None else False

    def press_button(self, xpath, keys: Keys, ignore_exception=False):
        logging.debug("[ARGS] xpath %s, ignore_exception %s," % (xpath, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception, editable=False)
        elem.send_keys(keys)

    def find_element_by_xpath_and_click(self, xpath, ignore_exception=False):
        logging.debug("[ARGS] xpath %s, ignore_exception %s," % (xpath, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception)
        self.advanced_move_and_click(elem)

    def find_element_by_xpath_and_return(self, xpath, ignore_exception=False):
        logging.debug("[ARGS] xpath %s, ignore_exception %s" % (xpath, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception)
        return elem

    def find_element_by_xpath_and_get_elem_invisible(self, xpath, ignore_exception=False):
        logging.debug("[ARGS] xpath %s, ignore_exception %s" % (xpath, ignore_exception))
        inv_elem = self.find_element_by_xpath(xpath, ignore_exception, visibility=False)
        return inv_elem
    
    def find_element_by_xpath_and_get_text(self, xpath, ignore_exception=False) -> str:
        """
        :param xpath: string, represents xpath of an element
        :param ignore_exception: Boolean
        """
        logging.debug("[ARGS] xpath %s, ignore_exception %s" % (xpath, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception)
        return elem.text

    def find_element_by_xpath_and_get_text_from_input(self, xpath, ignore_exception=False):
        logging.debug("[ARGS] xpath %s, ignore_exception %s" % (xpath, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception)
        return elem.get_attribute("value")

    def find_select_by_xpath_and_return_select(self, xpath, ignore_exception=False) -> Select:
        logging.debug("[ARGS] xpath %s, ignore_exception %s" % (xpath, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception)
        ActionChains(self.webdriver).move_to_element(elem).perform()
        return Select(elem)

    def find_element_by_xpath_and_wait_until_becomes_invisible(self, xpath, wait: int, ignore_exception=False) -> WebElement:
        logging.debug("[ARGS] xpath %s, wait %s, ignore_exception %s" % (xpath, wait, ignore_exception))
        temp = self.EXPLICIT_TIMEOUT
        if wait != self.EXPLICIT_TIMEOUT:
            self.EXPLICIT_TIMEOUT = wait
        elem = self.find_element_by_xpath(xpath, ignore_exception, visibility=False)
        self.EXPLICIT_TIMEOUT = temp
        return elem

    def find_element_by_xpath_and_wait_until_becomes_visible(self, xpath, wait: int, ignore_exception=False) -> WebElement:
        logging.debug("[ARGS] xpath %s, wait %s, ignore_exception %s" % (xpath, wait, ignore_exception))
        temp = self.EXPLICIT_TIMEOUT
        if wait != self.EXPLICIT_TIMEOUT:
            self.EXPLICIT_TIMEOUT = wait
        elem = self.find_element_by_xpath(xpath, ignore_exception, visibility=True)
        self.EXPLICIT_TIMEOUT = temp
        return elem

    def find_element_by_xpath_is_selected(self, xpath, ignore_exception=False) -> bool:
        logging.debug("[ARGS] xpath %s, ignore_exception %s" % (xpath, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception)
        return elem.is_selected()

    def find_element_by_xpath_and_clear(self, xpath, ignore_exception=False):
        logging.debug("[ARGS] xpath %s, ignore_exception %s" % (xpath, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception)
        self.advanced_move_and_click(elem)
        elem.clear()

    def find_element_by_xpath_and_move(self, xpath, ignore_exception=False):
        logging.debug("[ARGS] xpath %s, ignore_exception %s" % (xpath, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception)
        ActionChains(self.webdriver).move_to_element(elem).perform()

    def find_element_by_xpath_and_clear_by_backspaces(self, xpath, ignore_exception=False):
        logging.debug("[ARGS] xpath %s, ignore_exception %s" % (xpath, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception)
        self.advanced_move_and_click(elem)
        actions = ActionChains(self.webdriver).send_keys_to_element(elem, Keys.END)
        for c in elem.get_attribute("value"):
            actions.send_keys_to_element(elem, Keys.BACKSPACE)
        actions.perform()

    def find_element_by_xpath_and_send_keys(self, xpath, keys, ignore_exception=False):
        logging.debug("[ARGS] xpath %s, keys %s, ignore_exception %s" % (xpath, keys, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception=ignore_exception)
        self.advanced_move_and_click(elem)
        ActionChains(self.webdriver).send_keys_to_element(elem, keys).perform()

    def find_element_by_xpath_and_send_keys_invisible(self, xpath, keys, ignore_exception=False):
        logging.debug("[ARGS] xpath %s, keys %s, ignore_exception %s" % (xpath, keys, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception=ignore_exception, visibility=False)
        self.advanced_move_and_click(elem)
        ActionChains(self.webdriver).send_keys_to_element(elem, keys).perform()

    def find_element_by_xpath_and_send_keys_with_latency(self, xpath, keys, delay, ignore_exception=False):
        logging.debug("[ARGS] xpath %s, keys %s, delay %s, ignore_exception %s" % (xpath, keys, delay, ignore_exception))
        elem = self.find_element_by_xpath(xpath, ignore_exception)
        self.advanced_move_and_click(elem)
        for k in keys:
            elem.send_keys(k)
            time.sleep(delay)

    def get(self, url):
        logging.debug("[ARGS] url: %s" % url,)
        return self.Url.get(self.webdriver, url)

    def post(self, url, payload):
        logging.debug("[ARGS] url: %s, payload: %s" % (url,payload,))
        return self.Url.post(self.webdriver, url, payload, self.webdriver.get_cookies())

    def forward(self):
        logging.debug("[ARGS] %s" % self.__class__.__name__,)
        self.Page.forward(self.webdriver)

    def back(self):
        logging.debug("[ARGS] %s" % self.__class__.__name__,)
        self.Page.back(self.webdriver)

    def refresh(self):
        logging.debug("[ARGS] %s" % self.__class__.__name__,)
        self.Page.refresh(self.webdriver)

    def screenshot(self, description="Screenshot"):
        logging.debug("[ARGS] %s" % self.__class__.__name__,)
        return self.Page.make_screenshot(self.webdriver, description)

    @property
    def current_url(self):
        logging.debug("[ARGS] %s" % self.__class__.__name__,)
        return self.Page.current_url(self.webdriver)

    @property
    def title(self):
        logging.debug("[ARGS] %s" % self.__class__.__name__,)
        return self.Page.title(self.webdriver)

    def close_browser(self):
        logging.debug("[ARGS] Closing browser: %s" % self.__class__.__name__,)
        self.webdriver.quit()

    def close_window(self):
        logging.debug("[ARGS] Closeing window: %s" % self.__class__.__name__,)
        self.webdriver.close()

    class Url:

        def __init__(self):
            pass

        ssl_verify = False

        @classmethod
        def jar_cookie(cls, driver):
            cookies = driver.get_cookies()
            if cookies is not None:
                jar = CookieJar()
                for s_cookie in cookies:
                    jar.set_cookie(Cookie(
                        version=0, name=s_cookie['name'], value=s_cookie['value'], port='80', port_specified=False,
                        domain=s_cookie['domain'], domain_specified=True, domain_initial_dot=False,
                        path=s_cookie['path'], path_specified=True, secure=s_cookie['secure'],
                        expires=s_cookie['expiry'], discard=False, comment=None, comment_url=None, rest=None,
                        rfc2109=False))
                return jar
            return None

        @classmethod
        def get(cls, driver, url):
            if url.startswith(("http://", "https://", "ftp://")):
                status = str(requests.request("GET", url, verify=cls.ssl_verify, cookies=cls.jar_cookie(driver)).status_code)
                logging.info("Changing URL: [ %s ]: %s" % (url, status, ))
                driver.get(url)
                time.sleep(1) # To be sure that page is loaded
                return status
            elif url.startswith(("/", )):
                domain = '{uri.scheme}://{uri.netloc}'.format(uri=urlparse(driver.current_url()))
                url_f = domain + url
                status = str(requests.request("GET", url_f, verify=cls.ssl_verify, cookies=cls.jar_cookie(driver)).status_code)
                logging.info("Changing URL: [ %s ]: %s" % (url_f, status, ))
                driver.get(url_f)
                time.sleep(1) # To be sure that page is loaded
                return status
            else:
                logging.info("Unrecognizable url [ %s ]" % (url, ))
                return None

        @classmethod
        def post(cls, driver, url, payload, cookies=None):
            return requests.request("POST", url, verify=cls.ssl_verify, cookies=cookies or cls.jar_cookie(driver), data=payload)

    class Page:

        def __init__(self):
            pass

        @classmethod
        def refresh(cls, driver):
            logging.info("Refreshing current page [%s]" % driver.title)
            driver.refresh()

        @classmethod
        def title(cls, driver) -> str:
            logging.info("The title of the current page is: [%s]" % driver.title)
            return driver.title

        @classmethod
        def current_url(cls, driver) -> str:
            logging.info("Url of the current page is: [%s]" % driver.current_url)
            return driver.current_url

        @classmethod
        def back(cls, driver):
            logging.info("Going to the previous page, current page is: [%s]" % driver.current_url)
            driver.back()
            logging.info("Back command were send, current page is: [%s]" % driver.current_url)

        @classmethod
        def forward(cls, driver):
            logging.info("Going to the next page, current page is: [%s]" % driver.current_url)
            driver.forward()
            logging.info("Forward command were send, current page is: [%s]" % driver.current_url)

        @classmethod
        def make_screenshot(cls, driver, description=None, file_path=SystemVars.system.SCREENSHOT_RELATIVE):
            # browser sending base64 with png -> decode to png bytearray -> convert to jpg bytearray -> out
            logging.info("[ARGS] description %s" % description)
            file_name = "ss_%s.gif" % datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
            logging.info("[SS] Save screenshot to file %s%s" % (file_path, file_name))
            path = "%s%s" % (SystemVars.system.SCREENSHOT_ROOT, file_name)
            img = Image.open(io.BytesIO(driver.get_screenshot_as_png()))
            if img.format == "PNG":
                img.save(path, "GIF", optimize=True, quality=20)
            else:
                raise WebDriverException("Format of the screenshot captured is: " + img.format + ", but expected PNG")
