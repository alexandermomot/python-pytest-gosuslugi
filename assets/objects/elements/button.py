#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import logging
from assets.objects.elements.element import Element


class Button(Element):

    def __init__(self, xpath):
        Element.__init__(self, xpath)

    def click_by_label(self, label):
        xpath="//span[contains(text(),'"+label+"')]"
        if xpath != "":
            logging.info("click %s" % (xpath,))
            self.driver.find_element_by_xpath_and_click(xpath)

    def get_text(self):
        if self.xpath != "":
            elem_text = self.driver.find_element_by_xpath_and_get_text(self.xpath)
            logging.info("Getting text from the element [%s]: [%s]" % (self.xpath, elem_text))
            return elem_text
