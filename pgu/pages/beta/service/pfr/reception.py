#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button
from pgu.pages.beta.service import BaseReceptionPage, BaseClaimTypePage


class ReceptionPage(BaseReceptionPage, BaseClaimTypePage):

    #Pfr
    main_pfr_btn = Button('//*[@id="content"]/div[2]/div[1]/div[5]/div/div[3]/div/div[3]/span')
    main_get_pfr_service_btn = Button('//*[@id="form.FormStep2.PanelForElectrUsluga.Panel75.FormButton76"]/span[2]')
    #Currently service isn't working at all, to be continued