#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import (
    Button, Text )

from pgu.pages import BasePage

class LetterPage(BasePage):

    MailSubject = "//div[@id='mailshowdivhead']//div[@class='row-fluid']//div[contains(text(), '{subject}')]"

    RegMail = Button("//a[contains(., 'Подтвердите регистрацию')]")
    RecoveryMail = Button("//a[contains(., 'Восстановление доступа к учетной записи')]")
    InviteMail = Button("//a[contains(., 'Приглашение присоединиться к организации')]")
    RegLink = Button("//p[contains(., 'registration/?activationCode')]/a")
    RecoveryLink = Button("//p[contains(., 'recovery/password/setNewPassword')]/a")
    InviteLink = Button("//p[contains(., 'user/inviteHandler')]/a")

    def __init__(self, url):
        BasePage.__init__(self, url)

    def try_to_find_subject_of_letter(self, subject):
        """
        :param subject: Text string of an email subject
        :return: text
        """
        mail_subject = Text(LetterPage.MailSubject.format(subject=subject)).get_text()
        return mail_subject
