<!DOCTYPE html>
<head>
  <title>Report</title>
  <style type="text/css">
    body {
		margin: 0px;
		padding: 15px;
    }

	img {
		color: green;
		font:12px;   
	}
	
    body, td, th {
		font-family: Verdana, Helvetica, sans-serif;
		font-size: 8pt;
    }

    th {
		text-align: left;
    }

    h1 {
		font-family: Verdana, Helvetica, sans-serif;
		font-size: 14pt;
		margin-top: 0px;
    }
	
	h3 {
		font-family: Verdana, Helvetica, sans-serif;
		font-size: 12pt;
		margin-top: 0px;
    }
	
	ul {
		width: 100%;
		background: #fafafa;
		list-style: none;
		padding-left: 0;
	}
    
	li {
		padding: 5px;
		border-bottom: 1px solid white;
		border-top: 1px solid #e0e0eb;
		line-height: 15pt;
    }
	
	.param {
		color: #333;
	}

    .change-add {
		padding-left: 10px;
		color: #272;
    }

    .change-delete {
		padding-left: 10px;
		color: #722;
    }

    .change-edit {
		padding-left: 10px;
		color: #247;
    }

    .grayed {
		color: #AAA;
    }

    .error {
		color: #A33;
    }

    pre.console {
		color: #333;
		font-family: "Lucida Console", "Courier New";
		padding: 5px;
		line-height: 15px;
		background-color: #f5f5f5;
		border: 1px solid #DDD;
    }
  </style>
</head>