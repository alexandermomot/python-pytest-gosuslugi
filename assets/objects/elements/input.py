#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import logging
from assets.objects.elements.element import Element


class Input(Element):

    def __init__(self, xpath):
        Element.__init__(self, xpath)

    def send_keys_by_xpath(self, text, ignore_exceptions=False):
        logging.info("Send keys [%s] to element [%s]" % (text, self.xpath))
        self.driver.find_element_by_xpath_and_send_keys(self.xpath, text)

    def send_keys_by_xpath_delay(self, text, delay=0.2):

        """
        Simulates an user input with delay between keystrokes
        @param text text for input
        @param delay delay between keystrokes in seconds (default 0.2)
        """
        logging.info("Send keys [%s] to element [%s] with latency [%s]" % (text, self.xpath, delay))
        self.driver.find_element_by_xpath_and_send_keys_with_latency(self.xpath, text, delay)


    def clear(self):
        logging.info("Clear input [%s] from data" % (self.xpath,))
        self.driver.find_element_by_xpath_and_clear(self.xpath)

    def clear_backspace(self):
        logging.info("Clear input [%s] from data by backspaces" % (self.xpath,))
        self.driver.find_element_by_xpath_and_clear_by_backspaces(self.xpath)

    def get_value(self) -> str:
        elem_value = self.driver.find_element_by_xpath_and_get_text_from_input(self.xpath)
        logging.info("Getting value from the input [%s]: [%s]" % (self.xpath, elem_value))
        return elem_value

    def get_selected(self):
        elem_checkbox = self.driver.find_element_by_xpath_is_selected(self.xpath)
        logging.info("Getting checked or selected state [%s]: [%s]" % (self.xpath, elem_checkbox))

    def set_checked(self):
        elem_state = self.driver.find_element_by_xpath_is_selected(self.xpath)
        if elem_state:
            pass
        else:
            self.driver.find_element_by_xpath_and_click(self.xpath)
            assert self.driver.find_element_by_xpath_is_selected(self.xpath)


    def is_editable(self):
        v = self.driver.editable(self.xpath)
        logging.info("Element editable state: " + str(v))
        return v

    def set_unchecked(self):
        elem_state = self.driver.find_element_by_xpath_is_selected(self.xpath)
        if elem_state:
            self.driver.find_element_by_xpath_and_click(self.xpath)
            assert not self.driver.find_element_by_xpath_is_selected(self.xpath)
        else:
            pass

    def upload_file(self, sfile: str):
        logging.info("Sending file to input [%s]: [%s]" % (self.xpath, sfile))
        self.driver.find_element_by_xpath_and_send_keys_invisible(self.xpath, sfile)
