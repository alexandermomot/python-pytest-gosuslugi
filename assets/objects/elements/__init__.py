#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements.element import Element
from assets.objects.elements.button import Button
from assets.objects.elements.checkbox import Checkbox
from assets.objects.elements.input import Input
from assets.objects.elements.loader import Loader
from assets.objects.elements.radiobutton import Radiobutton
from assets.objects.elements.text import Text
