# #!/usr/bin/python
# # coding=utf-8
# # -*- coding: utf-8 -*-
# __author__ = 'dmitry.khaschinin'
#
# from assets.objects.elements import Button, Input, Text
# from pgu.pages import BasePage
#
#
# class EsiaSecurityPage(BasePage):
#
#     #url = Url("https://esia-portal1.test.gosuslugi.ru/profile/user/security.xhtml")
#
#     #  Настройки учетной записи    #################################
#
#     # Общие
#
#     # Изменение пароля
#
#     ChangePassword = Button(
#         "//form[@id='security']//dl[@class='line-link']//a[contains(text(), 'Изменить пароль')]")  # изменить пароль
#
#     OldPassword = Input("//input[@id='altPwdFrm:oldPwd']")  # Введите старый пароль
#     NewPassword = Input("//input[@id='altPwdFrm:newPwd']")  # Введите новый пароль
#     ConfirmPassword = Input("//input[@id='altPwdFrm:cfrmNewPwd']")  # Подтвердите новый пароль
#
#     UpdatePassword = Button(
#         "//form[@id='altPwdFrm']//div[@class='ui-dialog-buttonpane']//button[@id='altPwdFrm:submit']")  # изменить
#     CancelPassword = Button(
#         "//form[@id='altPwdFrm']//div[@class='ui-dialog-buttonpane']//button[@id='altPwdFrm:cancel']")  # отмена
#
#     # Контрольный вопрос
#
#     AddSecretQA = Button("//span[@id='secretQA']")  # Введите старый пароль
#
#     SecretQuestion = Input("//input[@id='altQAFrm:question']")  # Задайте новый контрольный вопрос
#     SecretAnswer = Input("//input[@id='altQAFrm:answer']")  # Задайте контрольный ответ
#     PasswordQA = Input("//input[@id='altQAFrm:pwd']")  # Введите старый пароль
#
#     EditSecretQA = Button("//form[@id='altQAFrm']//button[contains(., 'Изменить')]")
#
#     # Удаление УЗ
#
#     DeleteProfile = Button(
#         "//form[@id='security']//div[@class='delete-profile-btn']//button[@id='deleteProfileBtn']")  # Удалить учетную запись
#     PassForDeleteProfile = Input("//input[@id='pwd']")  # Введите старый пароль
#     DeletePerson = Button("//button[@id='delPersonBtn']")
#     DeleteProfileOK = Button("//a[@id='logoutLink']")
#
#     # Разрешения
#     # События безопасности
#
#     RegistrationButton = Button("//a[contains(., 'События безопасности')]")
#
#     LastEventLogin = Text("//tbody[@id='evtTbl_data']/tr[1]/td[@class='td-approval-type']")
#
#     @staticmethod
#     def SetNewPassword(old_password, new_password):
#         EsiaSecurityPage.ChangePassword.click()
#         EsiaSecurityPage.OldPassword.send_keys_by_xpath(old_password)
#         EsiaSecurityPage.NewPassword.send_keys_by_xpath(new_password)
#         EsiaSecurityPage.ConfirmPassword.send_keys_by_xpath(new_password)
#         EsiaSecurityPage.UpdatePassword.click()
#
#     @staticmethod
#     def FillSecretQAForm(clearall=False, secretquestion="testq", secretanswer="testa", passwordqa="1234567890"):
#         if clearall:
#             EsiaSecurityPage.SecretQuestion.clear()
#             EsiaSecurityPage.SecretAnswer.clear()
#             EsiaSecurityPage.PasswordQA.clear()
#         else:
#             EsiaSecurityPage.SecretQuestion.send_keys_by_xpath(secretquestion)
#             EsiaSecurityPage.SecretAnswer.send_keys_by_xpath(secretanswer)
#             EsiaSecurityPage.PasswordQA.send_keys_by_xpath(passwordqa)
