#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button, Element, Text
from pgu.pages import BasePage

class BaseClaimTypePage(BasePage):

    OnlineChbx = Button("//ul/*[contains(.,'Электронная услуга')]/a")
    OfflineChbx = Button("//ul/*[contains(.,'Личное посещение')]/a")

    BeginClaimBtn1 = Button("//h2/a[contains(.,'Заполните заявление')]")
    BeginClaimBtn2 = Button("(//*[contains(.,'Получить услугу')]/a)[1]")

    # TODO: when it will be able to use checkboxes
    #OnlineChbx = Button("(//ul[contains(@class,'PGU-FieldRadioList')]//a)[1]")
    #OfflineChbx = Button("(//ul[contains(@class,'PGU-FieldRadioList')]//a)[2]")

    @classmethod
    def online_claim_type(cls):
        cls.OnlineChbx.click()
        cls.BeginClaimBtn1.click()

    @classmethod
    def offline_claim_type(cls):
        cls.OfflineChbx.click()