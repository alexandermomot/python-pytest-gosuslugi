# #!/usr/bin/python
# # coding=utf-8
# # -*- coding: utf-8 -*-
# __author__ = 'dmitry.khaschinin'
#
# import time
#
# from pgu.forms.personalinfo import PersonalInfoPage
# from assets.objects.elements import Button, Input, Text
# from pgu.pages import BasePage
# from pgu.pages.esia import EsiaHomePage
#
#
# class EsiaPersonPage(BasePage):
#
#     #url = Url("https://esia-uat.test.gosuslugi.ru/profile/user/person")
#
#     ExitButton = Button("//a[@title='Выйти']")
#
#     # Top menu
#
#     TabPersonalData = Button("//ul[@class='top-menu']/li/a[contains(text(), 'Персональные данные')]")
#     OrganizationTab = Button("//ul[@class='top-menu']/li/a[contains(text(), 'Организации')]")
#     AccountSettingsTab = Button("//ul[@class='top-menu']/li/a[contains(text(), 'Настройки учетной записи')]")
#
#     # Основная информация
#     EditPerson = Button(
#         "//form[@id='person']//div[@id='person:commonInf']//button[contains(., 'Редактировать')]")
#
#     Birthday = Input("//input[@id='birthday_input']")
#     Birthplace = Input("//textarea[@id='birthplace']")
#     Snils = Input("//input[@id='snils']")
#     RfPasportSerNum = Input("//input[@id='serNum']")
#     RfPassportIssuedOn = Input("//input[@id='issuedOnRfPassport_input']")
#     RfPassportIssuedBy = Input("//textarea[@id='issuedByRfPassport']")
#     IssuerId = Input("//input[@id='issuerId']")
#
#     GenderList = Button("//div[@id='gender']//label[@id='gender_label']")
#     GenderPanelMan = Button("//div[@id='gender_panel']//li[contains(text(), 'Мужской')]")
#     GenderPanelWoman = Button("//div[@id='gender_panel']//li[contains(text(), 'Женский')]")
#     GenderPanelUncertain = Button("//div[@id='gender_panel']//li[contains(text(), 'Не указан')]")
#
#     SaveStep = Button("//div[@class='simple-cover']//div[@id='panelDisabledWrap']//button[@id='save']")
#     NextStep = Button("//div[@class='simple-cover']//div[@id='panelDisabledWrap']//button[@id='next']")
#     UpdatePersonPCD = Button("//form[@id='altCommonInfoFm']//button[contains(., 'Изменить')]")
#
#     # PVF
#
#     ConfirmPerson = Button("//div[@class='up-block motivate']//div[@class='up-buttons shortInfo']/button")
#
#     LinkMore = Button(
#         "//div[@class='up-block motivate']//a[contains(text(), 'Узнать больше о доступных сервисах')]")
#
#     GenderLink = Button("//form[@id='person']//div[@id='person:commonInf']//a[contains(text(), 'Укажите пол')]")
#     BirthdayLink = Button(
#         "//form[@id='person']//div[@id='person:commonInf']//a[contains(text(), 'Укажите дату рождения')]")
#     PlaceOfBirthLink = Button(
#         "//form[@id='person']//div[@id='person:commonInf']//a[contains(text(), 'Укажите место рождения')]")
#
#     GenderListPVF = Button("//form[@id='altCommonInfoFm']//div[@id='gender']/label[@id='gender_label']")
#
#     UpdatePersonPVF = Button("//form[@id='altCommonInfoFm']//button[contains(., 'Изменить')]")  # изменить
#
#     CancelPersonPVF = Button(
#         "//form[@id='altCommonInfoFm']//div[@class='ui-dialog-buttonpane']//button[contains(., 'Отмена')]")  # отмена
#
#     ContinueConfirmSnils = Button("//form[@id='cfmContinue']//button[contains(., 'Запустить проверку данных')]")
#
#     MessageSnilsError = Text(
#         "//div[@id='cfmContinue']//span[contains(text(), 'В системе уже существует подтвержденная учетная запись с этим СНИЛС! Регистрация более одной учётной записи на один СНИЛС невозможна. Пожалуйста, убедитесь, что вы верно ввели СНИЛС.')]")
#
#
#     # ИДЕНТИФИКАТОРЫ
#
#     WgtINN = Button("//div[@id='person:innInf']/a")
#     FindINN = Button("//a[contains(., 'Узнать свой ИНН')]")
#
#     INN = Input("//input[@id='altInnFrm:inn']")
#     UpdateInn = Button("//div[@id='altInn']//button[contains(., 'Изменить')]")
#
#     # Контактная информация
#
#     emailInf = Button("//form[@id='person']//span[@id='person:emailInf']/a")  # Адрес электронной почты
#
#     UpdateEmail = Button(
#         "//form[@id='altEmailFrm']//button[contains(., 'Изменить')]")  # изменить
#     CancelEmail = Button(
#         "//form[@id='altEmailFrm']//button[contains(., 'Отмена')]")  # отмена
#
#     MobileInf = Button("//form[@id='person']//div[@id='person:mobileInf']/a")  # Мобильный телефон в ЛК
#
#     Mobile = Input("//input[@id='mobile']")
#     UpdateMobile = Button(
#         "//form[@id='altMobileFrm']//button[contains(., 'Изменить')]")  # изменить
#     CancelMobile = Button(
#         "//form[@id='altMobileFrm']//div[@class='ui-dialog-buttonpane']//button[contains(., 'Отмена')]")  # отмена
#
#     HomePhoneInf = Button("//form[@id='person']//div[@id='person:phoneInf']/a")  # Домашний телефон
#
#     HomePhone = Input("//input[@id='homePhone']")
#     UpdateHomePhone = Button(
#         "//form[@id='altHomePhoneFrm']//button[contains(., 'Изменить')]")  # изменить
#     CancelHomePhone = Button(
#         "//form[@id='altHomePhoneFrm']//div[@class='ui-dialog-buttonpane']//button[contains(., 'Отмена')]")  # отмена
#
#     RegAddrInf = Button("//form[@id='person']//div[@id='person:regAddrInf']/a")  # Адрес регистрации
#     LiveAddrInf = Button("//form[@id='person']//div[@id='person:liveAddrInf']/a")  # Адрес места проживания
#
#     Address = Input("//form[@id='altAddrFrm']//div[@id='addrGroup']//textarea[@id='address']")  # Адрес
#
#     AddressList1 = Button(
#         "//div[@class='pui-autocomplete-panel ui-widget-content ui-corner-all ui-helper-hidden pui-shadow']//ul/li[1]")
#     AddressList2 = Button("//div//ul//li[contains(text(), 'Воронеж город, Воронежская улица')]")
#     AddressList3 = Button(
#         "//div[@class='pui-autocomplete-panel ui-widget-content ui-corner-all ui-helper-hidden pui-shadow']//ul/li[3]")
#
#     House = Input("//form[@id='altAddrFrm']//div[@id='addrGroup']//input[@id='house']")  # Дом
#     Frame = Input("//form[@id='altAddrFrm']//div[@id='addrGroup']//input[@id='frame']")  # Корпус
#     Build = Input("//form[@id='altAddrFrm']//div[@id='addrGroup']//input[@id='build']")  # Строение
#     Flat = Input("//form[@id='altAddrFrm']//div[@id='addrGroup']//input[@id='flat']")  # Квартира
#     ZipCode = Input("//form[@id='altAddrFrm']//div[@id='addrGroup']//input[@id='zipCode']")  # Индекс
#     BoxNotFlat = Button(
#         "//form[@id='altAddrFrm']//div[@id='hasNotFlat']//span[@class='ui-chkbox-icon']")  # Нет номера квартиры
#     IndexRef = Button(
#         "//form[@id='altAddrFrm']//a[contains(text(), 'Не помните индекс?')]")  # Забыли индекс?
#
#     EditAddress = Button(
#         "//form[@id='altAddrFrm']//button[contains(., 'Изменить')]")  # изменить
#     CancelAddress = Button(
#         "//form[@id='altAddrFrm']//button[contains(., 'Отмена')]")  # отмена
#     DeleteAddress = Button(
#         "//form[@id='altAddrFrm']//button[contains(., 'Удалить')]")  # удалить
#
#     ErrorMessageAddrSityStreetEmpty = Text(
#         "//form[@id='altAddrFrm']//div[@id='msg_address']//span[contains(text(), 'Введите адрес')]")
#     ErrorMessageAddrHouseEmpty = Text(
#         "//form[@id='altAddrFrm']//div[@id='msg_house']//span[contains(text(), 'Введите дом, корпус или строение')]")
#     ErrorMessageAddrSityFlatEmpty = Text(
#         "//form[@id='altAddrFrm']//div[@id='msg_flat']//span[contains(text(), 'Введите квартиру')]")
#     ErrorMessageAddrSityZipEmpty = Text(
#         "//form[@id='altAddrFrm']//div[@id='msg_zipCode']//span[contains(text(), 'Введите индекс')]")
#
#     InfMessageAddrSityStreet = Text(
#         "//form[@id='altAddrFrm']//div[@id='msg_address_inner']//span[contains(text(), 'При вводе адреса вы не следовали подсказкам, предлагаемым на основе базы данных адресов России.  Пожалуйста, убедитесь, что адрес введен корректно. Если вы уверены в правильности указания адреса, то можете не менять его.')]")
#
#     # Форма "Обновление адреса
#
#     UpdateAddress = Button(
#         "//form[@id='syncAddrsFrm']//button[contains(., 'Обновить')]")  # обновить
#     CancelUpdateAddress = Button(
#         "//form[@id='syncAddrsFrm']//button[contains(., 'Отмена')]")  # отмена
#
#     # Документы
#
#     DriverLicenseInf = Button("//form[@id='person']//div[@id='person:drLicenseInf']/a")
#
#     DL_serNum = Input("//input[@id='altDrLicenceFrm:serNum']")  # Серия и номер
#     DL_issuedOn = Input("//input[@id='altDrLicenceFrm:issuedOn_input']")  # Дата выдачи
#     DL_expiryDate = Input("//input[@id='altDrLicenceFrm:expiryDate_input']")  # Действительно до
#
#     VU = Button("//span[contains(text(), 'Водительское удостоверение')]")
#
#     ErrorMessageDLSerNumEmpty = Text(
#         "//form[@id='altDrLicenceFrm']//span[contains(text(), 'Введите серию и номер')]")
#     ErrorMessageDLIssuedOnEmpty = Text(
#         "//form[@id='altDrLicenceFrm']//span[contains(text(), 'Введите дату выдачи')]")
#     ErrorMessageDLExpiryDateEmpty = Text(
#         "//form[@id='altDrLicenceFrm']//span[contains(text(), 'Введите срок действия водительского удостоверения')]")
#
#     ErrorMessageDLIssuedOnWrong = Text(
#         "//form[@id='altDrLicenceFrm']//span[contains(text(), 'Некорректная дата выдачи документа')]")
#     ErrorMessageDLExpiryDateWrong = Text(
#         "//form[@id='altDrLicenceFrm']//span[contains(text(), 'Некорректная дата окончания срока действия документа')]")
#
#     ErrorMessageDLIssuedOnIncorrect = Text(
#         "//form[@id='altDrLicenceFrm']//span[contains(text(), 'Укажите дату выдачи в формате дд.мм.гггг')]")
#     ErrorMessageDLExpiryDateIncorrect = Text(
#         "//form[@id='altDrLicenceFrm']//span[contains(text(), 'Укажите дату окончания срока действия в формате дд.мм.гггг')]")
#
#     UpdateDL = Button(
#         "//form[@id='altDrLicenceFrm']//button[contains(., 'Изменить')]")  # изменить
#     CancelDL = Button(
#         "//form[@id='altDrLicenceFrm']//button[contains(., 'Отмена')]")  # отмена
#     DeleteDL = Button(
#         "//form[@id='altDrLicenceFrm']//button[contains(., 'Удалить')]")  # удалить
#
#     # Транспортные средства
#     # Добавить транспортное средство
#     AddVehicles = Button(
#         "//form[@id='person']//div[@class='data-bar']//button[contains(., 'Добавить транспортное средство')]")
#
#     Vehicle = Button("//div[@id='person:vehicleInf']//div[@id='person:vehicles_content']//a[1]")  # первое ТС
#
#     VhlName = Input("//input[@id='vhlName']")  # Название ТС
#     NumberPlate = Input("//input[@id='numberPlate']")  # Государственный регистрационный знак
#     RegCertSerNum = Input("//input[@id='regCertSerNum']")  # Серия и номер свидетельства о регистрации
#
#     UpdateVhl = Button("//form[@id='vehicleFm']//button[contains(., 'Добавить')]")  # изменить
#     CancelVhl = Button("//form[@id='vehicleFm']//button[contains(., 'Отмена')]")  # отмена
#     DeleteVhl = Button("//form[@id='vehicleFm']//button[contains(., 'Удалить')]")  # удалить
#
#     BirthCertInf = Button("//div[@id='person:birthCertInf']//a")  # Свидетельство о рождении
#
#     BirthSeries = Input("//input[@id='birthSeries']")
#     BirthNumber = Input("//input[@id='birthNumber']")
#     BirthIssuedOnForeigner = Input("//input[@id='birthIssuedOnForeigner_input']")
#     BirthIssuedBy = Input("//textarea[@id='birthIssuedBy']")
#
#     UpdateBirthCert = Button(
#         "//form[@id='birthCertificateFrm']//button[contains(., 'Изменить')]")  # изменить
#
#     DeleteBirthCert = Button(
#         "//form[@id='birthCertificateFrm']//button[contains(., 'Удалить')]")  # удалить
#
#     @classmethod
#     def FillFormBC(cls, clearall=False, BirthSeries=u"АБВ", BirthNumber="123456",
#                    BirthIssuedOnForeigner="30.09.2015", BirthIssuedBy=u"Тестом"):
#         if clearall:
#             cls.BirthSeries.clear()
#             cls.BirthNumber.clear_backspace()
#             cls.BirthIssuedOnForeigner.clear()
#             cls.BirthIssuedBy.clear()
#         else:
#             cls.BirthSeries.send_keys_by_xpath_delay(BirthSeries)
#             cls.BirthNumber.send_keys_by_xpath(BirthNumber)
#             cls.BirthIssuedOnForeigner.send_keys_by_xpath(BirthIssuedOnForeigner)
#             cls.BirthIssuedBy.send_keys_by_xpath(BirthIssuedBy)
#
#     PersonMipInf = Button("//div[@id='person:mipInf']//a")  # Полис ОМС
#
#     mipNumber = Input("//input[@id='mipNumber']")
#     mipExpiryDate = Input("//input[@id='mipExpiryDate_input']")
#
#     UpdatePersonMip = Button(
#         "//form[@id='mipFrm']//button[contains(., 'Изменить')]")  # изменить
#
#     DeletePersonMip = Button(
#         "//form[@id='mipFrm']//button[contains(., 'Удалить')]")  # удалить
#
#     @classmethod
#     def FillFormMip(cls, clearall=False, mipNumber="1234567890123456", mipExpiryDate="10.10.2020"):
#         if clearall:
#             cls.mipNumber.clear()
#             cls.mipExpiryDate.clear_backspace()
#         else:
#             cls.mipNumber.send_keys_by_xpath_delay(mipNumber)
#             cls.mipExpiryDate.send_keys_by_xpath(mipExpiryDate)
#
#     MltrInf = Button("//div[@id='person:mltrInf']//a")  # Военный Билет
#
#     mltrSeries = Input("//input[@id='mltrSeries']")
#     mltrNumber = Input("//input[@id='mltrNumber']")
#     mltrIssueDate = Input("//input[@id='mltrIssueDate_input']")
#     mltrIssuedBy = Input("//textarea[@id='mltrIssuedBy']")
#
#     UpdateMltrInf = Button(
#         "//form[@id='mltrFrm']//button[contains(., 'Изменить')]")  # изменить
#
#     DeleteMltr = Button(
#         "//form[@id='mltrFrm']//button[contains(., 'Удалить')]")  # удалить
#
#     @classmethod
#     def FillFormMltr(cls, clearall=False, mltrSeries=u"АБВ", mltrNumber="123456",
#                    mltrIssueDate="30.09.2015", mltrIssuedBy=u"Тестом"):
#         if clearall:
#             cls.mltrSeries.clear()
#             cls.mltrNumber.clear_backspace()
#             cls.mltrIssueDate.clear()
#             cls.mltrIssuedBy.clear()
#         else:
#             cls.mltrSeries.send_keys_by_xpath_delay(mltrSeries)
#             cls.mltrNumber.send_keys_by_xpath(mltrNumber)
#             cls.mltrIssueDate.send_keys_by_xpath(mltrIssueDate)
#             cls.mltrIssuedBy.send_keys_by_xpath(mltrIssuedBy)
#
#
#     FrgnPassportInf = Button("//div[@id='person:frgnPassportInf']//a")  # Загранпаспорт
#
#     FrgnLastName = Input("//input[@id='frgnLastName']")
#     FrgnFirstName = Input("//input[@id='frgnFirstName']")
#     FrgnSerNum = Input("//input[@id='frgnSerNum']")
#     FrgnIssueOn = Input("//input[@id='frgnIssueOn_input']")
#     FrgnExpiryDate = Input("//input[@id='frgnExpiryDate_input']")
#     FrgnIssuedBy = Input("//textarea[@id='frgnIssuedBy']")
#
#     UpdateFrgnPassport = Button(
#         "//form[@id='foreignPassportFrm']//button[contains(., 'Изменить')]")  # изменить
#
#     DeleteFrgnPassport = Button(
#         "//form[@id='foreignPassportFrm']//button[contains(., 'Удалить')]")  # удалить
#
#     ZP = Button("//span[contains(text(), 'Заграничный паспорт')]")
#
#     @classmethod
#     def FillFormFrgnPassport(cls, clearall=False, FrgnLastName="Vrag", FrgnFirstName="Naroda",
#                    FrgnSerNum="12 3456789", FrgnIssueOn="30.09.2015", FrgnExpiryDate="30.09.2030", FrgnIssuedBy="USA"):
#         if clearall:
#             cls.FrgnIssueOn.clear()
#             cls.ZP.click(method="classic")
#             EsiaHomePage.Datepicker.is_invisible()
#             cls.FrgnLastName.clear()
#             cls.FrgnExpiryDate.clear()
#             cls.ZP.click(method="classic")
#             EsiaHomePage.Datepicker.is_invisible()
#             cls.FrgnFirstName.clear_backspace()
#             cls.FrgnSerNum.clear()
#             cls.FrgnIssuedBy.clear()
#         else:
#             cls.FrgnIssueOn.send_keys_by_xpath(FrgnIssueOn)
#             cls.ZP.click(method="classic")
#             EsiaHomePage.Datepicker.is_invisible()
#             cls.FrgnLastName.send_keys_by_xpath_delay(FrgnLastName)
#             cls.FrgnFirstName.send_keys_by_xpath(FrgnFirstName)
#             cls.FrgnSerNum.send_keys_by_xpath(FrgnSerNum)
#             cls.FrgnExpiryDate.send_keys_by_xpath(FrgnExpiryDate)
#             cls.ZP.click(method="classic")
#             EsiaHomePage.Datepicker.is_invisible()
#             cls.FrgnIssuedBy.send_keys_by_xpath(FrgnIssuedBy)
#
#     @classmethod
#     def GoToEsiaPageProfileUserPerson(cls):
#         cls.TabPersonalData.is_visible()
#         cls.TabPersonalData.click()
#
#     @classmethod
#     def GoToEsiaPageProfileUserSecurity(cls):
#         cls.AccountSettingsTab.is_visible()
#         cls.AccountSettingsTab.click()
#
#     @classmethod
#     def GoToEsiaPageProfileUserEmps(cls):
#         cls.OrganizationTab.click()
#
#     @classmethod
#     def FillFormDL(cls, clearall=False, DLSerNum="6666666666", DLIssuedOn="01.09.2015", DLExpiryDate="30.09.2015"):
#         if clearall:
#             cls.DL_serNum.clear_backspace()
#             cls.DL_issuedOn.clear()
#             cls.VU.click(method="classic")
#             EsiaHomePage.Datepicker.is_invisible()
#             cls.DL_expiryDate.clear()
#             cls.VU.click(method="classic")
#             EsiaHomePage.Datepicker.is_invisible()
#         else:
#             cls.DL_issuedOn.send_keys_by_xpath(DLIssuedOn)
#             cls.VU.click(method="classic")
#             EsiaHomePage.Datepicker.is_invisible()
#             cls.DL_serNum.send_keys_by_xpath_delay(DLSerNum)
#             cls.DL_expiryDate.send_keys_by_xpath(DLExpiryDate)
#             cls.VU.click(method="classic")
#             EsiaHomePage.Datepicker.is_invisible()
#
#     @classmethod
#     def DeleteDriverLicense(cls):
#         cls.DriverLicenseInf.click()
#         time.sleep(2)
#         cls.DeleteDL.click()
#
#     @classmethod
#     def ChooseOptionFromListFIAS(cls, streetadnzip):
#         Button("//div//ul//li[contains(., '" + streetadnzip + "')]").click()
#
#     @classmethod
#     def FillFormRegAddress(cls, clearall=False, sitystreet=u"Тамбов", house="1", frame="2", build="3", flat="4",
#                            streetadnzip=u"Тамбовская область, Тамбовский район"):
#         if clearall:
#             cls.Address.clear()
#             cls.House.clear()
#             cls.Frame.clear()
#             cls.Build.clear()
#             cls.Flat.clear()
#             cls.ZipCode.clear_backspace()
#         else:
#             cls.Address.send_keys_by_xpath(sitystreet)
#             time.sleep(2)
#             cls.ChooseOptionFromListFIAS(streetadnzip)
#             cls.House.send_keys_by_xpath(house)
#             cls.Frame.send_keys_by_xpath(frame)
#             cls.Build.send_keys_by_xpath(build)
#             cls.Flat.send_keys_by_xpath(flat)
#
#     @classmethod
#     def DeleteRegAddress(cls):
#         cls.RegAddrInf.click()
#         cls.DeleteAddress.click()
#
#     @classmethod
#     def DeleteLiveAddress(cls):
#         cls.LiveAddrInf.click()
#         cls.DeleteAddress.click()
#
#     @classmethod
#     def FillFormVehicle(cls, clearall=False, vhlname=u"Верблюд", numberplate=u"TT123T45", regcertsernum="1234 567890"):
#         if clearall:
#             cls.VhlName.clear()
#             cls.NumberPlate.clear()
#             cls.RegCertSerNum.clear_backspace()
#         else:
#             cls.VhlName.send_keys_by_xpath(vhlname)
#             cls.NumberPlate.send_keys_by_xpath(numberplate)
#             cls.RegCertSerNum.send_keys_by_xpath_delay(regcertsernum)
#
#     @classmethod
#     def DeleteVehicle(cls):
#         cls.Vehicle.click()
#         cls.DeleteVhl.click()
#
#     @classmethod
#     def FillFormPVF(cls, clearall=False, surname=u"Хащинина", name=u"Светлана", midlename=u"Александровна",
#                     birthday="10.01.1971", birthplace=u"Воронеж"):
#         if clearall:
#             PersonalInfoPage.LastName.clear()
#             PersonalInfoPage.FirstName.clear()
#             PersonalInfoPage.Midlename.clear()
#             cls.Birthday.clear()
#             cls.Birthplace.clear()
#             cls.GenderListPVF.click()
#             cls.GenderPanelUncertain.click()
#         else:
#             PersonalInfoPage.LastName.send_keys_by_xpath(surname)
#             PersonalInfoPage.FirstName.send_keys_by_xpath(name)
#             PersonalInfoPage.Midlename.send_keys_by_xpath(midlename)
#             cls.Birthday.send_keys_by_xpath(birthday)
#             cls.Birthplace.send_keys_by_xpath(birthplace)
#             cls.GenderListPVF.click()
#             cls.GenderPanelWoman.click()
#
#     @classmethod
#     def FillFormRusPVD(cls, clearall=False, surname=u"Хащинина", name=u"Светлана", midlename=u"Александровна",
#                        birthday="10.01.1971",
#                        birthplace=u"Воронеж", snils="00000000054", rfpasportsernum="2003157443",
#                        rfpassportissuedon="10.08.2002",
#                        rfpassportissuedby=u"ТП УФМС", issuerid="123456"):
#         if clearall:
#             cls.RfPassportIssuedOn.clear()
#             cls.Birthday.clear()
#             PersonalInfoPage.LastName.clear()
#             PersonalInfoPage.FirstName.clear()
#             PersonalInfoPage.Midlename.clear()
#             cls.Birthplace.clear()
#             cls.Snils.clear_backspace()
#             cls.RfPasportSerNum.clear_backspace()
#             cls.RfPassportIssuedBy.clear()
#             cls.IssuerId.clear()
#             cls.GenderList.click()
#             cls.GenderPanelUncertain.click()
#         else:
#             cls.RfPassportIssuedOn.send_keys_by_xpath(rfpassportissuedon)
#             cls.Birthday.send_keys_by_xpath(birthday)
#             PersonalInfoPage.LastName.send_keys_by_xpath(surname)
#             PersonalInfoPage.FirstName.send_keys_by_xpath(name)
#             PersonalInfoPage.Midlename.send_keys_by_xpath(midlename)
#             cls.Birthplace.send_keys_by_xpath(birthplace)
#             cls.GenderList.click()
#             cls.GenderPanelWoman.click()
#             cls.Snils.send_keys_by_xpath_delay(snils)
#             cls.RfPasportSerNum.send_keys_by_xpath_delay(rfpasportsernum)
#             cls.RfPassportIssuedBy.send_keys_by_xpath(rfpassportissuedby)
#             cls.IssuerId.send_keys_by_xpath(issuerid)
#
#     @classmethod
#     def FillFormRusPCD(cls, clearall=False, surname=u"Пушкин", name=u"Александр", midlename=u"Сергеевич",
#                        birthday="06.06.1799",
#                        birthplace=u"Воронеж", rfpasportsernum="6666131313",
#                        rfpassportissuedon="10.08.2002",
#                        rfpassportissuedby=u"Империя", issuerid="123456"):
#         if clearall:
#             cls.RfPassportIssuedOn.clear()
#             cls.Birthday.clear()
#             PersonalInfoPage.LastName.clear()
#             PersonalInfoPage.FirstName.clear()
#             PersonalInfoPage.Midlename.clear()
#             cls.Birthplace.clear()
#             cls.RfPasportSerNum.clear_backspace()
#             cls.RfPassportIssuedBy.clear()
#             cls.IssuerId.clear()
#             cls.GenderList.click()
#         else:
#             cls.RfPassportIssuedOn.send_keys_by_xpath(rfpassportissuedon)
#             cls.Birthday.send_keys_by_xpath(birthday)
#             PersonalInfoPage.LastName.send_keys_by_xpath(surname)
#             PersonalInfoPage.FirstName.send_keys_by_xpath(name)
#             PersonalInfoPage.Midlename.send_keys_by_xpath(midlename)
#             cls.Birthplace.send_keys_by_xpath(birthplace)
#             cls.GenderList.click()
#             cls.GenderPanelWoman.click()
#             cls.RfPasportSerNum.send_keys_by_xpath_delay(rfpasportsernum)
#             cls.RfPassportIssuedBy.send_keys_by_xpath(rfpassportissuedby)
#             cls.IssuerId.send_keys_by_xpath(issuerid)
#
#     @classmethod
#     def FillFormHomePhone(cls, clearall=False, homephone="74732121212"):
#         if clearall:
#             cls.HomePhone.clear_backspace()
#         else:
#             cls.HomePhone.send_keys_by_xpath_delay(homephone)
#
#     @classmethod
#     def DeleteHomePhone(cls):
#         cls.HomePhoneInf.click()
#         cls.HomePhone.clear_backspace()
#         cls.UpdateHomePhone.click()
