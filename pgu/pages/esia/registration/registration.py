#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button, Input, Text
from pgu.pages import BasePage


class EsiaRegistrationPage(BasePage):

    #url = Url("https://esia-uat.test.gosuslugi.ru/registration")

    Name = Input("//input[@id='name']")
    Surname = Input("//input[@id='lastName']")
    MobilePhone = Input("//input[@id='phone']")
    Email = Input("//input[@id='email']")
    SubmitRegistration = Button("//button[@id='tryReg']/span[contains(text(), 'Зарегистрироваться')]/ancestor::button")
    GoToEsiaLoginPage = Button("//a[contains(text(), 'Войти')]")

    PassInput = Input("//input[@id='password']")
    PassConfirmInput = Input("//input[@id='confirm-password']")

    SaveRegistration = Button("//button[@id='save']")
    ToTryViaEmail = Button("//a[contains(text(), 'Попробовать через электронную почту')]")

    TermsOfUse = Button("//a[contains(text(), 'Условиями использования')]")
    PrivacyPolicy = Button("//a[contains(text(), 'Политикой конфиденциальности')]")

    PopUpCloseButtonTerms = Button("//div[contains(@id,'j_idt')]//span[text()='Условия использования']/ancestor::div//button")
    PopUpCloseCrossTerms = Button("//div[contains(@id,'j_idt')]//span[text()='Условия использования']/ancestor::node()/a")
    PopUpCloseButtonPrivacy = Button("//div[contains(@id,'j_idt')]//span[text()='Политика конфиденциальности']/ancestor::div//button")
    PopUpCloseCrossPrivacy = Button("//div[contains(@id,'j_idt')]//span[text()='Политика конфиденциальности']/ancestor::node()/a")

    PopUpMobilePhoneTip = Text("//div[@id='ui-tooltip-j_idt85-content']")
    PopUpEmailTip = Text("//div[@id='ui-tooltip-j_idt96-content']")

    RegistrationByDocumentationCenter = Button("//a[contains(text(), 'центре обслуживания')]")

    ErrorFieldFirstNameRequired = Text("//div[@class='field-error']/div[@id='msg_name']")
    ErrorFieldLastNameRequired = Text("//div[@class='field-error']/div[@id='msg_lastName']")
    ErrorFieldPhoneOrEmailRequired = Text("//div[@class='line-errs']/div[@id='msg_cttInputs']")

    @classmethod
    def FillRegisterForm(cls, clearall=False, name="test", surname="test", mobilephone="1234567890", email="test@test.ru"):
        if clearall:
            cls.Name.clear()
            cls.Surname.clear()
            cls.MobilePhone.clear()
            cls.Email.clear()
        else:
            cls.Name.send_keys_by_xpath(name)
            cls.Surname.send_keys_by_xpath(surname)
            cls.MobilePhone.send_keys_by_xpath(mobilephone)
            cls.Email.send_keys_by_xpath(email)

    @classmethod
    def OpenAndClosePrivacyPolicy(cls):
        cls.PrivacyPolicy.click()
        cls.PopUpCloseButtonPrivacy.click()
        cls.PrivacyPolicy.click()
        cls.PopUpCloseCrossPrivacy.click()


    @classmethod
    def OpenAndCloseTermsOfUse(cls):
        cls.TermsOfUse.click()
        cls.PopUpCloseButtonTerms.click()
        cls.TermsOfUse.click()
        cls.PopUpCloseCrossTerms.click()


    @classmethod
    def GoToRegisterByDocumentationCenterPage(cls):
        cls.RegistrationByDocumentationCenter.click()




