#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import random
import pytest
import logging
import requests
import json
from requests.auth import HTTPBasicAuth
from _pytest.python import SubRequest
from assets.driver import DriverStorage
from pgu.tests.conftest import auto_screenshot, Helper, step, attach, TEXT, JSON


@pytest.yield_fixture(scope="function", autouse=True)
def prepare_driver(request: SubRequest, url):
    config = request.config
    browser = config.getoption("--browser")
    helper = Helper(DriverStorage.create(browser, browser), url)
    helper.settings()
    helper.ref_creator()
    helper.beta_start()
    yield
    if request.node.rep_setup.passed:
        if request.node.rep_call.failed:
            auto_screenshot(helper.get_engine(), "Failed")
        elif request.node.rep_call.passed:
            auto_screenshot(helper.get_engine(), "Passed")
    DriverStorage.kill(browser)


@pytest.yield_fixture(scope="function", autouse=True)
def mocks(request: SubRequest):
    config = request.config
    state = config.getoption("--mocks")
    url = "http://egovdev:egovdev@mocks.testgate.egovdev.ru/status"
    demand_state = lambda x: {"pgu": {"beta": {"emulation": x}}}
    if state:
        with step("Опрос заглушки"):
            status = json.loads(requests.post(url).text)
            previous = status['pgu']['beta']['emulation']
            attach("Ответ от заглушки", json.dumps(status), type=JSON)
        if previous is False:
            with step("Включение заглушек"):
                attach("Ответ от заглушки", requests.post(url, json=demand_state(state)).text, type=JSON)
        yield
        with step("Возврат в начальное состояние заглушек"):
            attach("Ответ от заглушки", requests.post(url, json=demand_state(previous)).text, type=JSON)
    else:
        yield


@pytest.fixture
def num() -> int:
    return random.random


@pytest.yield_fixture(scope="session", autouse=True)
def renamer(request):
    yield
    config = request.config
    suffix = 'suffix'
    if hasattr(config, '_allurelistener'):
        config._allurelistener.impl.testsuite.name += '[%s]' % suffix


class Attachments():
    """
    source holds FS path to the data, type -- MIME-type of the contents
    """