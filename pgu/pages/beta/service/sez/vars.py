#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

class Vars:
    # ИП Николаев Н.Н.
    esia_snils_ip = "00000000057"
    esia_pass_ip = "11111111"

    # Почтовый адрес
    searchinput_address = "Москва Хуторская "
    post_address = "ул. 2 Хуторская, д. 38А, стр. 15, этаж 4"

    # Контактные данные лица и отделение роспотребнадзора
    personal_f = "Николаев"
    personal_i = "Николай"
    personal_o = "Николаевич"
    personal_email = "pgu@mailforspam.com"
    personal_tel = "70000000000"

    # Отделение куда направляется заявление
    searchinput_sanepidemnadzor_department_name = "Москв"
    ip_company_name = "Рога и копыта"

    # Наименование проектной документации
    sanepidemnadzor_documentation_names = "Документ 1"

    ##########################################################

    # ООО "Балтинформ"
    esia_snils_le = "02539655062"
    esia_pass_le = "11111111"

