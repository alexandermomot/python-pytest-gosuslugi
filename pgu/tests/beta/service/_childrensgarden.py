# # -*- coding: utf-8 -*-
# __author__ = 'Nadezhda Vavilova'
#
# import time
#
# from pgu.pages.beta.core.home import BetaHomePage
# from pgu.pages.beta.service.childrensgarden.application import ApplicationPage
#
# #from beta_pgu.vars import Vars
# #from utils.native_handling.upload_dialog import UploadDialogHandler
#
# class Kindergarten:
#     u""" Sanity-тестирование услуги "Запись ребёнка в детский сад" """
#
#     #BEGIN authorization
#     def test_step01_load_page_and_go_to_lk(self):
#         engine.get(self.URL)
#         BetaHomePage.lk_button_small.click()
#
#     def test_step02_click_another_user(self):
#         if BetaHomePage.lk_another_user.is_exists():
#             BetaHomePage.lk_another_user.click()
#
#     def test_step03_click_snils_btn(self):
#         if BetaHomePage.lk_snils_btn.is_exists():
#             BetaHomePage.lk_snils_btn.click()
#
#     def test_step04_click_snils_input(self):
#         BetaHomePage.lk_snils_input.click()
#
#     def test_step05_enter_snils(self):
#         BetaHomePage.lk_snils_input.send_keys_by_xpath('00000003333')
#         #BetaHome.lk_snils_input.send_keys_by_xpath('11223344696')
#
#     def test_step06_click_password_input(self):
#         BetaHomePage.lk_password_input.click()
#
#     def test_step07_enter_password(self):
#         BetaHomePage.lk_password_input.send_keys_by_xpath('qwerty12345')
#         #BetaHome.lk_password_input.send_keys_by_xpath('11111111')
#
#     def test_step08_click_to_login(self):
#         BetaHomePage.lk_login_btn.click()
#
#     def test_step09_click_first_option(self):
#         BetaHomePage.lk_role_first.click()
#     #END authorization
#
#     #START service
#     def test_step10_click_main_service_btn(self):
#         ApplicationPage.kindergarten_main_button.click()
#
#     #--START select region
#     def test_step11_click_select_region_btn(self):
#         BetaHomePage.region_select_button.click()
#
#     def test_step12_select_radio_btn(self):
#         BetaHomePage.region_select_radio.click()
#
#     def test_step13_select_region_btn(self):
#         for i in u'Санкт':
#             BetaHomePage.region_name_inp.send_keys_by_xpath(i)
#
#     def test_step14_select_name_region_btn(self):
#         BetaHomePage.region_name_button.click()
#
#     def test_step15_save_region_btn(self):
#         BetaHomePage.region_save_button.click()
#     #--END select region
#
#     def test_step16_select_variant_submit_btn(self):
#         ApplicationPage.kindergarten_apply_order_button.click()
#
#     def test_step17_click_fill_application_btn(self):
#         ApplicationPage.kindergarten_fill_application_button.click()
#
#     def test_step18_click_create_new_order_btn(self):
#         if BetaHomePage.create_new_form.is_exists():
#             BetaHomePage.create_new_form.click()
#
#     #--START Fill step
#     #----START Panel 4 - Personal data of the child
#     def test_step19_enter_family_child_inp(self):
#         ApplicationPage.kindergarten_family_child.send_keys_by_xpath(u'Тестовый')
#
#     def test_step20_enter_name_child_inp(self):
#         ApplicationPage.kindergarten_name_child.send_keys_by_xpath(u'Тест')
#
#     def test_step21_enter_patronymic_child_inp(self):
#         ApplicationPage.kindergarten_patronymic_child.send_keys_by_xpath(u'Тестович')
#
#     def test_step22_enter_birthday_child_inp(self):
#         ApplicationPage.kindergarten_birthday_child.send_keys_by_xpath('01.01.2012')
#
#     def test_step23_select_sex_child_btn(self):
#         ApplicationPage.kindergarten_sex_child.click()
#
#     def test_step24_enter_snils_child_inp(self):
#         ApplicationPage.kindergarten_snils_child.send_keys_by_xpath('123-123-123 00')
#     #----END Panel 4 - Personal data of the child
#
#     #----START Panel 5 -  birth certificate
#     def test_step25_enter_serial_birth_cert_inp(self):
#         ApplicationPage.kindergarten_serial_birth_cert.send_keys_by_xpath(u'VII-ЛД')
#
#     def test_step26_enter_number_birth_cert_inp(self):
#         BetaKindergarten.kindergarten_number_birth_cert.send_keys_by_xpath('123123')
#
#     def test_step27_enter_date_of_issue_birth_cert_inp(self):
#         BetaKindergarten.kindergarten_date_of_issue_birth_cert.send_keys_by_xpath('01.02.2012')
#
#     def test_step28_enter_num_of_assembly_record_birth_cert_inp(self):
#         BetaKindergarten.kindergarten_num_of_assembly_record_birth_cert.send_keys_by_xpath('123123')
#
#     def test_step29_enter_issued_birth_cert_inp(self):
#         BetaKindergarten.kindergarten_issued_birth_cert.send_keys_by_xpath(u'Место выдачи')
#
#     def test_step30_enter_birthplace_birth_cert_inp(self):
#         BetaKindergarten.kindergarten_birthplace_birth_cert.send_keys_by_xpath(u'Москва')
#     #----END Panel 5 -  birth certificate
#
#     #----START Panel 6 -  Registered Address ---FIAS
#     def test_step31_enter_region_name_inp(self):
#         for i in u'Санть':
#             BetaKindergarten.kindergarten_region_name_input.send_keys_by_xpath(i)
#
#     def test_step32_select_region_address_registration_btn(self):
#         time.sleep(3) #solution unstable problem with FIAS
#         BetaKindergarten.kindergarten_region_name_address_dropdown.click()
#
#     def test_step33_enter_address_house_inp(self):
#         BetaKindergarten.kindergarten_house_input.send_keys_by_xpath('15')
#
#     def test_step34_enter_address_apartment_inp(self):
#         BetaKindergarten.kindergarten_apartment_input.send_keys_by_xpath('16')
#     #----END Panel 6 -  Registered Address ---FIAS
#
#     #----START Panel 8 -  Select kindergarten on the map
#     def test_step35_add_first_kindergarten_btn(self):
#         BetaKindergarten.kindergarten_add_first_btn.click()
#
#     def test_step36_add_second_kindergarten_btn(self):
#         BetaKindergarten.kindergarten_add_second_btn.click()
#     #----START Panel 8 -  Select kindergarten on the map
#
#     #----START Panel 9 -  Options
#     def test_step37_click_date_btn(self):
#         BetaKindergarten.kindergarten_click_date_btn.click()
#
#     def test_step38_select_date_btn(self):
#         BetaKindergarten.kindergarten_select_date_btn.click()
#
#     def test_step39_click_specification_btn(self):
#         BetaKindergarten.kindergarten_click_specification_btn.click()
#
#     def test_step40_select_specification_btn(self):
#         time.sleep(3)
#         BetaKindergarten.kindergarten_select_specification_btn.click()
#
#     def test_step41_input_requisites_inp(self):
#         BetaKindergarten.kindergarten_requisites_inp.send_keys_by_xpath(u'Мои реквизиты')
#
#     def test_step42_select_benefits_btn(self):
#         BetaKindergarten.kindergarten_benefits_radio_btn.click()
#
#     #----------------BENEFITS does not work on form
#     # def test_step43_click_benefits_list_btn(self):
#     #     BetaKindergarten.kindergarten_click_benefits_list_btn.click()
#     #
#     # def test_step44_select_benefits_list_btn(self):
#     #     BetaKindergarten.kindergarten_select_benefits_list_btn.click()
#     #
#     # def test_step45_input_requisites_benefits_inp(self):
#     #     BetaKindergarten.kindergarten_benefits_requisites_inp.send_keys_by_xpath(u'Мои реквизиты2')
#
#     #----END Panel 9 -  Options
#
#     #----START Panel 10 -  Load Files
#     # def test_step46_load_file(self):
#     #     BetaKindergarten.kindergarten_load_photo_btn.click()
#     #     UploadDialogHandler.upload_photo(Vars.DATA_ROOT + 'passport1')
#
#     def test_step47_click_submit(self):
#         BetaKindergarten.kindergarten_submit_btn.click()
#     #--END Fill step
#     #END service
#
#     #START checkStatus
#     # def test_step48_click_go_to_lk_btn(self):
#     #     BetaKindergarten.kindergarten_go_to_lk_btn.click()
#
#
#     #determinateOrderID
#     #Parse strong
#     orderId0=0
#     def test_step48_determinateOrderID(self):
#         global orderId
#         orderStorng=BetaKindergarten.kindergarten_status_strong.get_value()
#         orderStorngList=orderStorng.split()
#         orderId=orderStorngList[3]
#
#     def test_step49_click_go_to_lk_btn(self):
#         BetaKindergarten.kindergarten_go_to_lk_btn.click()
#
#     def test_step50_select_order_in_list_btn(self):
#         BetaKindergarten.kindergarten_select_order_in_list_btn.click_by_label(orderId)
#
#     def test_step51_assert_in_progress(self):
#         if BetaKindergarten.kindergarten_after_in_progress.is_exists() and BetaKindergarten.kindergarten_after_in_progress.get_value() == u"Отправлено в ведомство":
#             pass
#         else:
#             self.fail('Status is wrong -->> ' + BetaKindergarten.kindergarten_after_in_progress.get_value())
#     #END checkStatus
#
#
#
# class KindergardenCancellationTest:
#
#     pass
#
# class KindergardenCheckTest:
#
#     pass
#
