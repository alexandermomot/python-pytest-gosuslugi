#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button
from pgu.pages.beta.service import BaseReceptionPage, BaseClaimTypePage


class ReceptionPage(BaseReceptionPage, BaseClaimTypePage):

    ChooseClaimTemporaryDriverLicenseLink = Button("//a[@href='/10056/1']")
    ChooseRenewDriverLicenseLink = Button("//a[@href='/10056/2']")
    ChooseClaimInternationalDriverLicenseLink = Button("//a[@href='/10056/3']")
    ChooseClaimMedicalCertificateDriverLicenseLink = Button("//a[@href='/10056/4']")
    ChooseClaimDriverLicenseLink = Button("//a[@href='/10056/5']")

    @classmethod
    def goto_driver_license_claim_subservice(cls):
        cls.ChooseClaimDriverLicenseLink.click()

