#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import pytest
import logging
import sys
import allure
import time
from allure.constants import AttachmentType
from _pytest.python import SubRequest
from _pytest.config import Parser
from assets.driver import DriverStorage, DriverEngine
from assets.objects.baseobject import BaseObject
from pgu.pages.basepage import BasePage

###
# Phases:
# "setup"   : before calling test module/class/func level
# "call"    : during main routine proccess (func test)
# "teardown": after browser shutdown, main routine has ended
###

skipif = pytest.mark.skipif  # marker, wrap method if needed to be skipped in certain conditions
fail = pytest.fail  # stop test and sign it as failed, used within test body
xfail = pytest.xfail  # stop test and sign it as expected to failure
step = pytest.allure.step  # simplest entity of the test, context manager, e.g. with step(): ... etc
testcase = pytest.allure.testcase
issue = pytest.allure.issue
feature = allure.feature
story = allure.story
attach = allure.attach
environment = allure.environment
# severity = pytest.allure.severity

log_level = logging.INFO
PNG = AttachmentType.PNG
TEXT = AttachmentType.TEXT
JSON = AttachmentType.JSON


def pytest_addoption(parser: Parser):
    parser.addoption("--base-url", action="store", default="http://pgu-uat-beta.test.gosuslugi.ru", required=False,
                     help="Base URL for test execution")
    parser.addoption("--browser", action="store", default="Firefox", required=False,
                     help="Browser name from one of the following: Firefox, Chrome, Ie, etc.")
    parser.addoption("--mocks", action="store_true", default=False, required=False,
                     help="Point this parameter to set the mock state to on during execution")
    parser.addoption("--hub", action="store", default="127.0.0.1:4444", required=False,
                     help="Selenium hub IP address to get the remote webdrivers and bind it to the test cases")


def pytest_runtest_setup(item):
    logging.info("Setting up:", item)


def pytest_report_header(config):
    if config.option.verbose > 0:
        return [
            "Logging level: " + str(log_level),
            "Mocks state: " + str(config.getoption("--mocks"))
        ]


@pytest.mark.tryfirst
def pytest_runtest_makereport(item, call, __multicall__):
    rep = __multicall__.execute()
    setattr(item, "rep_" + rep.when, rep)
    return rep


@pytest.fixture(scope='session', autouse=True)
def general_session_setup(request):
    config = request.config

    envvars = {
        "REPORT": "Автотесты ПГУ",
        "TESTBENCH": config.getoption("--base-url"),
        "BROWSER": config.getoption("--browser"),
        "MOCKS": config.getoption("--mocks"),
        "SELENIUMHUB": config.getoption("--hub")
    }

    environment(**envvars)

    logging.basicConfig(
        stream=sys.stdout,
        format='[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s',
        level=log_level
    )


@pytest.fixture(scope="function")
def engine(request: SubRequest) -> DriverEngine:
    config = request.config
    browser = config.getoption("--browser")
    return DriverStorage[browser]


@pytest.fixture(scope="function")
def url(request) -> str:
    config = request.config
    base_url = config.getoption("--base-url")
    if base_url is not None:
        return base_url


@pytest.fixture(scope="function")
def screenshot(engine: DriverEngine):
    def make(description):
        attach(description, engine.webdriver.get_screenshot_as_png(), type=PNG)
    return make


@step('Скриншот: {1}')
def auto_screenshot(engine: DriverEngine, reason: str):
    reason = reason if reason else engine.title
    attach(reason, engine.webdriver.get_screenshot_as_png(), type=PNG)


class Helper:
    def __init__(self, dengine: DriverEngine, url):
        self.engine = dengine
        self.url = url
        self.driver_name = "Firefox"
        self.hub = None
        self.baseurl = None
        self.dimensions = (1366, 768)
        self.windowpos = (0, 0)
        self.pageloadtimeout = 30
        self.scripttimeout = 30

    def ref_creator(self):
        BaseObject.driver = self.engine
        BasePage.driver = self.engine

    def settings(self):
        self.engine.webdriver.set_page_load_timeout(self.pageloadtimeout)
        self.engine.webdriver.set_script_timeout(self.scripttimeout)
        self.engine.webdriver.set_window_position(*self.windowpos)
        self.engine.webdriver.maximize_window()

    def get_engine(self):
        return self.engine

    def get_webdriver(self):
        return self.engine.webdriver

    @step("Открытие главной страницы бета-портала")
    def beta_start(self):
        self.engine.get(self.url)
        assert "http" in self.url
        time.sleep(2)
        auto_screenshot(self.engine, "Начало теста")
