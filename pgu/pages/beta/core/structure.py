#!/usr/bin/python
# coding=utf-8

__author__ = 'alexander-momot'

from assets.objects.elements import Button, Element
from pgu.pages import BasePage

class StructurePage(BasePage):

    __url__ = "/structure"

    TopStructuresLayer = Element("//div[@active='structures']")
    CategoryBtn = Button(TopStructuresLayer + "//div[@class='limiter']//a[contains(text(),'Категории услуг')]")

    StructuresRoot = Element("//div[@class='structures-root']")
    StructresCategory = Button(StructuresRoot + "//h3")

    FedSluzhbaVSferePravPotreb = Button("//a[contains(text(),'в сфере защиты прав потребителей и благополучия человека')]")
    SezService = Button("//a[@href='/16368' and text()='Выдача санитарно-эпидемиологических заключений']")


    @classmethod
    def expand_collapse_all(cls):
        for i in range(1, 5, 1):
            catcount = Button("(" + cls.StructresCategory.get_xpath() + ")[%s]" % str(i))
            catcount.click()

    @classmethod
    def expand(cls, number=2):
        catcount = Button("(" + cls.StructresCategory.get_xpath() + ")[%s]" % str(number))
        catcount.click()