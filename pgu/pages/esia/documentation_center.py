# #!/usr/bin/python
# # coding=utf-8
# # -*- coding: utf-8 -*-
# __author__ = 'alexander-momot'
#
# import time
#
# from driver.geo import Geo
# from assets.objects.elements import (
#     Button, Input,
#     Text )
# from pgu.pages import BasePage
#
# class EsiaDocumentationCenterPage(BasePage):
#
#     #url = Url("/public/ra/?fts=reg")
#
#     MapOfDocumentationCenters = Text("//div[@id='cfmTypeRAGrp']//div[@id='mapGrp']//div[@id='yaMapRA']")
#     ListOfDocumentationCenters = Text("//div[@id='cfmTypeRAGrp']//div[@id='listGrp']//div[@id='rcs_content']")
#
#     YaMapsZoomInButton = Button("//div[@id='yaMapRA']//ymaps[contains(@class, 'zoom')]/ymaps[contains(@class, 'plus')]")
#     YaMapsZoomOutButton = Button("//div[@id='yaMapRA']//ymaps[contains(@class, 'zoom')]/ymaps[contains(@class, 'minus')]")
#
#     YaMapsDocumentationCenterPoint = Button("//div[@id='yaMapRA']//ymaps[contains(@class, 'svg-icon')]")
#     YaMapsPopupCenterInfoWindow = Text("//div[@id='yaMapRA']//ymaps[contains(@class, 'balloon__content')]")
#     YaMapsPopupCenterInfoWindowClose = Button("//div[@id='yaMapRA']//ymaps[contains(@class, 'balloon__close-button')]")
#
#     LinkShowCentersAsMap = Button("//div[@id='cfmTypeRAGrp']//div[@class='search-type-wrap']//a[@id='map']")
#     LinkShowCentersAsList = Button("//div[@id='cfmTypeRAGrp']//div[@class='search-type-wrap']//a[@id='list']")
#
#     ListElementDocCenterEven = Text("//div[@id='cfmTypeRAGrp']//div[@id='listGrp']//div[@id='rcs_content']//div[@class='ra-line odd']")
#     ListElementDocCenterOdd = Text("//div[@id='cfmTypeRAGrp']//div[@id='listGrp']//div[@id='rcs_content']//div[@class='ra-line ']")
#
#     DocCenterSearchInput = Input("//div[@id='cfmTypeRAGrp']//input[@id='searchRA']")
#     DocCenterSearchButton = Button("//div[@id='cfmTypeRAGrp']//button[@id='find']")
#
#     UiDialogBottom = Text("//div[@class='wizard-navbar-out']//div[@class='ui-dialog-buttonpane']")
#     UiDialogBottomGoToRegister = Button("//div[@class='ui-dialog-buttonpane']//button[@id='j_idt139']")
#     UiDialogBottomAllServices = Button("//div[@class='ui-dialog-buttonpane']//button[@id='j_idt140']")
#
#     FilterServiceRegisterCheckBox = Input("//div[@class='ra-filter filter-services']/table//input[@id='raFilter:0']")
#     FilterServicePersonalCheckBox = Input("//div[@class='ra-filter filter-services']/table//input[@id='raFilter:1']")
#     FilterServiceAccessRestoreCheckBox = Input("//div[@class='ra-filter filter-services']/table//input[@id='raFilter:2']")
#
#     FilterTimeWeekEndsCheckBox = Input("//div[@class='ra-filter filter-time']/table//input[@id='opnModeFilter:0']")
#     FilterTimeEarlierCheckBox = Input("//div[@class='ra-filter filter-time']/table//input[@id='opnModeFilter:1']")
#     FilterTimeLaterCheckBox = Input("//div[@class='ra-filter filter-time']/table//input[@id='opnModeFilter:2']")
#
#     @classmethod
#     def CheckBoxEnabled(cls, cfilter="", num=0):
#         """
#         :type cfilter: String
#         :param cfilter: String part of id of an element
#         :type num: int
#         :param num: Number of an element in pattern
#         :return: bool
#         """
#         # type = opnModeFilter | raFilter
#         # //input[@id='raFilter:0'] | //input[@id='opnModeFilter:3'] | ...
#         checkbox_state = Input("//input[@id='" + cfilter + ":" + str(num) + "']").get_selected()
#         return checkbox_state
#
#     @classmethod
#     def SetLocation(cls):
#         a = Geo()
#         a.set_location(town="Krasnogorsk")
#         a.assign()
#
#     @classmethod
#     def YaMapsZoomIn(cls, times=1):
#         for i in range(1,times,1):
#             cls.YaMapsZoomInButton.click()
#             time.sleep(0.5)
#
#     @classmethod
#     def YaMapsZoomOut(cls, times=1):
#         for i in range(1,times,1):
#             cls.YaMapsZoomOutButton.click()
#             time.sleep(0.5)
#
#     @classmethod
#     def YaMapsZoomInAndZoomOut(cls, times=3):
#         cls.YaMapsZoomIn(times)
#         cls.YaMapsZoomOut(times)
#
#     @classmethod
#     def YaMapsClickAndChooseCenterPoint(cls, n=1):
#         count = 0
#         CenterPoint = cls.YaMapsDocumentationCenterPoint
#         while (count < 5) or (not CenterPoint.is_exists()):
#             cls.YaMapsZoomOutButton.click()
#             time.sleep(1)
#             count += 1
#         YaMapsDocumentationCenterPointNum = Button("(//div[@id='yaMapRA']//ymaps[contains(@class, 'svg-icon')])[" + str(n) + "]") # many of them
#         YaMapsDocumentationCenterPointNum.click()
#
#     @classmethod
#     def YaMapsCloseInfoWindowPopup(cls):
#         cls.YaMapsPopupCenterInfoWindowClose.click()
#
#     @classmethod
#     def SearchFormInputAndSelect(cls, cinput=u"Москва", select=1):
#         cls.DocCenterSearchInput.clear()
#         cls.DocCenterSearchInput.send_keys_by_xpath_delay(cinput)
#         DocCenterSearchSelect = \
#             Button("(//div[contains(@class, 'pui-autocomplete-panel')]//ul/li[contains(@class, 'pui-autocomplete-item')])[" + str(select) + "]")
#         DocCenterSearchSelect.click()
#         cls.DocCenterSearchButton.click()
#
#
#     @classmethod
#     def ListElementsCount(cls):
#         i = 0
#         if cls.ListOfDocumentationCenters.is_exists():
#             if cls.ListElementDocCenterEven.is_exists(): i += 1
#             if cls.ListElementDocCenterOdd.is_exists(): i += 1
#         return i
#
#     @classmethod
#     def CheckFilterBoxesService(cls, reg_usr=None, personal_ident=None, access_restore=None):
#         """
#         Set of filter by list
#         :param reg_usr: True - set checkbox to ON, False - checkbox OFF, None - leave as is
#         :param personal_ident: True - set checkbox to ON, False - checkbox OFF, None - leave as is
#         :param access_restore: True - set checkbox to ON, False - checkbox OFF, None - leave as is
#         :return: None
#         """
#         if reg_usr is not None:
#             if reg_usr is True:             cls.FilterServiceRegisterCheckBox.set_checked()
#             elif reg_usr is False:          cls.FilterServiceRegisterCheckBox.set_unchecked()
#
#         if personal_ident is not None:
#             if personal_ident is True:      cls.FilterServicePersonalCheckBox.set_checked()
#             elif personal_ident is False:   cls.FilterServicePersonalCheckBox.set_unchecked()
#
#         if access_restore is not None:
#             if access_restore is True:      cls.FilterServiceAccessRestoreCheckBox.set_checked()
#             elif access_restore is False:   cls.FilterServiceAccessRestoreCheckBox.set_unchecked()
#
#
#     @classmethod
#     def CheckFilterBoxesTime(cls, weekends=None, early=None, late=None):
#         """
#         Set of filter by list
#         :param weekends: True - set checkbox to ON, False - checkbox OFF, None - leave as is
#         :param early: True - set checkbox to ON, False - checkbox OFF, None - leave as is
#         :param late: True - set checkbox to ON, False - checkbox OFF, None - leave as is
#         :return: None
#         """
#         if weekends is not None:
#             if weekends is True:    cls.FilterTimeWeekEndsCheckBox.set_checked()
#             elif weekends is False: cls.FilterTimeWeekEndsCheckBox.set_unchecked()
#
#         if early is not None:
#             if early is True:       cls.FilterTimeEarlierCheckBox.set_checked()
#             elif early is False:    cls.FilterTimeEarlierCheckBox.set_unchecked()
#
#         if late is not None:
#             if late is True:        cls.FilterTimeLaterCheckBox.set_checked()
#             elif late is False:     cls.FilterTimeLaterCheckBox.set_unchecked()
#
#
#
#
