#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-

import string, re
from pgu.pages.beta.service import BaseFormPage
from assets.objects.elements import Button, Input, Text


class ApplicationPage(BaseFormPage):

    Step1_PersonalPhone = Input("//input[@id='form.FormStep1.ContactInfo.TCell2.PHONEedit']")

    Step2_PassportData_Country_ClickZone = Button("//div[@id='form.FormStep2.birthData.TCell1.BIRTHCOUNTRY']/div[@class='PGU-ComboBox PGU-FieldBody search PGU-context-search_show']")
    Step2_PassportData_Country_InputZone = Input("//div[@id='form.FormStep2.birthData.TCell1.BIRTHCOUNTRY']//input[@name='search_query' and @type='text']")
    Step2_PassportData_Country_ListElement = Button("//div[@id='form.FormStep2.birthData.TCell1.BIRTHCOUNTRY']//div[contains(@class,'PGU-ComboBoxOption') and @data-value!='']")
    Step2_PassportData_PlaceOfBirth = Input("//input[@id='form.FormStep2.birthData.TCell2.BIRTHPLACE']")

    Step3_HomeAddress = Input("//div[@id='form.FormStep3.registrationAddress.PanelFias.Fias']//input[@class='PGU-FiasDropdown-Search-Input js-PGU-FiasDropdown-Search-Input']")
    Step3_HomeAddress_AdditionalStreet = Input("//div[@id='form.FormStep3.registrationAddress.PanelFias.Fias']//div[@class='row-Fias row-Fias-additionalStreet']//input")
    Step3_HomeAddress_House = Input("//div[@id='form.FormStep3.registrationAddress.PanelFias.Fias']//div[@class='row-Fias row-Fias-house']//input")
    Step3_HomeAddress_Apartment = Input("//div[@id='form.FormStep3.registrationAddress.PanelFias.Fias']//div[@class='row-Fias row-Fias-apartment']//input")
    Step3_HomeAddress_RegisterDate = Input("//div[@id='form.FormStep3.registrationAddress.PanelDate.LPLACE_DATE']//input[@id='form.FormStep3.registrationAddress.PanelDate.LPLACE_DATE']")

    Step4_PhotoRequirementsButton = Button("//div[@id='form.FormStep4']//a[contains(text(),'Все требования')]")
    Step4_PhotoRequirementsPopUp = Text("//div[@id='form.FormStep4']//div[@class='PGU-Details js-PGU-Details']")
    Step4_PhotoRequirementsCross = Button("//div[@id='form.FormStep4']//div[@class='PGU-Details js-PGU-Details']//a[contains(@class,'close')]")
    Step4_PhotoFileInput = Input("//div[@id='form.FormStep4']//div[contains(@id, 'photo')]//input[@type='file']")
    Step4_TextPhotoIsOk = Text('//*[@id="imageEditorImageView"]/div[2]/span[contains(text(), "фотография успешно загружена")]')
    # zagran_form_18_register_date_label = Button('//*[@id="form.FormStep3.RegistrationAddress.PanelData.LPLACE_DATE"]/div[1]/label')

    Step5_IsPersonalDataWereNotChanged = Button("//div[@id='form.FormStep5']//a[contains(@class,'PGU-FieldRadioInput')]//span[contains(text(),'Нет')]/..")
    Step6_IsForeignPassportExist = Button("//div[@id='form.FormStep6']//a[contains(@class,'PGU-FieldRadioInput')]//span[contains(text(),'Нет')]/..")

    Step7_WorkingType = Button("//div[@id='form.FormStep7']//div[contains(@id,'FieldRadio11')]/ul/li[2]/a")
    Step7_FromYearSelect = Input("//div[@id='form.FormStep7']//input[contains(@id, 'CELL_START_YEAR.START_YEAR')]")
    Step7_ToYearSelect = Input("//div[@id='form.FormStep7']//input[contains(@id, 'CELL_END_YEAR.END_YEAR')]")
    Step7_FromMonthSelect = Input("//div[@id='form.FormStep7']//div[contains(@id, 'CELL_START_MONTH.START_MONTH')]")
    Step7_ToMonthSelect = Input("//div[@id='form.FormStep7']//div[contains(@id, 'CELL_END_MONTH.END_MONTH')]")
    Step7_FromMonthList = Button("//div[@id='form.FormStep7']//div[contains(@id, 'CELL_START_MONTH.START_MONTH')]//div[contains(@class, 'PGU-ComboBoxOption') and @data-value != '']")
    Step7_ToMonthList = Button("//div[@id='form.FormStep7']//div[contains(@id, 'CELL_END_MONTH.END_MONTH')]//div[contains(@class,'PGU-ComboBoxOption') and @data-value != '']")
    Step7_WorkPosition = Input("//div[@id='form.FormStep7']//input[contains(@id, 'CELL_POSITION.POSITION')]")
    Step7_WorkCompany = Input("//div[@id='form.FormStep7']//input[contains(@id, 'CELL_ORG.ORG')]")
    Step7_WorkAddress = Input("//div[@id='form.FormStep7']//input[contains(@id, 'CELL_ADDRESS.ADDRESS')]")
    Step7_IncludingCurrentTimeCheckBox = Button("//div[@id='form.FormStep7']//div[contains(@id,'PANEL_ATPRESENT.CELL_ATPRESENT.ATPRESENT')]/a")

    Step8_SecretDataAccessRadio = Button("//div[@id='form.FormStep8']//div[contains(@id, 'IS_PRIVATEACCESSpanel.No')]//a")
    Step8_UnavailableTravelAbroadRadio = Button("//div[@id='form.FormStep8']//div[contains(@id, 'IS_OBLIGATIONSpanel.No')]//a")

    Step9_NoChildDataRadio = Button("//div[@id='form.FormStep9']//div[contains(@id, 'form.FormStep9.CHILDpanel.No')]//a")

    Step10_DocumentsApplyAddress = Input("//div[@id='form.FormStep10']//input[@class='PGU-FiasDropdown-Search-Input js-PGU-FiasDropdown-Search-Input']")
    Step10_DocumentsApplyAddress_Select = Button("(//div[@id='form.FormStep10']//div[@class='PGU-Fias-Option-Wrap'])[2]")
    Step10_DocumentsApplyAddress_AdditionalStreet = Input("//div[@id='form.FormStep10']//div[@class='row-Fias row-Fias-additionalStreet']//input")
    Step10_DocumentsApplyAddress_House = Input("//div[@id='form.FormStep10']//div[contains(@class,'row-Fias-house')]//input")
    Step10_DocumentsApplyAddress_Apartment = Input("//div[@id='form.FormStep10']//div[@class='row-Fias row-Fias-apartment']//input")

    Step11_1_ChooseDeptIndividually = Button("//div[@id='form.FormStep11_1']//a[@id='hideOkato']")
    Step11_1_SetDeptName = Input("//div[@id='form.FormStep11_1']//input[contains(@id,'okato_drop')]") # Москва, затем dropdown и выбор
    Step11_1_DropDownList = Button("//div[@id='form.FormStep11_1']//ul[@class='dropdownList']/li") # Клик на элемент выбора, 10 шт обычно

    StepFinal_CheckBoxApproveLaw1 = Button("//div[@id='form.FormStep11']//div[contains(@id,'PanelAgree.Checkbox1')]/a")
    StepFinal_CheckBoxApproveLaw2 = Button("//div[@id='form.FormStep11']//div[contains(@id,'PanelAgree.Checkbox2')]/a")
    StepFinal_SubmitFormButton = Button("//div[@id='form.FormStep11']//a[contains(@id,'SendApplication')]")

    @classmethod
    def personal_data(cls, phonenum="70001234567"):
        cls.Step1_PersonalPhone.send_keys_by_xpath_delay(phonenum, 0.2)
        i = 0
        while re.sub("[^0-9]", "", cls.Step1_PersonalPhone.get_value()) != phonenum and i < 5:
            cls.Step1_PersonalPhone.get_value()
            cls.Step1_PersonalPhone.clear_backspace()
            cls.Step1_PersonalPhone.send_keys_by_xpath_delay(phonenum, 0.2)
            i += 1

    @classmethod
    def passport_data(cls, country="Росс", birthplace="Москва"):
        cls.Step2_PassportData_Country_ClickZone.click()
        cls.Step2_PassportData_Country_InputZone.click()
        cls.Step2_PassportData_Country_InputZone.clear()
        cls.Step2_PassportData_Country_InputZone.clear_backspace()
        cls.Step2_PassportData_Country_InputZone.send_keys_by_xpath_delay(country)
        cls.Step2_PassportData_Country_ListElement.click()
        cls.Step2_PassportData_PlaceOfBirth.clear()
        cls.Step2_PassportData_PlaceOfBirth.clear_backspace()
        cls.Step2_PassportData_PlaceOfBirth.send_keys_by_xpath_delay(birthplace)

    @classmethod
    # searchinput
    def home_address(cls, address = "Москва Хуторская ", selectinput_element = 2, reg_date='23122005'):
        cls.Step3_HomeAddress.clear()
        cls.Step3_HomeAddress.clear_backspace()
        cls.Step3_HomeAddress.send_keys_by_xpath_delay(address)
        Step_HomeAddress_Select = Button("(//div[@id='form.FormStep3.registrationAddress.PanelFias.Fias']//div[@class='PGU-Fias-Option-Wrap'])[" + str(selectinput_element) +"]")
        Step_HomeAddress_Select.click()
        cls.Step3_HomeAddress_AdditionalStreet.send_keys_by_xpath_delay("Мирлэнд")
        cls.Step3_HomeAddress_House.send_keys_by_xpath_delay("38")
        cls.Step3_HomeAddress_Apartment.send_keys_by_xpath_delay("5")
        cls.Step3_HomeAddress_RegisterDate.send_keys_by_xpath(reg_date)

    @classmethod
    def photo_upload(cls):
        cls.Step4_PhotoFileInput.upload_file("")



