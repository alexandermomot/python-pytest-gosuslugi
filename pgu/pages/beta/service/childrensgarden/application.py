__author__ = 'Nadezhda Vavilova'

from assets.objects.elements import Button, Input, Text
from pgu.pages import BasePage


class ApplicationPage(BasePage):

    kindergarten_main_button = Button('//*[@id="content"]/div[2]/div[1]/div[6]/div/div[1]/div/div[3]/span') #button on Main BasePage
    kindergarten_apply_order_button = Button('//*[@id="content"]/div[2]/div/form/fieldset/ul[2]/li[1]/a') #button on PageVariance
    kindergarten_fill_application_button = Button('//*[@id="form.FormStep46.Panel47.Panel83.FormButton39"]/span[2]') #button on Card services

    #Panel 4 - Personal data of the child
    kindergarten_birthday_child = Input('//*[@id="form.FormStep4.OtherData.TCell1.DateOfBirth"]//input')
    kindergarten_snils_child = Input('//*[@id="form.FormStep4.OtherData.TCell3.Snils"]//input')
    kindergarten_sex_child = Button('//*[@id="form.FormStep4.OtherData.TCell2.SEX"]/ul/li[1]/a/span')
    kindergarten_family_child = Input('//*[@id="form.FormStep4.FIO.TCell1.LastName"]//input')
    kindergarten_name_child = Input('//*[@id="form.FormStep4.FIO.TCell2.FirstName"]//input')
    kindergarten_patronymic_child = Input('//*[@id="form.FormStep4.FIO.TCell3.MiddleName"]//input')

    #Panel 5 -  Birth certificate
    kindergarten_birthplace_birth_cert = Input('//*[@id="form.FormStep5.chooseCountry.RF_panel.TRow2.BirthPlace"]//input')
    kindergarten_date_of_issue_birth_cert = Input('//*[@id="form.FormStep5.chooseCountry.RF_panel.TCell3.BirthDocIssueDate"]//input')
    kindergarten_issued_birth_cert = Input('//*[@id="form.FormStep5.chooseCountry.RF_panel.TRow.BirthDocIssuer"]//input')
    kindergarten_num_of_assembly_record_birth_cert = Input('//*[@id="form.FormStep5.chooseCountry.RF_panel.TCell4.BirthDocActNumber"]//input')
    kindergarten_number_birth_cert = Input('//*[@id="form.FormStep5.chooseCountry.RF_panel.TCell2.BirthDocNumber"]//input')
    kindergarten_serial_birth_cert = Input('//*[@id="form.FormStep5.chooseCountry.RF_panel.TCell1.BirthDocSeria"]//input')

    #Panel 6 -  Registered Address-FIAS
    kindergarten_region_name_input = Input('//*[@id="form.FormStep6.PanelFias.registrationAddress"]//input[contains(@class,"PGU-FiasDropdown-Search-Input")]')
    #kindergarten_region_name_address_dropdown = Input('//*[@id="form.FormStep6.PanelFias.registrationAddress"]/div/div[2]/div/div/div[1]/div[2]/div/div[1]/div')
    #kindergarten_region_name_address_dropdown = Input('//*[@id="form.FormStep6.PanelFias.registrationAddress"]/div/div[2]/div[1]/div/div[1]/div[2]/div/div')
    kindergarten_region_name_address_dropdown = Input('//*[@id="form.FormStep6.PanelFias.registrationAddress"]//div[contains(@class,"PGU-Fias-Option-Wrap")]')


    kindergarten_house_input = Input('//*[@id="form.FormStep6.PanelFias.registrationAddress"]//div[contains(@class,"row-Fias-house")]//input')
    kindergarten_apartment_input = Input('//*[@id="form.FormStep6.PanelFias.registrationAddress"]//div[contains(@class,"row-Fias-apartment")]//input')


    #Panel 8 -  Select kindergarten on the map
    kindergarten_add_first_btn = Button('//*[@id="form.FormStep9.TRow1.TCell1.EduOrganization"]/div/div[1]/div[1]/div[4]/div/div[1]/div/div[1]/i/i') #1
    kindergarten_add_second_btn = Button('//*[@id="form.FormStep9.TRow1.TCell1.EduOrganization"]/div/div[1]/div[1]/div[4]/div/div[31]/div/div[1]/i/i') #31

    #Panel 9 - Options
    kindergarten_click_date_btn = Button('//*[@id="form.FormStep8.Parametrs.TCell1.EntryDate"]/div[1]/div[1]/div')
    kindergarten_select_date_btn = Button ('//*[@id="form.FormStep8.Parametrs.TCell1.EntryDate"]/div[1]/div[3]/div/div[5]')
    kindergarten_click_specification_btn = Button('//*[@id="form.FormStep8.TRow2.TCell1.AdaptationProgramType"]/div[1]/div[1]/div')
    kindergarten_select_specification_btn = Button('//*[@id="form.FormStep8.TRow2.TCell1.AdaptationProgramType"]/div[1]/div[3]/div/div[2]')
    kindergarten_requisites_inp = Input('//*[@id="form.FormStep8.TRow2.TCell1.AdaptationProgramDocInfo"]//input')
    #kindergarten_benefits_radio_btn= Button('//*[@id="form.FormStep8.Benefits.Yes"]/ul/li/a/span')
    kindergarten_benefits_radio_btn= Button('//*[@id="form.FormStep8.Benefits.No"]/ul/li/a/i')
    #kindergarten_click_benefits_list_btn = Button('//*[@id="form.FormStep8.Benefits.Yes_panel.Benefits"]/div[2]/div[1]/div')
    #kindergarten_select_benefits_list_btn=Button('//*[@id="form.FormStep8.Benefits.Yes_panel.Benefits"]/div[2]/div[3]/div/div[2]')
    #kindergarten_benefits_requisites_inp=Input('//*[@id="form.FormStep8.Benefits.Yes_panel.BenefitsDocInfo"]')

    #Step 10 - Load files
    kindergarten_load_photo_btn = Button('//*[@id="form.FormStep10.Docs.FieldUpload:uploadDnD"]')
    kindergarten_submit_btn = Button('//*[@id="form.END.TRow1.SendRequest"]/span[2]')

    #Assert status
    kindergarten_go_to_lk_btn = Button('//*[@id="form.FormStepSendRequest.ThankYouPanel.Panel.TextPanel.PanelLK.GotoLKPanel.GoToLK"]/span[2]') #GoToLK
    #kindergarten_go_to_card_btn = Button('')
    kindergarten_after_in_progress = Text('//*[@id="content"]/div[2]/div/ng-include/div/ng-include/fieldset[1]/div[2]/div[1]/p[1]/b')

    kindergarten_status_strong = Text('//*[@id="form.FormStepSendRequest.ThankYouPanel.Panel.OrderLabel"]/h2/span[2]')
    #kindergarten_select_order_in_list_btn = Button('')
    #kindergarten_select_order_in_list_btn=selectOrderInList(orderId)
    #kindergarten_select_order_in_list_btn = Button('//span[contains(text(),orderId)]')



