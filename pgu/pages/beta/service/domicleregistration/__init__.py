#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from pgu.pages.beta.service.domicleregistration.reception import ReceptionPage
from pgu.pages.beta.service.domicleregistration.application import ApplicationPage
from pgu.pages.beta.service.domicleregistration.vars import Vars