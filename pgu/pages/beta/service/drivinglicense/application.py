#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import re
import string, datetime, time
from pgu.pages.beta.service import BaseFormPage
from assets.objects.elements import Button, Input, Loader, Element


class ApplicationPage(BaseFormPage):

    step2_personalphoneinput = Input("//input[@id='form.FormStep2.Panel1.Panel17.contactPhone']")

    LoaderAjax = Loader("//div[@id='shadowWrap']")
    LoaderForm = Loader("//div[@id='main-throbber']")

    # searchinput
    step4 = Element("//div[@id='form.FormStep4']")
    Step_HomeAddress = Input(step4 + "//input[@class='PGU-FiasDropdown-Search-Input js-PGU-FiasDropdown-Search-Input']")
    Step_HomeAddress_AdditionalStreet = Input(step4 + "//div[@class='row-Fias row-Fias-additionalStreet']//input")
    Step_HomeAddress_House = Input(step4 + "//div[contains(@class, 'row-Fias row-Fias-house')]//input")
    Step_HomeAddress_Apartment = Input(step4 + "//div[contains(@class, 'row-Fias row-Fias-apartment')]//input")

    Step_AutoschoolName = Input("//div[@id='form.FormStep5']//input[contains(@id, 'schoolName')]")
    Step_CertificateNum = Input("//div[@id='form.FormStep5']//input[contains(@id, 'sertificateNumber')]")
    Step_DateOfIssue = Input("//div[@id='form.FormStep5']//input[contains(@id, 'sertificateIssueDate')]")

    Step_MedicalDocumentNum = Input("//div[@id='form.FormStep6']//input[contains(@id, 'number')]")
    Step_MedicalDocumentIssueDate = Input("//div[@id='form.FormStep6']//input[contains(@id, 'issueDate')]")
    Step_MedicalInstitutionName = Input("//div[@id='form.FormStep6']//input[contains(@id, 'issuer')]")
    Step_MedicalInstitutionLicenseNum = Input("//div[@id='form.FormStep6']//input[contains(@id, 'issuerLicense')]")

    #searchinput
    Step_DrivingLicenseIssueAddress = Input("//div[@id='form.FormStep7']//input[@class='PGU-FiasDropdown-Search-Input js-PGU-FiasDropdown-Search-Input']")
    #Step_DrivingLicenseIssueAddress = Input("//div[@id='form.FormStep7']//div[@class='PGU-ComboBox PGU-FiasDropdown PGU-FieldBody']")
    Step_DrivingLicenseIssueAddress_AdditionalSteet = Input("//div[@id='form.FormStep7']//div[@class='row-Fias row-Fias-additionalStreet']//input")
    Step_DrivingLicenseIssueAddress_House = Input("//div[@id='form.FormStep7']//div[@class='row-Fias row-Fias-house']//input")
    Step_DrivingLicenseIssueAddress_Apartment = Input("//div[@id='form.FormStep7']//div[@class='row-Fias row-Fias-apartment']//input")
    Step_YaMaps_Search = Input("//div[@id='form.FormStep7']//div[contains(@id, 'chooseDepartment.deptCode')]//input")
    Step_YaMaps_ChooseDepartment = Button("//ymaps//span[contains(text(),'Выбрать')]/ancestor::a")
    Step_DateOfVisit = Input("(//div[@id='form.FormStep7']//div[contains(@id, 'AppointmentDate')]//input)[1]")
    Step_TimeOfVisit = Button("//div[@id='form.FormStep7']//div[contains(@id, 'AppointmentDateSlots')]//div[@class='PGU-ComboBoxFace']")

    YaMapsAnyLocation = Button("//div[contains(@id,'form.FormStep7')]//div[contains(@class,'PGU-FieldMapWithListItemWrap')]")

    Step_DisclaimerAgree = Button("//div[@id='form.FormStep8.PanelAgree']//a")
    Step_SubmitForm = Button("//a[@id='form.FormStep9.PanelSend.SetAppointment']/span[@class=contains(text(), 'Отправить')]")

    @classmethod
    def driving_category(cls, category_letter: str or list):
        if type(category_letter) is list:
            categories = []
            for cat in category_letter:
                Button("//div[@id='form.FormStep1']//div[@id='form.FormStep1.PanelService.Panel200.%s']/a" % cat).click()
        elif type(category_letter) is str:
            Button("//div[@id='form.FormStep1']//div[@id='form.FormStep1.PanelService.Panel200.%s']/a" % category_letter).click()
        else:
            pass

    @classmethod
    def personal_data(cls, phonenum="70001234567"):
        cls.step2_personalphoneinput.send_keys_by_xpath_delay(phonenum, 0.2)
        i = 0
        while re.sub("[^0-9]", "", cls.step2_personalphoneinput.get_value()) != phonenum and i < 5:
            cls.step2_personalphoneinput.get_value()
            cls.step2_personalphoneinput.clear_backspace()
            cls.step2_personalphoneinput.send_keys_by_xpath_delay(phonenum, 0.2)
            i += 1

    @classmethod
    def home_address_form(cls, selectinput_element=2, search_text="Москва Хуторская "):
        cls.Step_HomeAddress.clear_backspace()
        cls.Step_HomeAddress.send_keys_by_xpath_delay(search_text)
        homeaddress_select = Button("(//div[@id='form.FormStep4.Panel.Fias']//div[@class='PGU-Fias-Option-Wrap'])[" + str(selectinput_element) +"]")
        homeaddress_select.click()
        cls.Step_HomeAddress_AdditionalStreet.clear()
        cls.Step_HomeAddress_AdditionalStreet.send_keys_by_xpath("Мирлэнд")
        cls.Step_HomeAddress_House.clear()
        cls.Step_HomeAddress_House.send_keys_by_xpath("38")
        cls.Step_HomeAddress_Apartment.clear()
        cls.Step_HomeAddress_Apartment.send_keys_by_xpath("4")


    @classmethod
    def autoschool_info_and_certificate(cls, radiobutton = 2):
        cls.Step_AutoschoolName.send_keys_by_xpath("Центральная автошкола")
        cls.Step_CertificateNum.send_keys_by_xpath("BA222222222222222222")
        cls.Step_DateOfIssue.send_keys_by_xpath("05062005")
        Step_LicenceRadioButton = Input("(//div[@id='form.FormStep5']//div[contains(@id, 'FieldRadio')]//a)[" + str(radiobutton) + "]")
        Step_LicenceRadioButton.click()

    @classmethod
    def medical_certificate(cls, category_checkbox):
        if not category_checkbox:
            category_checkbox = [1, 3, 5]
        cls.Step_MedicalDocumentNum.send_keys_by_xpath("55322")
        cls.Step_MedicalDocumentIssueDate.send_keys_by_xpath("05062005")
        cls.Step_MedicalInstitutionName.send_keys_by_xpath("Городская больница №56")
        cls.Step_MedicalInstitutionLicenseNum.send_keys_by_xpath("АБ-99-99-999999")
        for i in category_checkbox:
            Button("(//div[@id='form.FormStep6.PanelMedicalCategory']//input[@type='checkbox']/ancestor::a)[" + str(i) + "]").click()

    @classmethod
    def gosautoinspection_department_address(cls, selectinput_element = 2, search_text = "Москва Хуторская "):
        cls.Step_DrivingLicenseIssueAddress.clear()
        cls.Step_DrivingLicenseIssueAddress.send_keys_by_xpath_delay(search_text)
        Step_DrivingLicenseIssueAddress_Select = Button("(//div[@id='form.FormStep7']//div[@class='PGU-Fias-Option-Wrap'])[" + str(selectinput_element) +"]")
        Step_DrivingLicenseIssueAddress_Select.click()
        cls.Step_DrivingLicenseIssueAddress_AdditionalSteet.send_keys_by_xpath("Мирлэнд")
        cls.Step_DrivingLicenseIssueAddress_House.send_keys_by_xpath("38")
        cls.Step_DrivingLicenseIssueAddress_Apartment.send_keys_by_xpath("4")

    @classmethod
    def yamaps_address(cls, search_department = "РЭО"):
        cls.Step_YaMaps_Search.send_keys_by_xpath_delay(search_department)
        Step_YaMaps_Department = Button("//div[@id='form.FormStep7']//h3[contains(text(), '" + search_department + "')]/ancestor::a")
        time.sleep(5)
        Step_YaMaps_Department.click()
        time.sleep(5)
        cls.Step_YaMaps_ChooseDepartment.click()

    @classmethod
    def date_and_time_of_visit(cls, date_select = None, time_select = "15:00"):
        if not date_select:
            date_select = (datetime.date.today() + datetime.timedelta(days=2)).strftime("%d.%m.%Y") # today + 1 day
        cls.Step_DateOfVisit.send_keys_by_xpath_delay(date_select + " ") # insert string with date + 1 day
        time.sleep(5)
        cls.Step_TimeOfVisit.click()
        time.sleep(5)
        if time_select: # if pointed as some time, choose that time particularly, else, choose first time available
            Step_TimeOfVisitSelect = Button("//div[@id='form.FormStep7']//div[contains(@id, 'AppointmentDateSlots')]//div[contains(text(), '" + time_select +"')]")
        else:
            Step_TimeOfVisitSelect = Button("(//div[@id='form.FormStep7']//div[contains(@id, 'AppointmentDateSlots')]//div[@class='PGU-ComboBoxOption'])[position()>1]")
        Step_TimeOfVisitSelect.click()

    @classmethod
    def agree_and_submit_form(cls):
        cls.Step_DisclaimerAgree.click()
        cls.Step_SubmitForm.click()
        time.sleep(5)

    @classmethod
    def wait_for_loaders(cls):
        cls.LoaderAjax.wait_for_disappear()
        #ApplicationPage.LoaderForm.wait_for_disappear()










