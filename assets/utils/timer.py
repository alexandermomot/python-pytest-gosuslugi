#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'alexander-momot'

import time


class Timer:

    def __init__(self):
        self.created = time.ctime()
        self.start_time = 0
        self.stop_time = 0
        self.round = 9

    @staticmethod
    def now():
        """
        :return: Returns current time in string view like "[ 25.12.2000 24:56:44 ] "
        :rtype: str
        """
        return "[" + time.strftime("%H:%M:%S", time.localtime()) + "] "

    @staticmethod
    def now_format(view="%d-%m-%Y_%H-%M-%S"):
        """
        :return: Returns current time with formated view, like "01-02-03_01-02-03"
        :rtype: str
        """
        return time.strftime(view, time.localtime())

    def start_timer(self):
        if self.start_time != 0:
            raise Exception("Reset the timer before performing next launch")
        else:
            self.start_time = time.time()

    def stop_timer(self):
        if self.start_time == 0:
            raise Exception("Initiate the time start of the timer first")
        else:
            if self.stop_time == 0:
                self.stop_time = time.time()
            return str(round(self.stop_time - self.start_time, self.round))

    def elapsed_sec_str(self):
        if self.start_time == 0 and self.stop_time == 0:
            return str(0)
        elif self.stop_time == 0:
            return str(round(time.time() - self.start_time, self.round))
        else:
            return str(round(self.stop_time - self.start_time, self.round))

    def elapsed_sec_int(self):
        if self.start_time == 0 and self.stop_time == 0:
            return int(0)
        elif self.stop_time == 0:
            return int(round(time.time() - self.start_time, self.round))
        else:
            return int(round(self.stop_time - self.start_time, self.round))

    def reset_timer(self):
        self.start_time = 0
        self.stop_time = 0

    def set_precision(self, precision = 9):
        precision = int(precision)
        if precision < 0 or precision > 9:
            raise Exception("Integer must be between 0 and 9")
        else:
            self.round = precision

