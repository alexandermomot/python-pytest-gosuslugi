#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import string, datetime, time
from pgu.pages.beta.service import BaseFormPage
from assets.objects.elements import Button, Input, Loader, Element, Text


class ApplicationPage(BaseFormPage):

    FormTitle = Text("//div[@class='title']")
    MainForm = Element("(//form[@name='form']/fieldset)[1]")
    FinesForm = Element("(//form[@name='form']/fieldset)[2]")

    DriverName = Input("//label[text()='Водитель']/following-sibling::input")
    DriveLicenseNumLabel = Element("//label[text()='Номер водительского удостоверения']")
    DriveLicenseNumInput = Input(DriveLicenseNumLabel + "/following-sibling::input")

    GosNumAuto = Input("//label[text()='Госномер транспортного средства']/following-sibling::input")
    TSRegisterNum = Input("//label[text()='Свидетельство о регистрации ТС']/following-sibling::input")

    FindFinesBtn = Button("//input[@value='Найти штрафы']")
    NewAutoBtn = Button("//span[text()='Добавить автомобиль']")

    FinesTitle = Text("//h3[text()='Ваши штрафы']")

    ErrorAdvice = Element("//div[@class='advice type_not']")

    ErrorHeader = Text(ErrorAdvice + "//h2/span[text()='Что-то пошло не так']")

