#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-

import logging
from assets.objects.elements.element import Element


class Text(Element):

    __xpath_countdown = ""

    def __init__(self, xpath):
        self.text = ""
        Element.__init__(self, xpath)

    def get_text(self):
        text = self.driver.find_element_by_xpath_and_get_text(self.xpath).strip().rstrip("\n")
        logging.info("Get text [%s]: [%s]" % (self.xpath, self.text))
        return text

    # def set_countdown(self, xpath_countdown):
    #     self.__xpath_countdown = unicode(xpath_countdown, "utf-8")




