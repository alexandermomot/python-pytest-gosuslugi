Для включения в отчёт:


Default Subject
$PROJECT_NAME: Test # $BUILD_NUMBER - $BUILD_STATUS!


Default Content
${SCRIPT, template="groovy-head.groovy"}
<body>
<h2>$PROJECT_NAME: Test # $BUILD_NUMBER - $BUILD_STATUS:</h2>
${SCRIPT, template="groovy-body.groovy"}
</body>


Default pre-send script:
if (build.result.toString().equals("FAILURE")) {
    msg.addHeader("X-Priority", "1 (Highest)");
    msg.addHeader("Importance", "High");
}
cancel = build.result.toString().equals("ABORTED");
