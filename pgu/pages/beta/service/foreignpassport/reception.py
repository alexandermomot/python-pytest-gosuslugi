#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button
from pgu.pages.beta.service import BaseReceptionPage, BaseClaimTypePage


class ReceptionPage(BaseReceptionPage, BaseClaimTypePage):

    class YearsOld:

        ForeignPassportOlder18Button = Button('//a[text()="На себя (старше 18 лет)"]')
        ForeignPassportBetween14and18Button = Button('//a[text()="На ребенка от 14 до 18 лет"]')
        ForeignPassportBelow14Button = Button('//a[text()="На ребенка до 14 лет"]')

        @classmethod
        def choose_less_14_years_old(cls):
            cls.ForeignPassportBelow14Button.click()

        @classmethod
        def choose_between_14_and_18_years_old(cls):
            cls.ForeignPassportBetween14and18Button.click()

        @classmethod
        def choose_more_18_years_old(cls):
            cls.ForeignPassportOlder18Button.click()

    NewForeignPassportTypeButton = Button("//a[text()='Выбрать новый образец']")
    OldForeignPassportTypeButton = Button("//a[text()='Выбрать старый образец']")

    @classmethod
    def choose_new_type(cls):
        cls.NewForeignPassportTypeButton.click()
        return cls.YearsOld

    @classmethod
    def choose_old_type(cls):
        cls.OldForeignPassportTypeButton.click()
        return cls.YearsOld


