#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import platform, logging, psutil

from seleniumrequests import Firefox, Chrome, Ie, Safari, Opera, Remote, PhantomJS
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver import DesiredCapabilities

from assets.driver.engine import DriverEngine
from vars import SystemVars


class DriverStorageMeta(type):

    # Default name of the driver is "main"
    # So you should use it anywhere in the runtime as DriverStorage["main"] until
    # additional drivers will be created and named by the same convention

    @classmethod
    def __setitem__(mcs, name, driver_engine):
        """
        :param name: name of the driver to set and store
        :param driver_engine: which driver_engine we should store
        :return: DriverStorage
        """
        if not issubclass(driver_engine.__class__, DriverEngine):
            raise IndentationError("Class: " + str(driver_engine) + " is not a subclass of " + str(DriverEngine.__class__))
        if not isinstance(name, str):
            raise IndentationError("Name: " + str(name) + " is not a valid string and cannot be indexed in storage for: " + str(DriverEngine))
        setattr(mcs, name, driver_engine)

    @classmethod
    def __getitem__(mcs, name):
        """
        :param name: Name of the DriverEngine object to be returned from the storage
        :rtype: DriverStorage
        """
        if not isinstance(name, str):
            raise IndentationError("Name: " + str(name) + "is not a valid string and cannot be indexed in storage for: " + str(DriverEngine))
        try:
            return getattr(mcs, name)
        except KeyError:
            return None

    @classmethod
    def __delitem__(mcs, name):
        # TODO: autoclose browser and then delete the reference
        delattr(mcs, name)


class DriverStorage(metaclass=DriverStorageMeta):

    def __init__(self):
        raise Exception(self.__class__.__name__ + " is a singleton and can't be instantiated")

    def __setitem__(self, key, value):
        """
        :param key: Name of the driver to be created and stored in storage
        :param value: DriverEngine reference to the driver
        :return: DriverEngine
        """
        pass

    def __getitem__(self, item) -> DriverEngine:
        """
        :param item: Name of the driver to return into runtime
        :rtype: DriverEngine
        """
        pass

    DRIVER_PATHS = SystemVars.__dict__.keys()

    BROWSER_CAPABILITIES = {
        'locationContextEnabled': True,
        'acceptSslCerts': True,
        'unexpectedAlertBehaviour': 'accept',
        'databaseEnabled': True,
        'applicationCacheEnabled': True,
        'browserConnectionEnabled': True,
        'webStorageEnabled': True
        # 'nativeEvents': True
    }

    FIREFOX_PREFERENCES = {
        "app.update.enabled": False,
        "browser.search.update": False,
        "extensions.update.enabled": False,
        "browser.startup.homepage": "about:blank",
        "startup.homepage_welcome_url": "about:blank",
        "startup.homepage_welcome_url.additional": "about:blank"
    }

    @classmethod
    def create(cls, browser, name="main"):
        """
        :param browser: name of the browser to be created and returned as instance of the driver_engine wrapper
        :param name: global string identifier of the browser, available anywhere in runtime
        :rtype: DriverEngine
        """
        # TODO: Usage: DriverStorage["browser1"].create("firefox")
        # TODO: Similar to: DriverStorage["browser1"] = DriverEngine("firefox")
        logging.debug("[ARGS] driver_name %s" % browser)
        DriverStorage[name] = cls._get_driver_from_dict(browser)
        return DriverStorage[name]

    @classmethod
    def create_remote(cls, driver_name, hub, name="main"):
        logging.debug("[ARGS] driver_name %s, hub %s" % (driver_name, hub))
        try:
            return Remote(command_executor=hub,
                          desired_capabilities=eval(cls.BROWSER_CAPABILITIES[driver_name]))
        except Exception as e:
            logging.critical(e)

    @classmethod
    def take(cls, name):
        # TODO: Usage: DriverStorage["browser1"].take()
        # TODO: Similar to: mybrows = DriverStorage["browser1"]
        # logging.debug("[ARGS] driver_name %s" % browser)
        pass

    @classmethod
    def kill(cls, name):
        # TODO: Usage: DriverStorage["browser1"].kill()
        # TODO: Similar to: mybrows = DriverStorage["browser1"]
        logging.debug("[ARGS] driver_name %s" % name)
        DriverStorage[name].close_browser()
        #del DriverStorage[name]

    @classmethod
    def _get_driver_from_dict(cls, browser):
        """
        :param browser: String name of the driver
        :rtype: WebDriver
        """
        logging.debug("[ARGS] driver_name %s" % browser)
        #print SystemVars.DATA_ROOT + '/drivers/linux/chromedriver_x86_2.16'
        browser = str(browser).lower()
        driver = {
                'firefox': Firefox,
                'chrome': Chrome,
                'opera': Opera,
                'safari': Safari,
                'ie': Ie,
                'phantomjs': PhantomJS
        }[browser]
        if browser == 'firefox':
            profile = FirefoxProfile()
            for key, value in cls.FIREFOX_PREFERENCES.items():
                profile.set_preference(key, value)
            profile.accept_untrusted_certs = True
            profile.assume_untrusted_cert_issuer = True
            profile.update_preferences()
            drv_object = driver(capabilities=DesiredCapabilities.FIREFOX.update(cls.BROWSER_CAPABILITIES),
                                firefox_profile=profile)
        elif browser == 'chrome':
            #options = ChromeOptions()
            drv_object = driver(executable_path=cls._get_platform_driver_path(browser),
                                desired_capabilities=DesiredCapabilities.CHROME.update(cls.BROWSER_CAPABILITIES))
        elif browser == 'opera':
            drv_object = driver(executable_path=cls._get_platform_driver_path(browser),
                                desired_capabilities=DesiredCapabilities.OPERA.update(cls.BROWSER_CAPABILITIES))
        elif browser == 'safari':
            drv_object = driver(executable_path=cls._get_platform_driver_path(browser),
                                desired_capabilities=DesiredCapabilities.SAFARI.update(cls.BROWSER_CAPABILITIES))
        elif browser == 'ie':
            drv_object = driver(executable_path=cls._get_platform_driver_path(browser),
                                capabilities=DesiredCapabilities.INTERNETEXPLORER.update(cls.BROWSER_CAPABILITIES))
        elif browser == 'phantomjs':
            drv_object = driver(executable_path=cls._get_platform_driver_path(browser),
                                desired_capabilities=DesiredCapabilities.PHANTOMJS.update(cls.BROWSER_CAPABILITIES))
        else:
            drv_object = None
            logging.critical("Set drv_object to None!")

        logging.info(drv_object.name.capitalize() + " arise" + " (pid: " + cls._get_pid(drv_object) + ")")

        return DriverEngine(webdriver=drv_object)

    @classmethod
    def _get_pid(cls, driver):
        logging.debug("[ARGS] driver %s" % driver)
        _pid = {'browser': None, 'service': None}
        # There are 2 pids for some browsers: service and child browser
        # driver.binary.process.pid # firefox (and other proxy-less drivers)
        # driver.service.process.pid # chrome (and other proxy-pass drivers)
        if type(driver) is Firefox:
            _pid['browser'] = driver.binary.process.pid
        elif type(driver) is Chrome:
            _pid['service'] = driver.service.process.pid
            _pid['browser'] = psutil.Process(driver.service.process.pid).children()[0].pid
        return str(_pid)

    @classmethod
    def _get_platform_driver_path(cls, driver):
        logging.debug("[ARGS] driver %s" % driver)
        os_type = platform.system().upper()  # [Linux, Windows, Darwin]
        driver_type = str(driver).upper()
        if platform.architecture()[0] == '32bit':
            os_architect = "X86"
            full_path = SystemVars.webdriver.get(os_type + "_" + driver_type + "DRIVER" + "_" + os_architect + "_PATH")
        elif platform.architecture()[0] == '64bit':
            os_architect = "X64"
            if SystemVars.webdriver.get(os_type + "_" + driver_type + "DRIVER" + "_" + os_architect + "_PATH"):
                full_path = SystemVars.webdriver.get(os_type + "_" + driver_type + "DRIVER" + "_" + os_architect + "_PATH")
            else:
                os_architect = "X86"
                full_path = SystemVars.webdriver.get(os_type + "_" + driver_type + "DRIVER" + "_" + os_architect + "_PATH")
        else:
            os_architect = "X86"
            full_path = SystemVars.webdriver.get(os_type + "_" + driver_type + "DRIVER" + "_" + os_architect + "_PATH")
        # example: returns LINUX_CHROMEDRIVER_X86_PATH
        logging.info(driver.capitalize() + " executable found: " + full_path)
        return full_path

