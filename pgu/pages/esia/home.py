#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button, Element, Input, Text
from pgu.pages import BasePage
from pgu.pages.beta.core.home import BetaHomePage


class EsiaHomePage(BasePage):

    __url__ = "/idp/rlogin?cc=bp"

    RegistrationButton = Button("//form[@id='authnFrm']//a[contains(text(), 'Зарегистрируйтесь')]")
    RecoveryPass = Button("//form[@id='authnFrm']//a[contains(text(), 'Восстановить пароль')]")

    LoginPasswordForm = Element("//div[@id='pageLogin']")

    EsiaErrorHeader = Text("//h1[contains(text(),'Уважаемые пользователи портала')]")
    EsiaErrorDescription = Text("//p[contains(text(),'портал временно недоступен')]")

    SecretAnswerForRecovery = Input("//input[@id='answer']")
    SecretAnswerButtonForRecovery = Button("//button[@id='secretAnswerBtn']")
    SearchMobileOrEmailBtnForRecovery = Button("//button[@id='searchMobileOrEmailBtn']")
    SearchSnilsForRecovery = Button("//button[@id='searchSnilsBtn']")
    SetNewPasswordButtonForRecovery = Button("//button[@id='setNewPasswordButton']")
    PassRecoveryViaSnils = Button("//a[contains(., 'Восстановить с помощью СНИЛС')]")

    AnotherUser = Input("//form[@id='authnFrm']//a[contains(., 'Другой пользователь')]")

    Datepicker = Button("//div[@id='ui-datepicker-div']")

    AjaxLoader = Button("//div[@class='ajax-loader-bg big']")

    RecoveryText = Text("//p[contains(., 'Код восстановления отправлен')]")

    CloseIcon = Button("//div[@class='ui-growl-icon-close ui-icon ui-icon-closethick']")

    @classmethod
    def registration(cls):
        cls.RegistrationButton.click()

    @classmethod
    def page_error(cls):
        return cls.EsiaErrorHeader.is_visible(1) and cls.EsiaErrorDescription.is_visible(1)

    class Login:

        def __init__(self, login, password):
            self.auth_retries = 0
            self.login = login
            self.password = password

        AuthVariantSnils = Button("//form[@id='authnFrm']/div[@class='enter-list']/a[contains(text(), 'СНИЛС')]")
        AuthVariantPhone = Button("//form[@id='authnFrm']/div[@class='enter-list']/a[contains(text(), 'Телефона/почты')]")
        LoginPhoneOrEmailInput = Input("//input[@id='mobileOrEmail']")
        LoginSnilsInput = Input("//form[@id='authnFrm']//input[@id='snils']")
        PassInput = Input("//form[@id='authnFrm']//input[@id='password']")
        MakeAuth = Button("//form[@id='authnFrm']//div[@class='line-btns']/button[@data-bind='click: loginByPwd']")

        AnotherUserBtn = Button("//a[@class='another-user']")

        ErrorDisclaimers = {
            "EmailOrPhoneEmpty": Text("//div[@class='field-error']/*[contains(.,'Введите мобильный телефон или почту')]"),
            "SnilsEmpty": Text("//div[@class='field-error']/*[contains(.,'Введите СНИЛС')]"),
            "PasswordEmpty": Text("//div[@class='field-error']/*[contains(.,'Введите пароль')]")
        }

        def by_email_or_phone(self):
            if self.LoginPhoneOrEmailInput.is_invisible(1):
                self.AuthVariantPhone.click()
            if not self.AnotherUserBtn.is_invisible(1):
                self.AnotherUserBtn.click()
            self.LoginPhoneOrEmailInput.is_visible()
            self.LoginPhoneOrEmailInput.clear()
            self.LoginPhoneOrEmailInput.send_keys_by_xpath_delay(self.login, 0.05)
            self.PassInput.is_visible()
            self.PassInput.clear()
            self.PassInput.send_keys_by_xpath_delay(self.password, 0.05)
            self.MakeAuth.click()
            if self.LoginPhoneOrEmailInput.is_visible(1) is not None:
                self.__retry(self.by_email_or_phone)
            return self.Personality

        def by_snils(self):
            if self.LoginSnilsInput.is_invisible(1):
                self.AuthVariantSnils.click()
            if not self.AnotherUserBtn.is_invisible(1):
                self.AnotherUserBtn.click()
            self.LoginSnilsInput.click()
            self.LoginSnilsInput.clear_backspace()
            self.LoginSnilsInput.send_keys_by_xpath(self.login)
            self.PassInput.click()
            self.PassInput.clear_backspace()
            self.PassInput.send_keys_by_xpath(self.password)
            self.MakeAuth.click()
            if self.LoginPhoneOrEmailInput.is_visible(1) is not None:
                self.__retry(self.by_snils)
            return self.Personality

        def __retry(self, retry_method):
            for error in self.ErrorDisclaimers.values():
                if not error.is_invisible(1) and self.auth_retries < 5:
                    self.auth_retries += 1
                    retry_method()


        class Personality:

            def __init__(self, name=""):
                self.name = name

            RoleIndividual              = Button("//div[@class='datalist-block']//div[@class='role-item P']")
            RoleIndividualEnterpreneuer = Button("//div[@class='datalist-block']//div[@class='role-item B']")
            RoleLegalEntity             = Button("//div[@class='datalist-block']//div[@class='role-item L']")

            @classmethod
            def as_person(cls):
                if cls.RoleIndividual.is_exists():
                    cls.RoleIndividual.click()
                    BetaHomePage.LoggedUserCabinet.is_exists()

            @classmethod
            def as_individual_enterpreneuer(cls):
                if cls.RoleIndividualEnterpreneuer.is_exists():
                    cls.RoleIndividualEnterpreneuer.click()
                    BetaHomePage.LoggedUserCabinet.is_exists()

            @classmethod
            def as_legal_entity(cls):
                pass


