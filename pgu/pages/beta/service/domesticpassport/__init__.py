#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from pgu.pages.beta.service.domesticpassport.reception import ReceptionPage
from pgu.pages.beta.service.domesticpassport.application import ApplicationPage
from pgu.pages.beta.service.domesticpassport.vars import Vars