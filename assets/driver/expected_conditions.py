from selenium.common.exceptions import WebDriverException, TimeoutException

"""
 * Canned "Expected Conditions" which are generally useful within webdriver
 * tests.
"""

class document_settle_condition(object):
    """ DocumentSettleCondition checks two properties.
    The first is the document.readyState property to be complete for the stabilization period.
    The second is the value of WebDriver.getCurrentUrl() to not change during the stabilization period.
    DocumentSettleCondition wraps another condition. After the document is considered settled,
    the value of the wrapped condition is returned.
    Since we don't know what page we're going to land on, the wrapped condition will be something generic,
    like By.cssSelector("body") or an element common across the application, from the header, footer, etc.
    """

    def __init__(self):
        self.url = None

    def __call__(self, driver):
            try:
                ready_state = driver.execute_script("return document.readyState")
                return True if ready_state == "complete" else False
            except TimeoutException as te:
                return False


class PageIsNotReadyException(WebDriverException):
    """
    Thrown when there is an issue with loading page
    """
    pass
