#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from pgu.pages.beta.core.home import BetaHomePage
from pgu.pages.beta.core.category import CategoryPage
from pgu.pages.beta.core.structure import StructurePage
from pgu.pages.beta.core.personal_cabinet import PersonalCabinetPage
