function getElementByXpath(path) {
  return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}

var cumulativeOffset = function(element) {
    var top = 0, left = 0;
    do {
        top += element.offsetTop  || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while(element);

    return {
        top: top,
        left: left
    };
};

function getElementCenter(element) {

    var coordinates = {
        "elem_x_center": element.getBoundingClientRect()["width"]/2,
        "elem_y_center": element.getBoundingClientRect()["height"]/2,
        "left": cumulativeOffset(element)["left"],
        "top": cumulativeOffset(element)["top"]
    };

    var screencenter = {
        "center_x": Math.max(document.documentElement.clientWidth, window.innerWidth || 0)/2,
        "center_y": Math.max(document.documentElement.clientHeight, window.innerHeight || 0)/2
    };

    return {
        "absolutecenterX": coordinates["left"] + coordinates["elem_x_center"] - screencenter["center_x"],
        "absolutecenterY": coordinates["top"] + coordinates["elem_y_center"] - screencenter["center_y"]
    }

}

coords = getElementCenter(getElementByXpath("%xpath%"));
window.scrollTo(coords["absolutecenterX"],coords["absolutecenterY"]);
