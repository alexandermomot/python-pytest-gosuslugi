# #!/usr/bin/python
# # coding=utf-8
# # -*- coding: utf-8 -*-
# __author__ = 'dmitry.khaschinin'
#
# from assets.objects.elements import (
#     Button, Input,
#     Text )
#
# from pgu.pages.esia.profile.person import EsiaPersonPage
# from pgu.pages import BasePage
#
#
# class EsiaConsoleTechPage(BasePage):
#
#     __url__ = "https://esia-uat.test.gosuslugi.ru/console/tech"
#
#     # Системы
#
#     Sys = Button("//div[@class='top-menu-wrap']//ul[@class='top-menu']//a[@title='Системы']")  # Вкладка системы
#
#     AddSys = Button("//button[contains(., 'Добавить систему')]")  # Добавить систему
#
#     # Форма добавления системы
#
#     SysName = Input("//input[@id='sysName']")
#     SysNsiId = Input("//input[@id='sysNsiId']")
#     SysInf = Input("//textarea[@id='sysInf']")
#     SysHomeUrl = Input("//input[@id='sysHomeUrl']")
#     SysStuFio = Input("//input[@id='sysStuFio_input']")
#
#     SaveSys = Button("//form[@id='sysDlgForm']//div[@id='sysDialogPnl']//button[contains(., 'Сохранить')]")  # сохранить
#     CancelSys = Button("//form[@id='sysDlgForm']//div[@id='sysDialogPnl']//button[contains(., 'Отмена')]")  # отмена
#
#     @staticmethod
#     def EditSysFromTableTechConsole(sys=u"Воронеж"):
#         Button(
#             "//div[@id='sysTbl']//tbody[@id='sysTbl_data']//tr[contains(., '" + sys + "')]//button[@title='Редактировать систему']").click()
#
#     @staticmethod
#     def ChooseFioFromSysStu(fiostu):
#         Button("//div[@id='sysStuFio_panel']//ul//li[contains(., '" + fiostu + "')]").click()
#
#     # Группы
#
#     Grp = Button("//div[@class='top-menu-wrap']//ul[@class='top-menu']//a[@title='Группы доступа']")
#
#     CreateGroup = Button("//button[contains(., 'Создать группу')]")  # Добавить группу
#
#     # Создание группы
#
#     SysGrp = Button("//form[@id='grpChForm']//div[@id='itSystems']//label[@id='itSystems_label']")  # Вкладка Системы
#
#     GrpMtdName = Input("//input[@id='grpMtdName']")
#     GrpMtdSsn = Input("//input[@id='grpMtdSsn']")
#     GrpMtdInf = Input("//textarea[@id='grpMtdInf']")
#
#     CreateGrp = Button("//form[@id='grpChForm']//button[contains(., 'Создать')]")  # создать
#     SaveGrp = Button("//form[@id='grpChForm']//button[contains(., 'Сохранить')]")  # сохранить
#     DeleteGrp = Button("//h1//button[contains(., 'Удалить группу')]")  # удалить
#     CancelGrp = Button("//form[@id='grpChForm']//button[contains(., 'Отмена')]")  # отмена
#
#     DeleteGrpOK = Button("//form[@id='rmGrpFrm']//button[contains(., 'Удалить')]")  # Подтверждение удаления
#
#     @staticmethod  # Выбор типа доступа группы
#     def ChooseAccessTypeGrp(atype):
#         Button("//form[@id='grpChForm']//table[@id='accessType']//label[contains(., '" + atype + "')]").click()
#
#     @staticmethod  # Выбор системы для группы
#     def ChooseItSysGrp(itsys):
#         Button("//div[@id='itSystems_panel']//ul//li[contains(., '" + itsys + "')]").click()
#
#     @staticmethod
#     def EditGrpFromTableTechConsole(grp):
#         Button(
#             "//div[@id='grpTbl']//tbody[@id='grpTbl_data']//tr[contains(., '" + grp + "')]//a[@title='Редактировать группу']").is_visible()
#         Button(
#             "//div[@id='grpTbl']//tbody[@id='grpTbl_data']//tr[contains(., '" + grp + "')]//a[@title='Редактировать группу']").click()
#
#     # Сервисы
#
#     AccSrvs = Button("//div[@class='top-menu-wrap']//a[@title='Сервисы']")
#
#     Ra = Button("//div[@id='orgProfileTab']//a[contains(.,'Обеспечение выдачи ПЭП')]")
#
#     AddRA = Button("//button[contains(.,'Добавить центр обслуживания')]")  # Добавить цо
#
#     RaName = Input("//input[@id='raName']")  # Название
#     RaAddress = Input("//textarea[@id='address']")  # Введите адрес
#     RaHouse = Input("//input[@id='house']")  # Дом
#     RaFrame = Input("//input[@id='frame']")  # Корпус
#     RaBuild = Input("//input[@id='build']")  # Строение
#     RaZipCode = Input("//input[@id='zipCode']")  # Индекс
#     RaLat = Input("//input[@id='raLat_input']")  # Широта
#     RaLng = Input("//input[@id='raLng_input']")  # Долгота
#     RaTime = Input("//input[@id='time']")  # Время работы
#     RaContacts = Input("//input[@id='contacts']")  # Контакты
#     RaAddInf = Input("//textarea[@id='addInf']")  # Дополнительная информация
#
#     RaReg = Button(
#         "//form[@id='raDlgForm']//div[@id='raDialogPnl']//span[contains(text(), 'Регистрация')]")  # ЧекБокс Регистрация
#     RaCfm = Button(
#         "//form[@id='raDlgForm']//div[@id='raDialogPnl']//span[contains(text(), 'Подтверждение')]")  # ЧекБокс Подтверждение
#     RaRcr = Button(
#         "//form[@id='raDlgForm']//div[@id='raDialogPnl']//span[contains(text(), 'Восстановление')]")  # ЧекБокс Восстановление
#
#     RaOpnModeWeekends = Button(
#         "//form[@id='raDlgForm']//div[@id='raDialogPnl']//span[contains(text(), 'Работает в выходные')]")  # ЧекБокс Работает в выходные
#     RaOpnModeEarly = Button(
#         "//form[@id='raDlgForm']//div[@id='raDialogPnl']//span[contains(text(), 'Раннее время работы (открыт с 8:00)')]")  # ЧекБокс Раннее время работы (открыт с 8:00)
#     RaOpnModeLate = Button(
#         "//form[@id='raDlgForm']//div[@id='raDialogPnl']//span[contains(text(), 'Позднее время работы (открыт после 19:00)')]")  # ЧекБокс Позднее время работы (открыт после 19:00)
#
#     SaveRa = Button("//form[@id='raDlgForm']//button[contains(., 'Сохранить')]")  # сохранить
#     DeleteRa = Button("//form[@id='rmRaFrm']//button[contains(., 'Удалить')]")  # elfkbnm
#     CancelRa = Button("//form[@id='raDlgForm']//button[contains(., 'Отмена')]")  # отмена
#
#     @staticmethod
#     def FillFormRa(clearall=False, raname=u"Тестовый ЦО", sitystreet=u"Воронежская область", house="1", frame="2",
#                    build="3", flat="4",
#                    streetadnzip=u"Воронежская область, Новоусманский район, свх Воронежский поселок, Воронежская улица",
#                    ralat="2.863180", ralng="53.863180",
#                    ratime="18-00", racontacts=u"Контакты", raaddinf=u"Дополнительная информация"):
#         if clearall:
#             EsiaConsoleTechPage.RaName.clear()
#             EsiaConsoleTechPage.RaAddress.clear()
#             EsiaConsoleTechPage.RaHouse.clear()
#             EsiaConsoleTechPage.RaFrame.clear()
#             EsiaConsoleTechPage.RaBuild.clear()
#             EsiaConsoleTechPage.RaLat.clear()
#             EsiaConsoleTechPage.RaLng.clear()
#             EsiaConsoleTechPage.RaZipCode.clear_backspace()
#             EsiaConsoleTechPage.RaTime.clear()
#             EsiaConsoleTechPage.RaContacts.clear()
#             EsiaConsoleTechPage.RaAddInf.clear()
#         else:
#             EsiaConsoleTechPage.RaName.send_keys_by_xpath(raname)
#             EsiaConsoleTechPage.RaAddress.send_keys_by_xpath(sitystreet)
#             EsiaPersonPage.ChooseOptionFromListFIAS(streetadnzip)
#             EsiaConsoleTechPage.RaHouse.send_keys_by_xpath(house)
#             EsiaConsoleTechPage.RaFrame.send_keys_by_xpath(frame)
#             EsiaConsoleTechPage.RaBuild.send_keys_by_xpath(build)
#             EsiaConsoleTechPage.RaLat.send_keys_by_xpath(ralat)
#             EsiaConsoleTechPage.RaLng.send_keys_by_xpath(ralng)
#             EsiaConsoleTechPage.RaTime.send_keys_by_xpath(ratime)
#             EsiaConsoleTechPage.RaContacts.send_keys_by_xpath(racontacts)
#             EsiaConsoleTechPage.RaAddInf.send_keys_by_xpath(raaddinf)
#             EsiaConsoleTechPage.RaReg.click()
#             EsiaConsoleTechPage.RaCfm.click()
#             EsiaConsoleTechPage.RaRcr.click()
#             EsiaConsoleTechPage.RaOpnModeWeekends.click()
#             EsiaConsoleTechPage.RaOpnModeEarly.click()
#             EsiaConsoleTechPage.RaOpnModeLate.click()
#
#     @staticmethod
#     def EditRaFromTableTechConsole(ra):
#         Button(
#             "//div[@id='raTbl']//tbody[@id='raTbl_data']//tr[contains(., '" + ra + "')]//button[@title='Редактировать центр обслуживания']").is_visible()
#         Button(
#             "//div[@id='raTbl']//tbody[@id='raTbl_data']//tr[contains(., '" + ra + "')]//button[@title='Редактировать центр обслуживания']").click()
#
#     @staticmethod
#     def DeleteRaFromTableTechConsole(ra):
#         Button(
#             "//div[@id='raTbl']//tbody[@id='raTbl_data']//tr[contains(., '" + ra + "')]//button[@title='Удалить центр обслуживания']").is_visible()
#         Button(
#             "//div[@id='raTbl']//tbody[@id='raTbl_data']//tr[contains(., '" + ra + "')]//button[@title='Удалить центр обслуживания']").click()
#
#     @staticmethod
#     def EditFormRa(clearall=False, raname=u"ЦО Тестовый", sitystreet=u"Ростовская область, Азов город, Ростовская улица", house="5", frame="6", build="7",
#                    streetadnzip=u"Ростовская область, Азов город, Ростовская улица", ralat="5.863180",
#                    ralng="13.863180",
#                    ratime="05-00", racontacts=u"Контакты1", raaddinf=u"Дополнительная информация111"):
#         if clearall:
#             EsiaConsoleTechPage.RaName.clear()
#             EsiaConsoleTechPage.RaAddress.clear()
#             EsiaConsoleTechPage.RaHouse.clear()
#             EsiaConsoleTechPage.RaFrame.clear()
#             EsiaConsoleTechPage.RaBuild.clear()
#             EsiaConsoleTechPage.RaLat.clear()
#             EsiaConsoleTechPage.RaLng.clear()
#             EsiaConsoleTechPage.RaZipCode.clear_backspace()
#             EsiaConsoleTechPage.RaTime.clear()
#             EsiaConsoleTechPage.RaContacts.clear()
#             EsiaConsoleTechPage.RaAddInf.clear()
#         else:
#             EsiaConsoleTechPage.RaName.send_keys_by_xpath(raname)
#             EsiaConsoleTechPage.RaAddress.send_keys_by_xpath(sitystreet)
#             EsiaPersonPage.ChooseOptionFromListFIAS(streetadnzip)
#             EsiaConsoleTechPage.RaHouse.send_keys_by_xpath(house)
#             EsiaConsoleTechPage.RaFrame.send_keys_by_xpath(frame)
#             EsiaConsoleTechPage.RaBuild.send_keys_by_xpath(build)
#             EsiaConsoleTechPage.RaLat.send_keys_by_xpath(ralat)
#             EsiaConsoleTechPage.RaLng.send_keys_by_xpath(ralng)
#             EsiaConsoleTechPage.RaTime.send_keys_by_xpath(ratime)
#             EsiaConsoleTechPage.RaContacts.send_keys_by_xpath(racontacts)
#             EsiaConsoleTechPage.RaAddInf.send_keys_by_xpath(raaddinf)
#             EsiaConsoleTechPage.RaCfm.click()
#             EsiaConsoleTechPage.RaOpnModeEarly.click()
#
#         # История операций
#
#     Opn = Button("//div[@class='top-menu-wrap']//ul[@class='top-menu']//a[@title='История операций']")
#
#     FirstStringInOpn = Text("//span[@id='opnTbl:0:nameMsg']")
#
#     @staticmethod
#     def ChooseEmpFromTableTechConsole(emp=u"Эксплуатация ИЭП"):
#         Button("//div[@id='empsTbl']//tbody[@id='empsTbl_data']//a[contains(., '" + emp + "')]").click()
#
#     @staticmethod
#     def FillFormSys(clearall=False, sysname="SystemTest", sysnsiid=u"MNT123", sysinf=u"Тестовая система для автотестов",
#                     syshomeurl="https://esiatest@esia.ru", sysstufio=u"Хащинин", fiostu=u"Хащинин Дмитрий Игоревич"):
#         if clearall:
#             EsiaConsoleTechPage.SysName.clear()
#             EsiaConsoleTechPage.SysNsiId.clear()
#             EsiaConsoleTechPage.SysInf.clear()
#             EsiaConsoleTechPage.SysHomeUrl.clear()
#             EsiaConsoleTechPage.SysStuFio.clear()
#         else:
#             EsiaConsoleTechPage.SysName.send_keys_by_xpath(sysname)
#             EsiaConsoleTechPage.SysNsiId.send_keys_by_xpath(sysnsiid)
#             EsiaConsoleTechPage.SysInf.send_keys_by_xpath(sysinf)
#             EsiaConsoleTechPage.SysHomeUrl.send_keys_by_xpath(syshomeurl)
#             EsiaConsoleTechPage.SysStuFio.send_keys_by_xpath(sysstufio)
#             EsiaConsoleTechPage.ChooseFioFromSysStu(fiostu)
#
#     @staticmethod
#     def EditSysForm(clearall=False, sysname="SystemTest", sysinf=u"Тестовая система для автотестов",
#                     syshomeurl="https://esiatest@esia.ru", sysstufio=u"Хащинин", fiostu=u"Хащинин Дмитрий Игоревич"):
#         if clearall:
#             EsiaConsoleTechPage.SysName.clear()
#             EsiaConsoleTechPage.SysInf.clear()
#             EsiaConsoleTechPage.SysHomeUrl.clear()
#             EsiaConsoleTechPage.SysStuFio.clear()
#         else:
#             EsiaConsoleTechPage.SysName.send_keys_by_xpath(sysname)
#             EsiaConsoleTechPage.SysInf.send_keys_by_xpath(sysinf)
#             EsiaConsoleTechPage.SysHomeUrl.send_keys_by_xpath(syshomeurl)
#             EsiaConsoleTechPage.SysStuFio.send_keys_by_xpath(sysstufio)
#             EsiaConsoleTechPage.ChooseFioFromSysStu(fiostu)
#
#     @staticmethod
#     def FillFormGrp(clearall=False, grpmtdname="GroupTest", grpmtdssn="123456",
#                     grpmtdinf=u"Тестовое описание группы доступа",
#                     atype=u"Непубличная"):
#         if clearall:
#             EsiaConsoleTechPage.SysGrp.click()
#             EsiaConsoleTechPage.ChooseItSysGrp(u"Не указана")
#             EsiaConsoleTechPage.GrpMtdName.clear()
#             EsiaConsoleTechPage.GrpMtdSsn.clear()
#             EsiaConsoleTechPage.GrpMtdInf.clear()
#         else:
#             EsiaConsoleTechPage.SysGrp.click()
#             EsiaConsoleTechPage.ChooseItSysGrp("SystemTest")
#             EsiaConsoleTechPage.GrpMtdName.send_keys_by_xpath(grpmtdname)
#             EsiaConsoleTechPage.GrpMtdSsn.send_keys_by_xpath(grpmtdssn)
#             EsiaConsoleTechPage.GrpMtdInf.send_keys_by_xpath(grpmtdinf)
#             EsiaConsoleTechPage.ChooseAccessTypeGrp(atype)
#
#     @staticmethod
#     def EditFormGrp(clearall=False, grpmtdname="GroupTest1",
#                     grpmtdinf=u"Редактирование тестового описание группы доступа"):
#         if clearall:
#             EsiaConsoleTechPage.GrpMtdName.clear()
#             EsiaConsoleTechPage.GrpMtdInf.clear()
#         else:
#             EsiaConsoleTechPage.GrpMtdName.send_keys_by_xpath(grpmtdname)
#             EsiaConsoleTechPage.GrpMtdInf.send_keys_by_xpath(grpmtdinf)
