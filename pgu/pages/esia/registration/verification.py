#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button, Input, Text
from pgu.pages import BasePage


class EsiaVerificationPage(BasePage):

    #url = Url("/registration/cfmPhone.xhtml")

    InputSMSCodeField = Input("//input[@id='code']")

    SumbitSMSCodeButton = Button("//button[@id='confirm']/span[contains(text(), 'Продолжить')]/ancestor::button")
    SendSMSCodeAgainButton = Button("//button[@id='resendCode']/span[contains(text(), 'Выслать еще раз')]/ancestor::button")

    ErrorMessageSMSCodeEmpty = Text("//div[@id='msg_code']/span[text() = 'Введите код']")
    ErrorMessageSMSCodeWrong = Text("//div[@id='msg_code']/span[text() = 'Неверный код подтверждения.']")
    ErrorMessageSMSCodeExpired = Text("//div[@id='codeExpiredState']/p[contains(text(), 'Срок действия кода подтверждения истек')]")
    #ErrorMessageSMSCodeExpired.set_countdown("//div[@id='codeAliveState']/p[contains(text(), 'Код действителен еще')]")

    ChangePersonalDataLink = Button("//a[@href='/registration' and contains(text(), 'Изменить данные')]")

    InfoEmailSendText = Text("//div[@class='content-box link-is-sent']/p[contains(text(), 'отправлено письмо')]")

    Surname = Input("//input[@id='lastName']")
    MobilePhone = Input("//input[@id='phone']")
    Email = Input("//input[@id='email']")

    @classmethod
    def GoToEsiaRegistrationPage(cls):
        cls.ChangePersonalDataLink.click()

    @classmethod
    def FillSmsVerificationCodeInputAndSubmit(cls, text = ""):
        cls.InputSMSCodeField.click()
        cls.InputSMSCodeField.send_keys_by_xpath(text)
        cls.SumbitSMSCodeButton.click()

    @classmethod
    def WaitUntilCodeExpiresAndClickSubmit(cls, wait = 300):
        if cls.ErrorMessageSMSCodeExpired.is_visible(wait=wait):
            cls.SendSMSCodeAgainButton.click()
            cls.InputSMSCodeField.send_keys_by_xpath_delay("98765432")
            cls.SumbitSMSCodeButton.click()
