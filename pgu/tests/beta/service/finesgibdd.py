#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.driver.engine import DriverEngine
from pgu.tests.conftest import step, testcase, skipif, issue, feature, story

from pgu.pages.esia import EsiaHomePage
from pgu.pages.beta.core import BetaHomePage, CategoryPage
from pgu.pages.beta.service.finesgibdd import ReceptionPage, ApplicationPage, Vars


@feature("Услуги")
@story("Штрафы ГИБДД")
@issue("PGU-1")
@testcase("http://pgu-uat-beta.test.gosuslugi.ru/10001/form")
class FinesSanitySuite:
    """ Sanity-тестирование услуги "Штрафы ГИБДД" """

    @skipif(False, reason="Ошибка при открытии главной страницы портала ПГУ")
    def case_fines_positive(self, url: str, engine: DriverEngine, screenshot):

        with step("Переход на страницу ЕСИА"):
            BetaHomePage.click_login_into_esia()

        with step("Выбираем авторизацию и вводим логин/пароль"):
            if EsiaHomePage.page_error():
                raise AssertionError("Не удалось авторизоваться в ЕСИА")
            EsiaHomePage.Login(Vars.esia_email, Vars.esia_pass).by_email_or_phone()

        with step("Переход на страницу портала госуслуг и выбор штрафов ГИБДД"):
            assert "Портал государственных услуг" in engine.find_element_by_xpath("(//h1)[1]").text
            BetaHomePage.CategoryBottomBtn.click()
            CategoryPage.Goto.service_by_name("Штрафы ГИБДД")

        with step("Заполнить заявку в электронном виде"):
            ReceptionPage.online_claim_type()

        with step("Проверка появления формы"):
            if not ApplicationPage.FormTitle.is_exists() or not ApplicationPage.MainForm.is_visible():
                raise AssertionError("Main service form not found")

        with step("Поле имя не редактируемо и содержит фамилию пользователя"):
            assert ApplicationPage.DriverName.is_editable() is False
            assert Vars.esia_f in ApplicationPage.DriverName.get_value()

        with step("Поле 'номер водительского удостоверения'"):
            ApplicationPage.DriveLicenseNumLabel.click()
            ApplicationPage.DriveLicenseNumInput.clear()
            ApplicationPage.DriveLicenseNumInput.send_keys_by_xpath_delay("74AA082008")
            assert ApplicationPage.DriveLicenseNumInput.get_value() in "74 AA 082008"

        with step("Проверка ввода АМТС"):
            ApplicationPage.GosNumAuto.clear()
            ApplicationPage.GosNumAuto.send_keys_by_xpath_delay("A001AA199")
            assert ApplicationPage.GosNumAuto.get_value() in "A001AA199"

        with step("Проверка свидетельства о регистрации АМТС"):
            ApplicationPage.TSRegisterNum.clear()
            ApplicationPage.TSRegisterNum.send_keys_by_xpath("77УЕ204623")
            assert ApplicationPage.TSRegisterNum.get_value() in "77 УЕ 204623"

        with step("Отправка запроса на проверку штрафов"):
            screenshot("Перед отправкой формы")
            ApplicationPage.FindFinesBtn.click()

        with step("Проверка появления штрафов ГИБДД"):
            if not ApplicationPage.FinesTitle.is_visible(15):
                if ApplicationPage.ErrorHeader.is_visible(15):
                    print(ApplicationPage.ErrorHeader.get_text())
                    raise AssertionError("Ошибка формы")
                raise AssertionError("Штрафы не найдены")

