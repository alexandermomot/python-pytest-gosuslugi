#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import logging

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from assets.objects.baseobject import BaseObject


class Element(BaseObject):

    def __init__(self, xpath):
        BaseObject.__init__(self, xpath)

    def click(self, ignore_exceptions: bool=False):
        """Click on element by left mouse button.
        :param ignore_exceptions: throw exception if appears, or ignore it
        :return: None
        """
        logging.info("Click [%s]" % self.xpath)
        self.driver.find_element_by_xpath_and_click(self.xpath, ignore_exceptions)

    def is_exists(self):
        logging.info("Is exists [%s]" % self.xpath)
        return self.driver.find_element_by_xpath_and_return(self.xpath, ignore_exception=True) is not None

    def is_visible(self, wait: int=10):
        logging.debug('[ARGS] wait %s ' % wait)
        el = self.driver.find_element_by_xpath_and_wait_until_becomes_visible(self.xpath, wait=wait, ignore_exception=True) or \
             self.driver.find_element_by_xpath_and_wait_until_becomes_invisible(self.xpath, wait=1, ignore_exception=True)
        out = "Element: [%s] is visible: [%s] and exist in DOM: [%s] within [%s] seconds"
        if el and isinstance(el, WebElement):
            logging.info(out % (self.xpath, el.is_displayed(), el is not None, wait, ))
        else:
            logging.info("Element: [%s] wasn't found in DOM within [%s] seconds" % (self.xpath, wait, ))
            return None
        return bool(el is not None and el.is_displayed())

    def is_invisible(self, wait=10):
        logging.debug('[ARGS] wait %s ' % wait)
        el = self.driver.find_element_by_xpath_and_wait_until_becomes_invisible(self.xpath, wait=wait, ignore_exception=True) or \
             self.driver.find_element_by_xpath_and_wait_until_becomes_visible(self.xpath, wait=1, ignore_exception=True)
        out = "Element: [%s] is invisible: [%s] and exist in DOM: [%s] within [%s] seconds"
        if el and isinstance(el, WebElement):
            logging.info(out % (self.xpath, not el.is_displayed(), el is not None, wait, ))
        else:
            logging.info("Element: [%s] wasn't found in DOM within [%s] seconds" % (self.xpath, wait, ))
            return None
        return bool(el is not None and not el.is_displayed())

    def press_up(self):
        self.driver.press_button(self.xpath, Keys.ARROW_UP)

    def press_down(self):
        self.driver.press_button(self.xpath, Keys.ARROW_DOWN)

    def press_enter(self):
        self.driver.press_button(self.xpath, Keys.ENTER)

    def scroll_view(self):
        logging.info("Scrolling view to [%s]" % self.xpath)
        self.driver.find_element_by_xpath_and_move(self.xpath)

    def get_xpath(self):
        logging.info("Xpath of the element [%s]" % self.xpath)
        return self.xpath

    def get_elem(self):
        logging.info("Exporting the Element [%s] outside" % (self.xpath,))
        return self.driver.find_element_by_xpath_and_return(self.xpath, )

    def hover(self):
        logging.info("Hovering mouse over element [%s]" % (self.xpath, ))
        self.driver.find_element_by_xpath_and_move(self.xpath)

    def submit(self, ignore_exceptions=False):
        logging.info("Submit form [%s]" % (self.xpath,))
        self.driver.find_element_by_xpath_and_return(self.xpath, ignore_exception=ignore_exceptions).submit()

