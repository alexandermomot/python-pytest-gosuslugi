#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button
from pgu.pages.beta.service import BaseReceptionPage, BaseClaimTypePage

class ReceptionPage(BaseReceptionPage, BaseClaimTypePage):

    @classmethod
    def choose_option_type(cls, otype="по месту жительства"):
        PassportTypeButton = Button("//a[contains(text(),'%s')]" % otype)
        PassportTypeButton.click()


