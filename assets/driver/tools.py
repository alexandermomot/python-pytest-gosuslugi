#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import codecs, re
from vars import SystemVars


class Tools:

    @classmethod
    def read_javascript(cls, script_name):
        """
        Reading javascript file, return it as string, also recoding into the utf-8
        :param script_name: filename of the javascript in scripts folder
        :rtype: str
        """
        with codecs.open(script_name, 'r', encoding='utf-8') as javascript_file:
            return javascript_file.read()

    @classmethod
    def read_html(cls, template_name):
        """
        Reading html templates, trims html comments inside it, also recoding all the stuff into utf-8
        :param template_name: filename of the template
        :rtype: str
        """
        with codecs.open(template_name, 'r', encoding='utf-8') as html_file:
            return re.sub("<!--.*?-->", "", html_file.read())

class Reader(Tools):

    def __init__(self):
         raise Exception("This is an action class, use it like this: Reader.Template.screenshot()")



    class Template(Tools):

        screenshot_template = None
        template_path = SystemVars.system.HTML_REPORT_TEMPLATES

        def __init__(self):
             raise Exception("This is an action class, use it like this: Reader.Template.screenshot()")

        @classmethod
        def screenshot(cls):
            """
            Returns screenshot template for report
            :rtype: str
            """
            if cls.screenshot_template is None: # Prevent reading script from the disk every time it invokes
                cls.screenshot_template = cls.read_html(cls.template_path + "screenshot.html")
            return cls.screenshot_template


    class Javascript(Tools):

        center_view_script = None
        set_geo_script = None
        script_path = SystemVars.system.SCRIPTS_ROOT

        def __init__(self):
            raise Exception("This is an action class, use it like this: Reader.Javascript.center_view('xpath')")

        @classmethod
        def center_view(cls, xpath):
            """
            Returns script to execute, center the screen on the element while test is running
            :rtype: str
            """
            if cls.center_view_script is None: # Prevent reading script from the disk every time it invokes
                cls.center_view_script = cls.read_javascript(cls.script_path + "centerview.js")
            return cls.center_view_script.replace("%xpath%", cls.verify_xpath(xpath))

        @classmethod
        def set_geo(cls, coords):
            """
            Returns script to execute to set the coords on the geolocation services
            :rtype: str
            """
            if cls.set_geo_script is None: # Prevent reading script from the disk every time it invokes
                cls.set_geo_script = cls.read_javascript(cls.script_path + "geolocation.js")
            return cls.set_geo_script.replace("%coords%", coords)

        @classmethod
        def verify_xpath(cls, xpath):
            return xpath.replace("\"", "'")
