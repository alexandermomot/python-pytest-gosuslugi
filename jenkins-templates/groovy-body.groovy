<table>
  <tr><th>Проект:</th><td>${project.name}</td></tr>
  <tr><th>Дата теста:</th><td>${it.timestampString}</td></tr>
  <tr><th>Длительность теста:</th><td>${build.durationString}</td></tr>
</table>

<!-- PARAMETERS -->
<h2>Параметры запуска</h2>
	<ul>
<%
  import hudson.model.*
  def parametersAction = build.getAction(ParametersAction.class)

  if (parametersAction != null)
  {
    for (p in parametersAction.parameters)
    {
      %><li class="param"><b><%=p.name%></b>: <%=p.value%></li><%
    }
  }
%></ul>

<!-- REPORT -->
<h2>Отчет Allure</h2>
<div>
<a href="http://jenkins-new.testgate.egovdev.ru/jenkins/job/pytest-selenium-sanity-pgu/allure">
	<img alt=" &bull; Allure Report" width="30" height="30" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAC9FBMVEUAAAD/3QTiBhKm0aP/3gT//yj/3QL4yRH11AWfw5/N+6Kn0aThxA//3QX/3Af/3QQ7q0f/3gT/40TfwhD/3gSo1Kjhww/H4MOm0aPVAgCl0aPgxA/jBhLiBhGl0aHkBRP+2wf7DgP/3QTlCRGm0qU0pTThxA//3QTdwBT2kg48rEfgxA//5AjhBRLjBhLlAAA8q0dIrUbrzAv01Aj/3hThww7HhmqYvTno2lbvjQ7+3QfhABPjGRI4q0TkCBX7/9j52Qb/3grewg/jCBL96GDgwg7x0wxpu2634V3nyQ3h44PjCxUAp0ngww7rzmD63STs0RVXtGDixh3/3gfmyij/3xL/4Bfr0kXsz3H121P62yfkGRJQsVXvzwr/3wb/3x3lBQ7/3gnixy3+3Qn/4DD82w3w0zTjyDH+4jn63BF3wH7t0Tr/3hHt1kmMyZP/3xTuZm732ziPy4n/3haTvDfhxRfiHh2m0KH/4QPixB//3xrujA7hxiP/3gam0KLjEhf/3Qn/4Cfjxilft2ZbtVtkt2JuvHb/3xLjyzz/3gVsuFj/3g/jxyv/5Ef/5FDevhDsjBBJrkpfskF7tzy/ySzPzSXd0h5bsELh1Bvmxw/m2VYcq0ii0qSk0aDHhmvm2FX93RP+3R9Pr0vT0jv+4jz/3xBOtEqoxkj+3iP/5Uhbsk394T8ApkmZvTl7woHqSVGBvmXjSFjiFxHlBxThAABNuVXgww87rEf/3QT/6QX/5gTtBxP/7g//3QfiBRH/3gCx166u26v/4gTlxw7pyxD/3wT/6AQ+tUqm0qM5rEftzwwAp0n/4QL/3wCj0qX/7ATnBRPjChH/5QRCrUbiHBv93AVDuFD/6gE5sUb/4jNKtVI2rEP/5AJItUn/4hnoyQjjxQit264rrEgbrEjkxxv/6BDOj3Tu4mAAsk5Au03nNj2qwjLqAxPqGhL1kQ//9ASy162j0Z/nPECkyz+hxzzmMDj/4jfH0i7tHh35BxT/7A7wAACfFuxwAAAAtnRSTlMA/v7+/QL5BAUDAf7+cCz8/e27/vgd/ED5BuH6+Sz+7S8F+x3+A+3hLAL7Lxz54f5w/v7+/vf+/v7++/7++i8G/vz79kD+/roK/hP9/PcbE/7l3dSqlk1GNysY9fLx8OznyMG2r6qWjIGBeGldWEM+PTQsIP7++/n59fXy6ufh2trU0M7Cn5eFfHZraGJcOC8t/v7+/v7+/f38+ePh4eHh4dzWyse/u7u7t352cHBmYkdALCciIcWD6AYAAAZ9SURBVGje7ZpltBJBFIAH3QVBFBXEwMR82K0YKAp2d3d3d3d3d3crrtgiBoqF2N3d7R8vCMwusct6lh+e4/fer3nnzPfu3Jk7dzig//zn30eWMVXGECRSNhKEIONwCPSvsv8xY6YwVEvORrWcTHrnRKxkRLuHt8gdxPAWy5akHpY6LDC+dHm2FdkYDK2HSBZJKlQqzY/XB5h8//X0wbGIPDj27NqTk3Ss71pWQBI2SdoWF8umycWgX57CKVP2TxmJ/ovEBcV0VGJrIg5J+gNpDtOJd/hKnsLnT8ePROZ0RY8UpBgoLmfrDRYOSTwGHknmSI6EXomIAaHYXxfJZLGVUNrjqu4oQUwlgPhCaxmSxFZCae3Hu8RYAhbx/tqNwBIjCc79eITiYiuh9Leb9USSqCWHfZKE4cCSINyqC20RIqOTVK5cuZ9XkjACICEKlvFCYAdhL53/xBQUF50kT548lzwnPt1p+A3+AdKd/1DUecSL8wgRULjNNuOJWjj3bJLDlb+8fPnx4tMHRyPx/ujRZ3e/3vBy94jCq7GLCtiUSrna0R7OPbfkYprtPXr02NkgBRvlfDTYXMappUQEUfqERWmRy9W2gT1RgigkZfcgHrS5poVkOOQehcVi0Tjq4NyzSUqhVHFxUlZIiReyApo26LbebAMFOAD1/cddkSQqSRyKElj/dschGWD4g9zoGFUJLEJKYLpqtSqqQeGXqE01JoA7rCQeXwm2dD5hUVswmntDGiKJoJFAkg2tHBpaKHKNuR0Mh5Nc8lMZS6INpf7j+4wFu9+3PpIES3Y1ftO4RIA3jflJSITamY0gCWB0tDIgMiSS17mKB7iYC0uiDKXhkHsamkRtCilhCdCMwW+LYd4OnoEkPPvTCTVM9AXT5K9VHSwMSeJm+wecO+7HOiADSPiFUmmUA0LBuTflbx8iSWpVUYHr7bgKS6K2dIXc0yzqilmzw8RMyWWxXyLCEl65r+OgS+RGXMJwJDTJOf4SmKPBwHu+rEARU5oqltbXg1FBJVBG2jv+SEChtBUQaS+shQ5cUAkioYTd08hBYVHKHW6CUFCXJyNSJqgExUEJM8FKgaK0nYDWRXVyBNzEwklwCVMqKxYg7CIPsGDjwS2QBJewvpaFBUR2ItCBN8MTYQnHFubexoc8KxVA7O/C8GG0+yB4S3AJa27VU1iisNvrIUlA0tRtLuDHXNqMJTy3cYf9sK+wRbvf/5AESfasFaualD6MtqrTsYRnCRt5TUyTKAh4SJIByQkNrtO2qjmwhKdlakGnAlsIrdW/jZkSaM8K/a0kAWTltp6+Xtba1UMlwN9LYGHqWhU0B0Gc7AijQkpgovoFb+sp2mpdzpYTSQSVwJnfcE1M0V9eJydC+RJSIiNR5ydH6GnXn/RmREgJtJIrrSp6INRleA3LhJTAZB0uaEUYCt71JEwkpEQC7f05LcH8hAJGhZSQnoeKmPkBRV0YFlQCh937sMOLZc3WG6YWRoLLllVL372UNZEv60LVLti+HZ5Q9MMuxgVYqEigI2rOPOt2Ee6IaBK5j7+QxEHWGdsX7ve2MBwkqWhU+jHKed8n0KXedUIg2HE8aWIYZUhyNC2QH+NoCpHwc1RYDduXoH9c1BEuyjANN+GDdyMBk02602eB00m4fQ4xLlpCSbKgXk0eVnG9qgEawuPwFi0S3MJ19TDZ2FM618Ozs+aUdor0jKIlVMMNgeyYeUa3z+V6dHb2XLdT5MZFSygJOAxjTun2AS7X2Ud95jmdely02CUkD0mnm2eSgAPId+YspObk0MQwZxSSBCQnWaQeKqDkNa9DIF7OuPI9rDL/W0eURcYhgcQnRjwYd7UkBPKHJPtcukeL90J43JH0RAZZdBhQtya3PIEENEnubAIHl4Q4UmbNxmScrF9X3kvN6xAHRnd9VSU2CebnQW4+Pb951QPTkeTMnUnYwSoRR0GRKvlKetDto6M7NcaApFFJDnHhpqgiZ/Od2ReM7tbNbhAIm4QbnDdRWEnJF2MRksZUAlmv2QsCialEV/JqJyiYMZZcH10JFiumEsj6Nlis2EpKniovQ9KYSiCQJnj7CiMJOSdJSl4dFyHr+PoVETxQEASc+CR0YPsmh8Vilah4SkRBEsh6J1gsFkkGxbvL+3lx7drn53dO0Xkx2sAmkaDqkycm4svWLXmZ9OL6aoEQQBzskPyRZpEywNc6SzD/+c8/ym/0UsVqaVwuYAAAAABJRU5ErkJggg==">
</a>
</div>
<div>
<a href="http://jenkins-new.testgate.egovdev.ru/jenkins/job/pytest-selenium-sanity-pgu/allure">
	&bull; Allure Report
</a>
</div>

<!-- CHANGE SET -->
<% changeSet = build.changeSet
if (changeSet != null) {
  hadChanges = false %>
  <h2>Изменения</h2>
  <ul>
<% 	changeSet.each { cs ->
    hadChanges = true
    aUser = cs.author %>
      <li>Commit <b>${cs.revision}</b> by <b><%= aUser != null ? aUser.displayName : it.author.displayName %>:</b> (${cs.msg})
        <ul>
<%        cs.affectedFiles.each { %>
          <li class="change-${it.editType.name}"><b>${it.editType.name}</b>: ${it.path}</li>
<%        } %>
        </ul>
      </li>
<%  }

  if (!hadChanges) { %>
      <li>Изменений нет</li>
<%  } %>
  </ul>
<% } %>

<!-- ARTIFACTS -->
<% artifacts = build.artifacts
if (artifacts != null && artifacts.size() > 0) { %>
  <h2>Собранные артефакты</h2>
    <ul>
<%    artifacts.each() { f -> %>
      <li><a href="${rooturl}${build.url}artifact/${f}">${f}</a></li>
<%    } %>
    </ul>
<% } %>

<%
  testResult = build.testResultAction

  if (testResult) {
    jobName = build.parent.name
    rootUrl = hudson.model.Hudson.instance.rootUrl
    testResultsUrl = "${rootUrl}${build.url}testReport/"

    lastBuildSuccessRate = String.format("%.2f", (testResult.totalCount - testResult.result.failCount) * 100f / testResult.totalCount)
    lastBuildDuration = String.format("%.2f", testResult.result.duration)

    startedPassing = []
    startedFailing = []
    failing = []

    previousFailedTestCases = new HashSet()
    currentFailedTestCase = new HashSet()

    if (build.previousBuild?.testResultAction) {
      build.previousBuild.testResultAction.failedTests.each {
        previousFailedTestCases << it.simpleName + "." + it.safeName
      }
    }

    testResult.failedTests.each { tr ->
        packageName = tr.packageName
        className = tr.simpleName
        testName = tr.safeName
        displayName = className + "." + testName

        currentFailedTestCase << displayName
        url = "${rootUrl}${build.url}testReport/$packageName/$className/$testName"
        if (tr.age == 1) {
          startedFailing << [displayName: displayName, url: url, age: 1]
        } else {
          failing << [displayName: displayName, url: url, age: tr.age]
        }
    }

    startedPassing = previousFailedTestCases - currentFailedTestCase
    startedFailing = startedFailing.sort {it.displayName}
    failing = failing.sort {it.displayName}
    startedPassing = startedPassing.sort()
%>

<% if (testResult) { %>
<h2>Результаты тестов</h2>
<ul>
  <li>Всего тестов запущено: <a href="${testResultsUrl}">${testResult.totalCount}</a></li>
  <li>Количество дефектов &#47; дельта: ${testResult.failCount} ${testResult.failureDiffString}</li>
  <li>Процент успешных тестов: ${lastBuildSuccessRate}% </li>
</ul>
<% } %>

<% if (startedPassing) { %>
<h3>Эти тесты перестали падать. Good work!</h3>
<ul>
  <% startedPassing.each { %>
    <li>${it}</li>
  <% } %>
</ul>
<% } %>

<% if (startedFailing) { %>
<h3>Тесты, которые начали падать в этой сборке</h3>
<ul>
  <% startedFailing.each { %>
    <li><a href="${it.url}">${it.displayName}</a></li>
  <% } %>
</ul>
<% } %>

<% if (failing) { %>
<h3>Эти тесты падают уже продолжительное время. Кому-то срочно необходимо взглянуть на них!!!</h3>
<ul>
  <% failing.each { %>
    <li><a href="${it.url}">${it.displayName}</a> (Провален ${it.age} раз)</li>
  <% } %>
</ul>
<% } %>

<% } %>


<!-- BUILD FAILURE REASONS -->
<% if (build.result == hudson.model.Result.FAILURE) {
  log = build.getLog(10).join("\n")
  warningsResultActions = build.actions.findAll { it.class.simpleName == "WarningsResultAction" }

  if (warningsResultActions.size() > 0) { %>
    <h2>Ошибки сборки</h2>
    <ul>
    <% warningsResultActions.each {
        newWarnings = it.result.newWarnings
        if (newWarnings.size() > 0) {
          newWarnings.each {
            if (it.priority.toString() == "HIGH") { %>
              <li class="error">In <b>${it.fileName}</b> at line ${it.primaryLineNumber}: ${it.message}</li>
          <% }} %>
    <% }} %>
    </ul>
  <% } %>

<h2>Консоль: </h2>
<h3><a href="${rooturl}${build.url}">${rooturl}${build.url}</a></h3>
<pre class="console">${log}</pre>

<% } %>