#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button, Element, Text
from pgu.pages import BasePage


class BaseFormPage(BasePage):

    def __init__(self, url):
        BasePage.__init__(self, url)

    FormContent_Body = Element("//body//div[@id='form']//div[@class='PGU-FormContentBody']")

    DraftPopupContainer = Text("//div[@id='draftContainerLast']//*[contains(text(),'черновик')]")
    DraftPopupNewDraft = Button("//a[@class='new-draft']")
    DraftPopupContinueDraft = Button("//span[contains(@class,'continue-draft')]")

    YaMapsAnyLocation = Button("//div[contains(@id,'form.FormStep')]//div[contains(@class,'PGU-FieldMapWithListItemWrap')]")
    YaMapsPopupButton = Button("//div[contains(@id,'form.FormStep')]//div[contains(@class,'baloonContent-wrap')]//div[contains(@class,'Item')]/a")
    YaMapsError = Text("//div[contains(@id,'form.FormStep')]//div[contains(@class,'js-PGU-Map-error-content')]//div[contains(@class,'error-text')]")

    @classmethod
    def create_new_draft(cls):
        for i in range(5): # нехитрый способ быстрой проверки появления либо того, либо другого
            if cls.DraftPopupContainer.is_visible(wait=2):
                cls.DraftPopupNewDraft.click()
                return True
            if cls.FormContent_Body.is_visible(wait=2):
                return False

    @classmethod
    def yamaps_setlocation(cls):
        cls.YaMapsAnyLocation.is_visible(wait=15)
        cls.YaMapsAnyLocation.click()
        for i in range(5):
            if cls.YaMapsError.is_invisible(1):
                break
            else:
                Button("(" + cls.YaMapsAnyLocation.get_xpath() + ")[" + str(i + 1) + "]").click()
        cls.YaMapsPopupButton.is_visible(wait=15)
        cls.YaMapsPopupButton.click()

    class FormError:

        def __init__(self):
            pass

        FormContent_Body = Element("//body//div[@id='form']//div[@class='PGU-FormContentBody']")

        ERROR_TEXT = ""
        ERROR_FORMS = {
            "Attention": Text("//div[contains(@id,'form.FormStep0')]//h4[contains(text(), 'Внимание')]"),
            "ErrorDescription": Text("//div[@class='ITEMS_CONTAINER']//div[contains(text(),'заказ выбранной вами услуги сейчас невозможен')]"),
            "ClaimExistsError": Text("//div[contains(@id,'form.FormStep0')]//div[contains(text()[position()>1],'Дело уже существует')]"),
            "DepartmentUnavailableError": Text("//div[contains(@id,'form.FormStep0')]//div[contains(text()[position()>1],'Сервис ведомства недоступен')]"),
        }

        def __call__(self, *args, **kwargs):
            if not self.FormContent_Body.is_visible(30):
                self.FormContent_Body.is_exists()
                self.ERROR_TEXT = "Форма невидимая или не существует"
                return True
            else:
                return False

    class FormDepartmentError(FormError):

        ERROR_TEXT = ""

        def __call__(self, *args, **kwargs):
            if self.ERROR_FORMS["Attention"].is_visible(5):
                self.ERROR_FORMS["ErrorDescription"].get_text()
                self.ERROR_TEXT = "Ошибка сервиса"
                return True
            else:
                return False

    class FormPhotoUploadError(FormError):

        ERROR_TEXT = ""

        ERROR_PHOTO = {
            "UploadError": Text("//div[contains(@id,'imageEditorImageView')]//span[contains(text(), 'ошибка')]")
        }

        def __call__(self, *args, **kwargs):
            if self.ERROR_PHOTO["UploadError"].is_visible(1):
                self.ERROR_TEXT = self.ERROR_PHOTO["UploadError"].get_text()
                return True
            else:
                return False

    form_error = FormError()
    form_error_department = FormDepartmentError()
    form_photo_upload_error = FormPhotoUploadError()
