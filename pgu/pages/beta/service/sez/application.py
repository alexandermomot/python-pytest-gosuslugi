#!/usr/bin/python
# coding=utf-8
__author__ = 'alexander-momot'

from selenium.webdriver.common.keys import Keys
from assets.objects.elements import Button, Input, Text, Element
from pgu.pages.beta.service import BaseFormPage
from pgu.pages.beta.service.sez.vars import Vars


class ApplicationPage(BaseFormPage):

    page = "Общие для всех подуслуг объекты страницы 'Санэпидемназдор'"

    DraftPopupContainer = Text("//div[@class='popup']")
    DraftPopupNewDraft = Button("//a[contains(text(), 'Начать заново')]")
    DraftPopupContinueDraft = Button("//span[contains(@class,'continue-draft')]")

    # шаги и элементы шагов
    Step_PostAddress = Input("//div[@id='form.FormStep29.Panel30']//input[@class='PGU-FiasDropdown-Search-Input js-PGU-FiasDropdown-Search-Input']")
    Step_PostAddress_Select = Button("//div[@id='form.FormStep29.Panel30.Fias31']//div[@class='pgu-combobox-holder js-pgu-combobox-holder']//div[@class='PGU-Fias-Option-Wrap']")
    Step_PostAddress_AdditionalStreet = Input("//div[@id='form.FormStep29']//div[@class='row-Fias row-Fias-additionalStreet']//input")
    Step_PostAddress_House = Input("//div[@id='form.FormStep29']//div[@class='row-Fias row-Fias-house']//input")
    Step_PostAddress_Apartment = Input("//div[@id='form.FormStep29']//div[@class='row-Fias row-Fias-apartment']//input")

    Step_Layer = Element("//div[@class='PGU-FormStep']//h3/span[text()='Персональные данные']//ancestor::div[@class='PGU-FormStep']")
    Step1_Surname = Input("//div[@id='form.FormStep5']//input[@id='form.FormStep5.Panel34.Panel35.last_name_kon']")
    Step1_Name = Input("//div[@id='form.FormStep5']//input[@id='form.FormStep5.Panel34.Panel36.first_name_kon']")
    Step1_Patronymic = Input("//div[@id='form.FormStep5']//input[@id='form.FormStep5.Panel34.Panel37.middle_name_kon']")
    Step1_ContactPhone = Input(Step_Layer.get_xpath() + "//label[contains(text(),'Контактный телефон')]/ancestor::div[1]//input")
    Step1_EmailAddress = Input("//input[contains(@id, 'form.FormStep1_IP.Panel608.Panel609.email')]")

    Step5_Surname = Input("//input[@id='form.FormStep5.Panel34.Panel35.last_name_kon']")
    Step5_Name = Input("//input[@id='form.FormStep5.Panel34.Panel36.first_name_kon']")
    Step5_Phone = Input("//input[@id='form.FormStep5.Panel34.Panel38.phone']")
    Step5_Email = Input("//input[@id='form.FormStep5.Panel34.Panel39.email']")

    Step_RospotrebNadzorDepartment = Button("//div[@id='form.FormStep6.Panel133']//div[@class='PGU-ComboBoxFace']")
    Step_RospotrebNadzorDepartment_Input = Input("//div[@id='form.FormStep6.Panel133']//input[@name='search_query']")
    Step_RospotrebNadzorDepartment_Select = Button("//div[@id='form.FormStep6.Panel133']//div[@class='pgu-combobox-holder']//div[@data-value='30']")
    Step_NameOfEnterpreneuer = Input("//div[@id='form.FormStep7.Panel233.FieldRadio235_panel.FieldText61']//input[@id='form.FormStep7.Panel233.FieldRadio235_panel.FieldText61']")

    # radiobuttons
    Step_RadioButton_LE = Button("//div[@id='form.FormStep7.Panel233.FieldRadio234']//a/input[@type='radio']/following-sibling::i")
    Step_RadioButton_IP = Button("//div[@id='form.FormStep7.Panel233.FieldRadio235']//a/input[@type='radio']/following-sibling::i")

    Step_IpCompany_Name = Input("//div[@id='form.FormStep7.Panel233.FieldRadio235_panel.FieldText61']//input[@id='form.FormStep7.Panel233.FieldRadio235_panel.FieldText61']")

    Step_JuridicalAddress = Input("//div[@id='form.FormStep8']//input")
    Step_JuridicalAddress_Select = Button("//div[@id='form.FormStep8']//div[@class='pgu-combobox-holder js-pgu-combobox-holder']//div[@class='PGU-Fias-Option-Wrap']")
    Step_JuridicalAddress_AdditionalStreet = Input("//div[@id='form.FormStep8']//div[contains(@class,'row-Fias-additionalStreet')]//input")
    Step_JuridicalAddress_House = Input("//div[@id='form.FormStep8']//div[contains(@class,'row-Fias-house')]//input")
    Step_JuridicalAddress_Apartment = Input("//div[@id='form.FormStep8']//div[contains(@class,'row-Fias-apartment')]//input")

    Step_ProjectDocumentationNames = Input("//div[@id='form.FormStep9.Panel66']//textarea[@id='form.FormStep9.Panel66.name_proj']")

    SubmitFormButton = Button("//div[@id='form.finalStep.Panel110']//a[@id='form.finalStep.Panel110.FormButton111']")

    FinalStatusText = Text("//div[@class='form-text form-text-common']//dd[text() = 'Поставлен в очередь на обработку']")


    @classmethod
    def create_new_draft(cls):
        for i in range(5): # нехитрый способ быстрой проверки появления либо того, либо другого
            if cls.DraftPopupContainer.is_visible(wait=2):
                cls.DraftPopupNewDraft.click()
                return True
            if cls.FormContent_Body.is_visible(wait=2):
                return False

    @classmethod
    def click_and_type_postaddress(cls):
        cls.Step_PostAddress.clear()
        cls.Step_PostAddress.send_keys_by_xpath_delay(Vars.searchinput_address)
        cls.Step_PostAddress_Select.click()
        cls.Step_PostAddress_AdditionalStreet.send_keys_by_xpath("Мирлэнд ")
        cls.Step_PostAddress_House.send_keys_by_xpath("38")
        cls.Step_PostAddress_Apartment.send_keys_by_xpath("5")


    @classmethod
    def click_and_type_personal_email(cls):
        if cls.Step1_EmailAddress.is_visible(10) and cls.Step1_EmailAddress.is_editable():
            cls.Step1_EmailAddress.send_keys_by_xpath(Vars.personal_email)

    @classmethod
    def click_and_type_rospotrebnadzor_department(cls):
        cls.Step_RospotrebNadzorDepartment.click()
        cls.Step_RospotrebNadzorDepartment_Input.send_keys_by_xpath_delay(Vars.searchinput_sanepidemnadzor_department_name)
        cls.Step_RospotrebNadzorDepartment_Input.send_keys_by_xpath(Keys.ENTER)
        cls.Step_RospotrebNadzorDepartment_Select.click()

    @classmethod
    def set_personal_data_of_contact(cls):
        cls.Step5_Surname.clear()
        cls.Step5_Surname.send_keys_by_xpath(Vars.personal_f)
        cls.Step5_Name.clear()
        cls.Step5_Name.send_keys_by_xpath(Vars.personal_i)
        cls.Step5_Phone.clear()
        cls.Step5_Phone.send_keys_by_xpath(Vars.personal_tel)
        cls.Step5_Email.clear()
        cls.Step5_Email.send_keys_by_xpath(Vars.personal_email)


    @classmethod
    def click_and_type_ie_name(cls):
        cls.Step_RadioButton_IP.click()
        cls.Step_IpCompany_Name.click()
        cls.Step_IpCompany_Name.clear()
        cls.Step_IpCompany_Name.send_keys_by_xpath(Vars.ip_company_name)

    @classmethod
    def click_and_type_juridical_address(cls):
        cls.Step_JuridicalAddress.send_keys_by_xpath_delay(Vars.searchinput_address)
        cls.Step_JuridicalAddress_Select.click()
        cls.Step_JuridicalAddress_AdditionalStreet.send_keys_by_xpath("Мирлэнд ")
        cls.Step_JuridicalAddress_House.send_keys_by_xpath("38")
        cls.Step_JuridicalAddress_Apartment.send_keys_by_xpath("5")

    @classmethod
    def click_and_type_project_documentation_list(cls):
        cls.Step_ProjectDocumentationNames.click()
        cls.Step_ProjectDocumentationNames.send_keys_by_xpath(Vars.sanepidemnadzor_documentation_names)

    @classmethod
    def click_submit_page(cls):
        cls.SubmitFormButton.click()
