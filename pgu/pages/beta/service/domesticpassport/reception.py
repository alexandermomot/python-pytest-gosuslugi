#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button
from pgu.pages.beta.service import BaseReceptionPage, BaseClaimTypePage

class ReceptionPage(BaseReceptionPage, BaseClaimTypePage):

    @classmethod
    def find_and_click_category(cls, otype="Мне исполняется 20 или 45 лет"):
        PassportTypeButton = Button("//a[text()='%s']" % otype)
        PassportTypeButton.click()
