# #!/usr/bin/python
# # coding=utf-8
# # -*- coding: utf-8 -*-
#
# __author__ = 'dmitry.khaschinin'
#
# from pgu.pages.mailinator import InboxPage
# from pgu.pages import BasePage
# from assets.objects.elements import (
#     Button, Input,
#     Text )
#
#
# class EsiaOrganizationPage(BasePage):
#
#     #url = Url("https://esia-uat.test.gosuslugi.ru/profile/user/emps.xhtml")
#     ww = 2
#     ml = InboxPage("esia", "verification")
#     test_email = ml.get_email_address()
#
#     # Настройки учетной записи
#
#     # ИП
#
#     AddIE = Button("//a[contains (text(), 'создать учетную запись индивидуального предпринимателя')]")
#
#     PsoInn = Input("//input[@id='psoInn']")
#     Ogrnip = Input("//input[@id='ogrnip']")
#
#     AddIENextStep = Button("//button[@id='next']")
#
#     @staticmethod
#     def FillFormIE(clearall=False, PsoInn="616302768970", Ogrnip="310619504300040"):
#         if clearall:
#             EsiaOrganizationPage.PsoInn.clear()
#             EsiaOrganizationPage.Ogrnip.clear_backspace()
#         else:
#             EsiaOrganizationPage.PsoInn.send_keys_by_xpath(PsoInn)
#             EsiaOrganizationPage.Ogrnip.send_keys_by_xpath(Ogrnip)
#
#     CanselIE = Button("//a[contains (text(), 'Отменить')]")
#
#     GoToProfile = Button("//button[@id='authn']")
#
#     @staticmethod
#     def ChooseIE(ie):  # ИП
#         Button("//tbody[@id='empsTbl_data']//a[contains(., '" + ie + "')]").click()
#
#     RemoteIE = Button("//button[contains (., 'Удалить ИП')]")
#
#
#
#     # ЮЛ
#
#     # Общие данные
#
#     OrgOrg = Button("//div[@id='orgProfileTab']//a[contains(., 'Общие данные')]")
#
#     # ОПФ организации
#
#     @staticmethod
#     def ChooseEmpFromTableLK(emp=u"Эксплуатация ИЭП"):
#         Button("//a[contains(., '" + emp + "')]").click()
#
#     LegalFormGrp = Button("//a//span[@id='legalFormGrp']")  # ОПФ
#     LegalForm = Button("//label[@id='legalForm_label']")  # ОПФ список
#     EditLegalForm = Button("//form[@id='altLegalFormFm']//button[contains(., 'Изменить')]")
#
#     @staticmethod
#     def ChooseLegalForm(lf=u"Общественные движения"):  # Адвокатские бюро
#         Button("//div[@id='legalForm_panel']//ul/li[contains(., '" + lf + "')]").click()
#
#
#     # Данные об органе государственной власти
#
#     # Тип органа
#
#     AgencyTypeGrp = Button("//a[span/@id='agencyTypeGrp']")
#     AgencyType = Button("//label[@id='agencyType_label']")  # ОПФ список
#     EditAgencyType = Button("//form[@id='altAgencyTypeFm']//button[contains(., 'Изменить')]")
#
#     @staticmethod
#     def ChooseAgencyType(at=u"Орган исполнительной власти субъекта РФ"):  # Орган исполнительной власти субъекта РФ
#         Button("//div[@id='agencyType_panel']//ul/li[contains(., '" + at + "')]").click()
#
#     # Территориальная принадлежность
#
#     TerritoryGrp = Button("//a[span/@id='rfTerritoryGrp']")
#     Territory = Button("//label[@id='rfTerritory_label']")  # ОПФ список
#     EditTerritoryGrp = Button("//form[@id='altRfTerritoryFm']//button[contains(., 'Изменить')]")
#
#     @staticmethod
#     def ChooseTerritory(at=u"02 Республика Башкортостан"):  # Орган исполнительной власти субъекта РФ
#         Button("//div[@id='rfTerritory_panel']//ul/li[contains(., '" + at + "')]").click()
#
#     # ОКТМО
#
#     OktmoGrp = Button("//a//span[@id='rfOktmoGrp']")
#     Oktmo = Input("//input[@id='rfOktmo']")  # 20701000
#     TextOktmoGrp = Text("//span[@id='rfOktmoMsg']")
#     EditOktmoGrp = Button("//form[@id='altRfOktmoFm']//button[contains(., 'Изменить')]")
#
#     # Транспортные средства
#
#     AddVehicles = Button(
#         "//div[@id='orgProfileTab']//button[contains(., 'Добавить транспортное средство')]")  # Добавить транспортное средство
#
#     Vehicle = Button("//div[@id='vehicles_content']//a")  # первое ТС
#
#     VhlName = Input("//input[@id='vhlName']")  # Название ТС
#     NumberPlate = Input("//input[@id='numberPlate']")  # Государственный регистрационный знак
#     RegCertSerNum = Input("//input[@id='regCertSerNum']")  # Серия и номер свидетельства о регистрации
#
#     AddVhl = Button("//div[@id='vehicleData']//button[contains(., 'Добавить')]")  # изменить
#     ChangeVhl = Button("//div[@id='vehicleData']//button[contains(., 'Изменить')]")  # изменить
#     CancelVhl = Button("//div[@id='vehicleData']//button[contains(., 'Отмена')]")  # изменить
#     DeleteVhl = Button("//div[@id='vehicleData']//button[contains(., 'Удалить')]")  # изменить
#
#     @staticmethod
#     def FillFormVehicle(clearall=False, vhlname=u"Верблюд", numberplate=u"TT123T45", regcertsernum="1234 567890"):
#         if clearall:
#             EsiaOrganizationPage.VhlName.clear()
#             EsiaOrganizationPage.NumberPlate.clear()
#             EsiaOrganizationPage.RegCertSerNum.clear_backspace()
#         else:
#             EsiaOrganizationPage.VhlName.send_keys_by_xpath(vhlname)
#             EsiaOrganizationPage.NumberPlate.send_keys_by_xpath(numberplate)
#             EsiaOrganizationPage.RegCertSerNum.send_keys_by_xpath(regcertsernum)
#
#     @staticmethod
#     def DeleteVehicle():
#         EsiaOrganizationPage.Vehicle.click(method="classic")
#         EsiaOrganizationPage.DeleteVhl.click(method="classic")
#
#     # Контактная информация
#     # Почтовый адрес
#
#     PostAddrGrp = Button("//a//span[@id='postAddrGrp']")
#
#     Address = Input("//textarea[@id='address']")  # Адрес
#     House = Input("//input[@id='house']")  # Дом
#     Frame = Input("//input[@id='frame']")  # Корпус
#     Build = Input("//input[@id='build']")  # Строение
#     Flat = Input("//input[@id='flat']")  # Квартира
#     ZipCode = Input("//input[@id='zipCode']")  # Индекс
#
#     EditPostAddrGrp = Button("//form[@id='altAddrFrm']//button[contains(., 'Изменить')]")
#     DelPostAddrGrp = Button("//form[@id='altAddrFrm']//button[contains(., 'Удалить')]")
#
#     @staticmethod
#     def FillFormRegAddress(clearall=False, sitystreet=u"Липецкая область", house="1", frame="2", build="3", flat="4",
#                            streetadnzip=u"Липецкая область, Липецк город, Липецкая улица"):
#         if clearall:
#             EsiaOrganizationPage.Address.clear()
#             EsiaOrganizationPage.House.clear()
#             EsiaOrganizationPage.Frame.clear()
#             EsiaOrganizationPage.Build.clear()
#             EsiaOrganizationPage.Flat.clear()
#             EsiaOrganizationPage.ZipCode.clear_backspace()
#         else:
#             EsiaOrganizationPage.Address.send_keys_by_xpath_delay(sitystreet)
#             EsiaOrganizationPage.ChooseOptionFromListFIAS(streetadnzip)
#             EsiaOrganizationPage.House.send_keys_by_xpath(house)
#             EsiaOrganizationPage.Frame.send_keys_by_xpath(frame)
#             EsiaOrganizationPage.Build.send_keys_by_xpath(build)
#             EsiaOrganizationPage.Flat.send_keys_by_xpath(flat)
#
#     @staticmethod
#     def ChooseOptionFromListFIAS(streetadnzip):
#         Button("//div//ul//li[contains(., '" + streetadnzip + "')]").click()
#
#     # Адрес электронной почты
#
#     EmailGrp = Button("//a//span[@id='emailGrp']")
#
#     # Email = Input("//input[@id='email']")  # Индекс
#
#     EditEmail = Button("//form[@id='altEmailFm']//button[contains(., 'Изменить')]")
#     DelEmail = Button("//form[@id='altEmailFm']//button[contains(., 'Удалить')]")
#
#     # Телефон
#
#     PhoneGrp = Button("//a//span[@id='phoneGrp']")
#
#     Phone = Input("//input[@id='phone']")
#
#     EditPhone = Button("//form[@id='altPhoneFrm']//button[contains(., 'Изменить')]")
#     DelPhone = Button("//form[@id='altPhoneFrm']//button[contains(., 'Удалить')]")
#
#     # Факс
#
#     FaxGrp = Button("//a//span[@id='faxGrp']")
#
#     Fax = Input("//input[@id='fax']")
#
#     EditFax = Button("//form[@id='altFaxFrm']//button[contains(., 'Изменить')]")
#     DelFax = Button("//form[@id='altFaxFrm']//button[contains(., 'Удалить')]")
#
#     # Филиалы
#
#     AddBhr = Button("//div[@id='orgProfileTab']//button[contains(., 'Добавить филиал')]")
#
#     ####### Сотрудники
#
#     OrgEmps = Button("//div[@id='orgProfileTab']//ul//a[contains(., 'Сотрудники')]")
#
#     InviteEmp = Button("//form[@id='org']//button[contains(., 'Пригласить нового участника')]")
#
#     CheckAdmin = Button(
#         "//form[@id='inviteEmpFrm']//div[@id='authority']//span[contains(., 'Администраторы профиля организации')]")
#
#     Invate = Button("//form[@id='inviteEmpFrm']//button[contains(., 'Пригласить')]")
#
#     @classmethod
#     def FillFormInviteEmp(cls, clearall=False, email="esia_test_emp@mailinator.com", lastname=u"Толстой", firstname=u"Лев",
#                           midlename=u"Николаевич", snils="000-000-000 61"):
#         if clearall:
#             cls.Email.clear()
#             cls.LastName.clear()
#             cls.FirstName.clear()
#             cls.Midlename.clear()
#             cls.Snils.clear()
#         else:
#             cls.Email.send_keys_by_xpath(email)
#             cls.LastName.send_keys_by_xpath(lastname)
#             cls.FirstName.send_keys_by_xpath(firstname)
#             cls.Midlename.send_keys_by_xpath(midlename)
#             cls.Snils.send_keys_by_xpath(snils)
#             EsiaOrganizationPage.CheckAdmin.click()
#
#     GlobalFilterEmps = Input("//input[@id='globalFilter']")  # поиск
#     SearchBtn = Button("//button[@id='searchBtn']")  # "найти"
#
#     FirstEmp = Text("//div[@class='emp-line']//span[@class='name']")
#
#     @staticmethod  # Редактирование Толстой Лев Николаевич
#     def EditEmpFromTable(emp):
#         Button(
#             "//tbody[@id='empsTable_data']//tr[contains(., '" + emp + "')]//button[@title='Редактировать']").click(method="classic")
#
#     @staticmethod  # Исключить Толстой Лев Николаевич
#     def DeleteEmpFromTable(emp):
#         Button(
#             "//tbody[@id='empsTable_data']//tr[contains(., '" + emp + "')]//button[@title='Исключить']").click()
#
#     ExcludeEmp = Button("//form[@id='rmEmpFrm']//button[contains(., 'Исключить')]")
#     DisconnectFromOrg = Button("//button[contains(., 'Отсоединиться')]")
#     ContinueDisconnected = Button("//form[@id='dtchOrgFrm']//button[contains(., 'Продолжить')]")
#     Continueremove = Button("//form[@id='delOrgFrm']//button[contains(., 'Продолжить')]")
#
#     CorporatePhone = Input("//input[@id='altCorporatePhone']")
#     CorporateEmail = Input("//input[@id='altCorporateEmail']")
#     Pos = Input("//input[@id='altPos']")  # Должность
#
#     @staticmethod
#     def FillFormEmpInf(clearall=False, corporatephone="+7(4955)55-55-55*555555",
#                        corporateemail="esia_test_emp@mailinator.com", pos=u"Писатель"):
#         if clearall:
#             EsiaOrganizationPage.CorporatePhone.clear_backspace()
#             EsiaOrganizationPage.CorporateEmail.clear()
#             EsiaOrganizationPage.Pos.clear()
#         else:
#             EsiaOrganizationPage.CorporatePhone.send_keys_by_xpath(corporatephone)
#             EsiaOrganizationPage.CorporateEmail.send_keys_by_xpath(corporateemail)
#             EsiaOrganizationPage.Pos.send_keys_by_xpath(pos)
#
#     EditEmp = Button("//form[@id='altEmpFm']//button[contains(., 'Изменить')]")
#
#     BlkUnblkBtn = Button("//button[@id='blkUnblkBtn']")  # Блокировка\разблокировка
#
#     # Доступ к системам
#
#     OrgPerms = Button("//div[@id='orgProfileTab']//a[contains(., 'Доступ к системам')]")
#
#     @staticmethod  # Выбрать группу доступа
#     def ChooseGrpFromList(grp):
#         Button("//div[@id='grpList']//div[contains(., '" + grp + "')]/a").click()
#
#     JoinGroupStart = Button("//button[@id='joinGroupStart']")  # Добавить участников
#
#     AddMbrsInput = Input("//input[@id='addMbrs_input']")  # Поле для ФИО
#
#     AddMbrs = Button("//div[@id='addMbrsPn']//button[contains(., 'Добавить')]")  # Добавить
#
#     CloseMbrsForm = Button("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable dialog-grpDlg dialogEditGrp']//a[@href='#']")  # Закрыть форму
#
#     @staticmethod  # Выбрать сотрудника для включение в группу доступа
#     def ChooseEmpFromMbrsPanel(emp):
#         Button("//div[@id='addMbrs_panel']//li[contains(., '" + emp + "')]").click()
#
#     SearchBtn2 = Button("//button[@id='find']")  # "найти"
#
#     @staticmethod  # Исключить Толстой Лев Николаевич
#     def DeleteEmpFromGrpTable(emp):
#         Button(
#             "//tbody[@id='mbrsTbl_data']//tr[contains(., '" + emp + "')]//button[@title='Удалить']").click()
#
#     # История операций
#
#     OrgOpnHis = Button("//div[@id='orgProfileTab']//a[contains(., 'История операций')]")
#
#     FirstStringInOpn = Text("//span[@id='opnTbl:0:nameMsg']")
#
#     # Филиалы
#
#     @staticmethod  # Открыть филиал
#     def OpenBranch(emp):
#         Button("//div[@id='branches_content']//div[contains(., '" + emp + "')]//a[contains(., 'Перейти в профиль филиала')]").click()
#
#     BranchName = Button("//div[@id='orgProfileTab']//dl[contains(., 'Название')]//a")
#     BranchNameInput = Input("//textarea[@id='altBrhName']")
#     EditBranch = Button("//form[@id='altBrhNameDlgFm']//button[contains(., 'Изменить')]")  # Изменить
#
#
