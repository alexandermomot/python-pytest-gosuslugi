#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import logging
from assets.driver import DriverEngine
from lxml import etree
from io import StringIO


class BaseObject(object):

    """
        Main class wrapper, holds reference for all elements and pages to one driver
        at the runtime. Allows to control any browser and create elements for it, manipulate
        pages and driver itself. Also, such approach gives ability to hold control for only
        specified driver, if more than one is working @ background as grid
    """

    driver = DriverEngine
    """ :rtype: DriverEngine """

    def __init__(self, xpath):
        self.xpath = xpath
        try:  # let's check the syntax of xpath expression
            etree.parse(StringIO("<?xml version='1.0'?><x/>")).xpath(self.xpath)
        except etree.XPathEvalError as e:  # invalid xpath, throw an exception, skip the assignment
            logging.critical("Xpath incorrect: [%s]" % self.xpath)
            raise e

    def __add__(self, other):
        return self.xpath + other
