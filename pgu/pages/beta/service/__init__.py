#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from pgu.pages.beta.service.b_form import BaseFormPage
from pgu.pages.beta.service.b_info import BaseInfoPage
from pgu.pages.beta.service.b_reception import BaseReceptionPage
from pgu.pages.beta.service.b_claimtype import BaseClaimTypePage