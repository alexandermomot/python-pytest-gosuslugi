#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import os
from assets.utils.pathresolver import PathMap


class SystemVars:

    # System variables such as:
    # directories, system filenames and paths to binaries, stored here
    # Please, do not use it as storage for test data
    # This file must be in the root directory of the project

    # A bit tricky, returns platform-dependent paths.
    # Must return raw and unicoded string (for windows-cyrillic chars)
    # return as example on linux: ur"/home/user/git/py-at-framework-git"
    # or as example on windows: ur"C:\\Users\\user\\git\\py-at-framework-git"

    # Normalized paths to all system components

    ROOTDIR = os.getcwd()
    REPORTS_ROOT = ROOTDIR + "/html-reports"
    REPORT_FILE_NAME = "test_report.html"
    REPORT = None

    system = PathMap({
        "ROOTDIR": os.getcwd(),
        "REPORTS_HTML": ROOTDIR + "/html-reports/",
        "REPORTS_XML": ROOTDIR + "/test-reports/",
        "REPORT_FILE_PATH": REPORTS_ROOT + "/" + REPORT_FILE_NAME,
        "SCREENSHOT_ROOT": REPORTS_ROOT + "/screenshots/",
        "DATA_ROOT": ROOTDIR + "/pgu/data/",
        "SCRIPTS_ROOT": ROOTDIR + "/assets/driver/scripts/",
        "CASE_ROOT": ROOTDIR + "/pgu/suits/",
        "DRIVERS_PATH": ROOTDIR + "/driver/executable/",
        "SCREENSHOT_RELATIVE": "screenshots/",
        "HTML_REPORT_TEMPLATES": ROOTDIR + "/utils/reporter/html/"
    })

    # Testlink credentials

    testlink = {
      "TESTLINK_API_PYTHON_SERVER_URL": "http://testlink.rtlabs.ru/testlink/lib/api/xmlrpc/v1/xmlrpc.php",
      "TESTLINK_API_PYTHON_DEVKEY": "d75538761d0e0fe025def3739aa08d9c"
    }

    # Normalized paths to browser-dependent files

    webdriver = PathMap({

            #"FIREFOX_PROFILE": system["DATA_ROOT"] + "/profiles/firefox/testprofile.ff-v42",

            "CHROMEDRIVER_PATH": system["DRIVERS_PATH"] + "windows",

            "WINDOWS_CHROMEDRIVER_X86_PATH": system["DRIVERS_PATH"] + "windows/chromedriver_2.21.exe",
            "WINDOWS_OPERADRIVER_X86_PATH" : None,
            "WINDOWS_IEDRIVER_X86_PATH" : system["DRIVERS_PATH"] + "windows/IEDriverServer_x86_2.52.exe",
            "WINDOWS_IEDRIVER_X64_PATH" : system["DRIVERS_PATH"] + "windows/IEDriverServer_x64_2.52.exe",

            "LINUX_CHROMEDRIVER_X86_PATH" : system["DRIVERS_PATH"] + "linux/chromedriver_x86_2.21",
            "LINUX_CHROMEDRIVER_X64_PATH" : system["DRIVERS_PATH"] + "linux/chromedriver_x64_2.21",
            "LINUX_OPERADRIVER_X86_PATH" : None,
            "LINUX_OPERADRIVER_X64_PATH" : None,

            "DARWIN_CHROMEDRIVER_X86_PATH" : system["DRIVERS_PATH"] + "/mac/chromedriver_2.21",
            "DARWIN_OPERADRIVER_X86_PATH" : None,
            "DARWIN_SAFARIDRIVER_X86_PATH" : None,

            # "WINDOWS": {
            #         "X86": {
            #             "CHROMEDRIVER": system["DRIVERS_PATH"] + "/windows/chromedriver_2.19.exe",
            #             "OPERADRIVER": "asd",
            #             "IEDRIVER": "asd"
            #         },
            #         "X64": {
            #             "CHROMEDRIVER": system["DRIVERS_PATH"] + "/windows/chromedriver_2.19.exe",
            #             "OPERADRIVER": "asd",
            #             "IEDRIVER": "asd"
            #         }
            # },
            # "LINUX": {
            #         "X86": {
            #             "CHROMEDRIVER": system["DRIVERS_PATH"] + "/windows/chromedriver_2.19.exe",
            #             "OPERADRIVER": "asd"
            #         },
            #         "X64": {
            #             "CHROMEDRIVER": system["DRIVERS_PATH"] + "/windows/chromedriver_2.19.exe",
            #             "OPERADRIVER": "asd"
            #         }
            # },
            # "DARWIN": {
            #         "X86_64": {
            #             "CHROMEDRIVER": system["DRIVERS_PATH"] + "/windows/chromedriver_2.19.exe",
            #             "OPERADRIVER": "asd",
            #             "SAFARIDRIVER": None
            #         }
            # },

    })

