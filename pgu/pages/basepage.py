#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.driver.engine import DriverEngine


class BasePage(object):

    """
        Main class wrapper, holds reference for all pages of one driver
        at the runtime. Allows to control any browser and create elements for it, manipulate
        pages and driver itself. Also, such approach gives ability to hold control for only
        specified driver, if more than one is working @ background as grid
    """

    __url__ = None

    driver = DriverEngine
    """ :rtype: DriverEngine """

    # def __new__(cls, *args, **kwargs):
    #     print "Is attr: __url__ : " + str(hasattr(cls, "__url__"))
    #     if not hasattr(cls, "__url__"):
    #         raise Exception("Please, set __url__ attributes for all pageobjects")
    #     "foo" in cls.__dict__

    def __init__(self, url):
        self.__url__ = url

    @property
    def url(self):
        return self.__url__

    @url.setter
    def url(self, value):
        self.__url__ = value

