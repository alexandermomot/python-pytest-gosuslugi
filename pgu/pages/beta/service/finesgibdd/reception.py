#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button
from pgu.pages.beta.service import BaseReceptionPage, BaseClaimTypePage


class ReceptionPage(BaseReceptionPage, BaseClaimTypePage):

    BeginClaimBtn1 = Button("//h2/a[contains(.,'Заполните заявку')]")

    @classmethod
    def online_claim_type(cls):
        cls.BeginClaimBtn1.click()