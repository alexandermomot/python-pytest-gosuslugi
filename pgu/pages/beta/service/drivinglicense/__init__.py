#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from pgu.pages.beta.service.drivinglicense.application import ApplicationPage
from pgu.pages.beta.service.drivinglicense.reception import ReceptionPage
from pgu.pages.beta.service.drivinglicense.vars import Vars