#!/usr/bin/python
# coding=utf-8

__author__ = 'alexander-momot'

from assets.objects.elements import Button, Element, Text
from pgu.pages import BasePage

class CategoryPage(BasePage):

    __url__ = "/category"

    TopCategoriesLayer = Element("//div[@active='categories']")

    StructureBtn = Button(TopCategoriesLayer + "//div[@class='limiter']//a[contains(text(),'Органы власти')]")

    class Goto:

        def __init__(self):
            raise Exception("This is an action class, use it like this: Page.Goto.some_service_by_name('name')")

        ServiceName = "//div[@class='catalog-container']//div//a[contains(text(),'%s')]"

        @classmethod
        def service_by_name(cls, name):
            """
            :type name: str
            :return: None
            """
            Service = Button(cls.ServiceName % name)
            Service.click()
