#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import os
import pytest
import sys

if __name__ == "__main__":
    sys.dont_write_bytecode = True
    os.environ["VERIFY_BASE_URL"] = "TRUE"
    pytest.main()
