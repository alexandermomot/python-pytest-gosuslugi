#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from pgu.pages.beta.service.foreignpassport.reception import ReceptionPage
from pgu.pages.beta.service.foreignpassport.application import ApplicationPage
from pgu.pages.beta.service.foreignpassport.vars import Vars