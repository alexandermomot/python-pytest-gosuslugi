#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import time
from assets.driver.engine import DriverEngine
from pgu.tests.conftest import step, testcase, skipif, issue, feature, story, fail

from pgu.pages.esia import EsiaHomePage
from pgu.pages.beta.core import BetaHomePage, PersonalCabinetPage
from pgu.pages.beta.service.foreignpassport import ReceptionPage, ApplicationPage, Vars
from vars import SystemVars


@feature("Услуги")
@story("Получение загранпаспорта")
@issue("PGU-5")
@testcase("http://pgu-uat-beta.test.gosuslugi.ru/10070/4/form")
class ForeignPassportSanitySuite:
    """ Sanity-тестирование услуги "Получение заграничного паспорта" """

    @skipif(False, reason="Ошибка при открытии главной страницы портала ПГУ")
    def case_foreign_passport_positive(self, url: str, engine: DriverEngine, screenshot):

        with step("Заходим в авторизацию ЕСИА"):
            BetaHomePage.click_login_into_esia()

        with step("Выбираем авторизацию и вводим логин/пароль, выбираем физическое лицо если требуется"):
            if EsiaHomePage.page_error():
                fail("Esia login failed")
            EsiaHomePage.Login(Vars.esia_email, Vars.esia_pass).by_email_or_phone()

        with step("Выбираем форму услуги 'заграничный паспорт'"):
            BetaHomePage.Goto.foreign_passport()

        with step("Выбираем паспорт старого образца"):
            ReceptionPage.choose_old_type().choose_more_18_years_old()

        with step("Выбираем получение услуги, тип: онлайн заявка"):
            ReceptionPage.online_claim_type()

        with step("Кликаем на 'Новый черновик', если существует запрос"):
            ApplicationPage.create_new_draft()

        with step("Проверка появления главного локатора формы услуги, если не появилась - падение кейса"):
            if ApplicationPage.form_error():
                fail(ApplicationPage.form_error.ERROR_TEXT)

        with step("Проверка на невидимость дисклеймеров 'Заявка подана' и 'Ведомство недоступно', иначе - падение кейса"):
            if ApplicationPage.form_error_department():
                fail(ApplicationPage.form_error_department.ERROR_TEXT)

        with step("Вводим номер телефона"):
            ApplicationPage.Step1_PersonalPhone.is_visible(10)
            ApplicationPage.personal_data()

        with step("Вводим паспортные данные"):
            ApplicationPage.passport_data()

        with step("Выбор домашнего адреса"):
            ApplicationPage.home_address()

        with step("Проверка требований к фото"):
            ApplicationPage.Step4_PhotoRequirementsButton.click()
            assert ApplicationPage.Step4_PhotoRequirementsPopUp.is_exists()
            ApplicationPage.Step4_PhotoRequirementsCross.click()
            assert not ApplicationPage.Step4_PhotoRequirementsPopUp.is_exists()

        with step("Загружаем фото, делаем скриншот"):
            ApplicationPage.Step4_PhotoFileInput.upload_file(SystemVars.system.DATA_ROOT + "passport1.jpg")
            if ApplicationPage.form_photo_upload_error():
                fail(ApplicationPage.form_photo_upload_error.ERROR_TEXT)
            assert ApplicationPage.Step4_TextPhotoIsOk.is_visible(wait=15), "Не найден текст об удачной загрузке фото"

        with step("Выбираем чекбоксы изменения персональных данных и существования загранпаспорта"):
            ApplicationPage.Step5_IsPersonalDataWereNotChanged.click()
            ApplicationPage.Step6_IsForeignPassportExist.click()

        with step("Ввод данных о предыдущих местах работы и учёбы за последние 10 лет"):
            ApplicationPage.Step7_WorkingType.click()

            ApplicationPage.Step7_FromYearSelect.click()
            ApplicationPage.Step7_FromYearSelect.send_keys_by_xpath_delay("2001")

            ApplicationPage.Step7_ToYearSelect.click()
            ApplicationPage.Step7_ToYearSelect.send_keys_by_xpath_delay("2012")

            ApplicationPage.Step7_FromMonthSelect.click()
            ApplicationPage.Step7_FromMonthSelect.send_keys_by_xpath(u"Сентябрь")
            ApplicationPage.Step7_FromMonthList.click()

            ApplicationPage.Step7_ToMonthSelect.click()
            ApplicationPage.Step7_ToMonthSelect.send_keys_by_xpath(u"Октябрь")
            ApplicationPage.Step7_ToMonthList.click()
            ApplicationPage.Step7_IncludingCurrentTimeCheckBox.click()

            ApplicationPage.Step7_WorkPosition.clear_backspace()
            ApplicationPage.Step7_WorkPosition.send_keys_by_xpath(u"Грузчик")
            ApplicationPage.Step7_WorkCompany.clear_backspace()
            ApplicationPage.Step7_WorkCompany.send_keys_by_xpath(u"ЗАО Увеземвлес")
            ApplicationPage.Step7_WorkAddress.clear_backspace()
            ApplicationPage.Step7_WorkAddress.send_keys_by_xpath(u"125993, г. Москва, Волоколамское шоссе, д. 4")
            time.sleep(3)

        with step("Доступ к секретной информации и разрешение на выезд"):
            ApplicationPage.Step8_SecretDataAccessRadio.click()
            ApplicationPage.Step8_UnavailableTravelAbroadRadio.click()

        with step("Информация о детях"):
            ApplicationPage.Step9_NoChildDataRadio.click()

        with step("Центр выдачи документов, ввод адреса"):
            ApplicationPage.Step10_DocumentsApplyAddress.clear()
            ApplicationPage.Step10_DocumentsApplyAddress.send_keys_by_xpath_delay(u"Москва Хуторская ")
            ApplicationPage.Step10_DocumentsApplyAddress_Select.click()
            ApplicationPage.Step10_DocumentsApplyAddress_AdditionalStreet.send_keys_by_xpath_delay(u"Мирлэнд")
            ApplicationPage.Step10_DocumentsApplyAddress_House.send_keys_by_xpath("5")
            ApplicationPage.Step10_DocumentsApplyAddress_Apartment.send_keys_by_xpath("38")

        with step("Место на я.картах, выбор подразделения"):
            ApplicationPage.yamaps_setlocation()

        with step("Флажки 'согласен с условиями' и отправка формы"):
            ApplicationPage.StepFinal_CheckBoxApproveLaw1.click()
            ApplicationPage.StepFinal_CheckBoxApproveLaw2.click()
            screenshot("До отправки формы")
            ApplicationPage.StepFinal_SubmitFormButton.click()

        with step("Подтверждение отправки формы в ведомство"):
            PersonalCabinetPage.AcceptedHeader.is_visible(wait=30)
            assert PersonalCabinetPage.AcceptedHeader.is_exists(), "Не найден текст заголовка"
            assert PersonalCabinetPage.check_submittext_existence(), "Не найден текст статуса 'Поставлен в очередь на обработку'"
            engine.refresh()
            assert PersonalCabinetPage.check_submittext_existence(), "Не найден текст статуса 'Принято'"
