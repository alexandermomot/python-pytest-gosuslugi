#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button, Text
from pgu.pages.beta.service import BaseReceptionPage, BaseClaimTypePage


class ReceptionPage(BaseReceptionPage, BaseClaimTypePage):

    page = "Общие для всех предбанников объекты страницы услуги 'Получение СЭЗ'"

    sez_project_documentation = Button("//a[text()='Получение санитарно-эпидемиологического заключения на проектную документацию']")

    ZapolniteZayavku = Button("//a[@href='#' and contains(text(), 'Заполните заявку')]")
    PopUpNapomnitPozhe = Button("//a[contains(text(), 'Напомнить позже')]")
    PoluchitUsluguBeta = Button("//a[contains(text(), 'Получить услугу')]")
    PoluchitUsluguAlpha = Button("//div[contains(@class,'right')]//a[contains(@title, 'Получить услугу')]")
    PassOnbetaPortal = Button("//a[@name='btnLink' and text()='Перейти']")
    LoadingSpinner = Text("//div[@id='shadowWrap']") # Loader

    @classmethod
    def click_apply_claim_beta_portal(cls):
        cls.ZapolniteZayavku.is_exists()
        cls.ZapolniteZayavku.click()

    @classmethod
    def click_claim_service_alpha_portal(cls):
        if cls.PopUpNapomnitPozhe.is_exists():
            cls.PopUpNapomnitPozhe.click()
        cls.PoluchitUsluguAlpha.is_exists()
        cls.PoluchitUsluguAlpha.click()
        if cls.PassOnbetaPortal.is_visible(10):
            cls.PassOnbetaPortal.click()
