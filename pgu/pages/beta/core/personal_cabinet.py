#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

from assets.objects.elements import Button, Text
from pgu.pages import BasePage

class PersonalCabinetPage(BasePage):

    __url__ = "/"

    AcceptedHeader = Text("//div[@class='details-header']//h2[contains(text(), '')]")

    InProgressText = Text("//div[@class='status']//b[contains(text(), 'Поставлен в очередь на обработку')]")
    SendedText = Text("//div[@class='status']//b[contains(text(), 'Отправлено в ведомство')]")
    AcceptedText = Text("//div[@class='status']//b[contains(text(), 'Принято')]")
    DeclinedText = Text("//div[@class='status']//b[contains(text(), 'Ошибка отправки в ведомство')]")

    PersonalInfoBtn = Button("//div[@id='content-wrapper']//ul[@class='tabs']//li[contains(text(), 'Персональная информация')]")
    NotificationsBtn = Button("//div[@id='content-wrapper']//ul[@class='tabs']//li[contains(text(), 'Лента уведомлений')]")
    NotificationSettingsBtn = Button("//div[@id='content-wrapper']//ul[@class='tabs']//li[contains(text(), 'Настройка уведомлений')]")
    GepsBtn = Button("//div[@id='content-wrapper']//ul[@class='tabs']//li[contains(text(), 'Госпочта')]")

    @classmethod
    def check_submittext_existence(cls):
        final_text_exist = cls.InProgressText.is_exists() or cls.AcceptedText.is_exists() or cls.SendedText.is_exists() or cls.DeclinedText.is_exists()
        return final_text_exist

