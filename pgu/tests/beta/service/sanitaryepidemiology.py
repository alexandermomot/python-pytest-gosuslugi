#!/usr/bin/python
# coding=utf-8
__author__ = 'alexander-momot'

from assets.driver.engine import DriverEngine
from pgu.tests.conftest import step, testcase, skipif, issue, feature, story, fail

from pgu.pages.esia import EsiaHomePage
from pgu.pages.beta.core import BetaHomePage, CategoryPage, StructurePage, PersonalCabinetPage
from pgu.pages.beta.service.sez import ApplicationPage, ReceptionPage, Vars


@feature("Услуги")
@story("Получение СЭЗ")
@issue("PGU-7")
@testcase("http://pgu-uat-beta.test.gosuslugi.ru/10059/3/form")
class SanitaryEpidemiologySanitySuite:
    """ Sanity-тестирование услуги "Получение Санитарно-Эпидемиологического заключения (СЭЗ)" для юр. лица """

    @skipif(False, reason="Ошибка при открытии главной страницы портала ПГУ")
    def case_sanitary_epidemiology_positive(self, url: str, engine: DriverEngine, screenshot):

        with step("Переход на страницу ЕСИА"):
            BetaHomePage.click_login_into_esia()
            assert EsiaHomePage.__url__ in engine.current_url, "Page is not ESIA"

        with step("Авторизация в ЕСИА, вход по СНИЛС, как ИП"):
            if EsiaHomePage.page_error():
                fail("Esia login failed")
            # Николаев Н.Н.
            EsiaHomePage.Login(Vars.esia_snils_ip, Vars.esia_pass_ip).by_snils().as_individual_enterpreneuer()

        with step("Переход на страницу 'каталог услуг' белого портала, затем 'органы власти'"):
            # Прямой перепрыг на услугу
            BetaHomePage.CategoryTopBtn.click()
            assert CategoryPage.__url__ in engine.current_url
            CategoryPage.StructureBtn.click()
            assert StructurePage.__url__ in engine.current_url

        with step("Затем переход и поиск услуги в разделе 'органы власти'"):
            StructurePage.expand(2)
            StructurePage.FedSluzhbaVSferePravPotreb.click()
            StructurePage.SezService.click()
            ReceptionPage.sez_project_documentation.click()
            ReceptionPage.PoluchitUsluguBeta.click()

        with step("Заполнение поля 'новый черновик' если необходимо"):
            ApplicationPage.create_new_draft()

        with step("Проверка видимости и заполняемости формы"):
            if not ApplicationPage.FormContent_Body.is_visible(30):
                if ApplicationPage.form_error():
                    fail("Service form not found")

        with step("Заполнение персональной информации: email"):
            ApplicationPage.click_and_type_personal_email()

        with step("Заполнение персональной информации: домашний адрес"):
            ApplicationPage.click_and_type_postaddress()

        with step("Ввод персональных данных контактного лица"):
            ApplicationPage.set_personal_data_of_contact()

        with step("Выбор департамента роспотребнадзора"):
            ApplicationPage.click_and_type_rospotrebnadzor_department()

        with step("Заполнение информации об ИП"):
            ApplicationPage.click_and_type_ie_name()

        with step("Ввод юридического адреса ИП"):
            ApplicationPage.click_and_type_juridical_address()

        with step("Ввод информации о документах, передаваемых в ведомство"):
            ApplicationPage.click_and_type_project_documentation_list()

        with step("Отправка формы"):
            screenshot("Перед отправкой формы")
            ApplicationPage.click_submit_page()

        with step("Подтверждение отправки формы в ведомство"):
            assert PersonalCabinetPage.check_submittext_existence(), 'Не удалось найти текст об отправке формы'
            engine.refresh()
