#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import codecs

from assets.utils.timer import Timer
from assets.driver.storage import DriverStorage
from vars import SystemVars


class Geo:

    # @Singleton

    # testing url:
    # http://viralpatel.net/blogs/demo/js/html5-geolocation-example/html5-geolocation-google-map.html
    # http://ctrlq.org/maps/where/
    # http://html5demos.com/geo

    __latitude = None
    __longitude = None

    __script = SystemVars.system.SCRIPTS_ROOT + "geolocation.js"

    def __init__(self):
        pass

    def __str__(self):
        return None

    @classmethod
    def assign(cls, latitude = "55.7522200", longitude = "37.6155600"):
        # default location is Moscow
        cls.__latitude  = latitude
        cls.__longitude = longitude
        print(Timer.now() + "Location changed to [%s]" % (cls.__latitude + " " + cls.__longitude))
        DriverStorage["main"].webdriver.execute_script(cls.__get_coords())
        return

    @classmethod
    def set_location(cls, town="Moscow"):
        location = {
          # "town" : "Latitude Longitude"
            "Moscow": "55.7522200 37.6155600",
            "Krasnogorsk": "55.8203600 37.3301700",
            "New York": "40.7142700 -74.0059700",
            "Saint Petersburg": "59.9386300 30.3141300"
        }
        latlong = location[town].split()
        cls.__latitude  = latlong[0]
        cls.__longitude = latlong[1]
        return

    @classmethod
    def __read(cls, filepath) -> str:
        """
        :param filepath: path to the serializable js file to read
        :rtype: str
        """
        with codecs.open(filepath, 'r', encoding='utf-8') as text:
            return text.read()

    @classmethod
    def __get_coords(cls):
        jstext = cls.__read(cls.__script)
        return jstext\
            .replace("%latitude%", cls.__latitude, 1)\
            .replace("%longitude%", cls.__longitude, 1)
