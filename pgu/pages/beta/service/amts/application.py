#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import time, string, datetime, re

from pgu.pages.beta.service import BaseFormPage
from assets.objects.elements import Button, Input, Element


class ApplicationPage(BaseFormPage):

    PersonalPhone = Input("//input[@id='form.FormStep2.Panel4.Panel333.telPasEdit']")

    Step_HomeAddress = Input("//div[@id='form.FormStepAddress.Panel1.Panel11.registration_panel.Fias']//input[@class='PGU-FiasDropdown-Search-Input js-PGU-FiasDropdown-Search-Input']")
    Step_HomeAddress_AdditionalStreet = Input("//div[@id='form.FormStepAddress.Panel1.Panel11.registration_panel.Fias']//div[@class='row-Fias row-Fias-additionalStreet']//input")
    Step_HomeAddress_House = Input("//div[@id='form.FormStepAddress.Panel1.Panel11.registration_panel.Fias']//div[@class='row-Fias row-Fias-house']//input")
    Step_HomeAddress_Apartment = Input("//div[@id='form.FormStepAddress.Panel1.Panel11.registration_panel.Fias']//div[@class='row-Fias row-Fias-apartment']//input")

    ObtainLicensePlateYes = Button("//div[@id='form.FormStep4.Panel20.Yes']//a")
    ObtainLicensePlateNo = Button("//div[@id='form.FormStep4.Panel20.No']//a")

    PTS_exist = Button("//div[@id='form.FormStep5.Panel21.passportTC']//span[contains(text(), 'Получить новый')]/parent::a")
    PTS_renew = Button("//div[@id='form.FormStep5.Panel21.passportTC']//span[contains(text(), 'Внести изменения в действующий')]/parent::a")

    step7 = Element("//div[@id='form.FormStep7']")
    step7_category = Element(step7 + "//div[@id='form.FormStep7.Panel25.Panel26.category']")

    Step_VehicleDescriptionVIN = Input("//div[@id='form.FormStep8']//input[@id='form.FormStep8.Panel27.Panel28.vin']")
    Step_VehicleDescriptionVINRepeat = Input("//div[@id='form.FormStep8']//input[@id='form.FormStep8.Panel27.Panel4488.checkVin']")
    Step_VehicleDescriptionChassis = Input("//div[@id='form.FormStep8']//input[@id='form.FormStep8.Panel27.Panel29.frameSerial']")
    Step_VehicleDescriptionBody = Input("//div[@id='form.FormStep8']//input[@id='form.FormStep8.Panel27.Panel30.bodySerial']")

    Step_VehicleDescriptionCarBrand = Input("//input[@id='form.FormStep9.Panel31.Panel32.mark']")
    Step_VehicleDescriptionCarModel = Input("//input[@id='form.FormStep9.Panel31.Panel33.model']")
    Step_VehicleDescriptionIssueDateInput = Input("//input[@id='form.FormStep9.Panel31.Panel34.productionYear']")
    Step_VehicleDescriptionIssueDateSelect = Button("//input[@id='form.FormStep9.Panel31.Panel34.productionYear']/following-sibling::span[@class='PGU-FieldYear-Controls']")

    Step_GAIAddress = Input("//div[@id='form.FormStep13.personType_owner.chooseAddress2_panel.Fias']//input[@class='PGU-FiasDropdown-Search-Input js-PGU-FiasDropdown-Search-Input']")
    Step_GAIAddress_AdditionalStreet = Input("//div[@id='form.FormStep13.personType_owner.chooseAddress2_panel.Fias']//div[@class='row-Fias row-Fias-additionalStreet']//input")
    Step_GAIAddress_House = Input("//div[@id='form.FormStep13.personType_owner.chooseAddress2_panel.Fias']//div[@class='row-Fias row-Fias-house']//input")
    Step_GAIAddress_Apartment = Input("//div[@id='form.FormStep13.personType_owner.chooseAddress2_panel.Fias']//div[@class='row-Fias row-Fias-apartment']//input")

    Step_YaMaps_Search = Input("//div[@id='form.FormStep13.chooseDepartment.department']//input[@class='PGU-FieldTextInputBasic field-bordered field-padded']")
    Step_YaMaps_ChooseDepartment = Button("//ymaps//span[contains(text(),'Выбрать')]/ancestor::a")
    Step_DateOfVisit = Input("(//div[@id='form.FormStep13']//div[contains(@id, 'AppointmentDate')]//input)[1]")
    Step_TimeOfVisit = Button("//div[@id='form.FormStep13']//div[contains(@id, 'AppointmentDateSlots')]//div[@class='PGU-ComboBoxFace']")

    Step_DisclaimerAgree = Button("//div[@id='form.FormStep14.Panel59.FieldCheckboxMale']//a")
    Step_SubmitForm = Button("//a[@id='form.FormStep14.Panel59.sendApplication']/span[@class=contains(text(), 'Отправить')]")

    @classmethod
    def transport_type(cls, radiobutton = 1):
        TransportTypeSelect = Button("(//div[@id='form.FormStep0.Panel1.Panel11.registrationType']//span/parent::a)[%s]" % radiobutton)
        TransportTypeSelect.click()

    @classmethod
    def property_type(cls, radiobutton = 1):
        PropertyTypeSelect = Button("(//div[@id='form.FormStep1.Panel1.personType']//span/parent::a)[%s]" % radiobutton)
        PropertyTypeSelect.click()

    @classmethod
    def personal_data(cls, phonenum="70001234567"):
        cls.PersonalPhone.send_keys_by_xpath_delay(phonenum, 0.2)
        i = 0
        while re.sub("[^0-9]", "", cls.PersonalPhone.get_value()) != phonenum and i < 5:
            cls.PersonalPhone.get_value()
            cls.PersonalPhone.clear_backspace()
            cls.PersonalPhone.send_keys_by_xpath_delay(phonenum, 0.2)
            i += 1

    @classmethod
    def passport_data(cls):
        pass

    @classmethod
    # searchinput
    def home_address(cls, radiobutton = 1, address = "Москва Хуторская ", selectinput_element = 1):
        _formId = "//div[@id='form.FormStepAddress.Panel1.Panel11']"
        HomeAddressType = Button("(//div[@id='form.FormStepAddress.Panel1.Panel11']//div[contains(@id, 'form.FormStepAddress') and not(contains(@class, 'PGU-Control-Hidden'))]//input[@type='radio']/following-sibling::span/parent::a)[%s]" % radiobutton)
        HomeAddressType.click()
        cls.Step_HomeAddress.send_keys_by_xpath_delay(address)
        Step_HomeAddress_Select = Button("(//div[@id='form.FormStepAddress.Panel1.Panel11.registration_panel.Fias']//div[@class='PGU-Fias-Option-Wrap'])[" + str(selectinput_element) +"]")
        Step_HomeAddress_Select.click()
        cls.Step_HomeAddress_AdditionalStreet.send_keys_by_xpath_delay("Мирлэнд")
        cls.Step_HomeAddress_House.send_keys_by_xpath_delay("38")
        cls.Step_HomeAddress_Apartment.send_keys_by_xpath_delay("5")


    @classmethod
    def obtain_state_licenseplate(cls, obtain = True):
        if obtain:
            cls.ObtainLicensePlateYes.click()
        if not obtain:
            cls.ObtainLicensePlateNo.click()

    @classmethod
    def new_passport_of_vehicle_required(cls, renew = False):
        if renew:
            cls.PTS_renew.click()
        if not renew:
            cls.PTS_exist.click()

    @classmethod
    def vehicle_category_type(cls, select="B"):
        Button(cls.step7_category+"//div[@class='PGU-ComboBoxFace']").click()
        Button(cls.step7_category+"//div[contains(@class,'PGU-ComboBoxOption') and @data-value='%s']" % select).click()

    @classmethod
    def vehicle_description(cls, vin_num="15649815615456746845631321634", chassis_num="45964984156156156465465468", body_num=""):
        cls.Step_VehicleDescriptionVIN.send_keys_by_xpath(vin_num)
        cls.Step_VehicleDescriptionVINRepeat.send_keys_by_xpath(vin_num)
        cls.Step_VehicleDescriptionChassis.send_keys_by_xpath(chassis_num)
        cls.Step_VehicleDescriptionBody.send_keys_by_xpath(body_num)

    @classmethod
    def vehicle_data(cls, car_brand="Toyota", car_model="Corolla", issue_date=2005):
        cls.Step_VehicleDescriptionCarBrand.send_keys_by_xpath(car_brand)
        cls.Step_VehicleDescriptionCarModel.send_keys_by_xpath(car_model)
        cls.Step_VehicleDescriptionIssueDateInput.send_keys_by_xpath(issue_date)
        # Step_VehicleDescriptionIssueDateListItem = \
        #     Button("//div[@id='form.FormStep9.Panel31.Panel34.productionYear']//div[@class='PGU-ComboBoxOption' and text()='%s']" % issue_date)
        # Step_VehicleDescriptionIssueDateListItem.click()


    @classmethod
    def DocumentPassportVehicle(cls):
        # pass
        pass

    @classmethod
    def DocumentPropertyOfVehicle(cls):
        # pass
        pass

    @classmethod
    def DocumentInsurance(cls):
        # pass
        pass

    @classmethod
    def location_of_registration(cls, selectinput_element = 2):
        Step_ChooseAnotherAddress = Button("//input[@id='form.FormStep13.personType_owner.chooseAddress2']/parent::a")
        Step_ChooseAnotherAddress.click()
        cls.Step_GAIAddress.send_keys_by_xpath("Москва Хуторская  ")
        Step_DrivingLicenseIssueAddress_Select = Button("(//div[@id='form.FormStep13']//div[@class='PGU-Fias-Option-Wrap'])[" + str(selectinput_element) +"]")
        Step_DrivingLicenseIssueAddress_Select.click()
        cls.Step_GAIAddress_AdditionalStreet.send_keys_by_xpath("Мирлэнд")
        cls.Step_GAIAddress_Apartment.send_keys_by_xpath("5")
        cls.Step_GAIAddress_House.send_keys_by_xpath("38")

    @classmethod
    def date_of_visit(cls, time_visit = "15:00"):
        date_select = (datetime.date.today() + datetime.timedelta(days=2)).strftime("%d%m%Y") # today + 1 day
        cls.Step_DateOfVisit.send_keys_by_xpath(date_select)
        if time_visit: # if pointed as some time, choose that time particularly, else, choose first time available
            Step_TimeOfVisitSelect = Button("//div[@id='form.FormStep13']//div[contains(@id, 'AppointmentDateSlots')]//div[contains(text(), '" + time_visit +"')]")
        else:
            Step_TimeOfVisitSelect = Button("(//div[@id='form.FormStep13']//div[contains(@id, 'AppointmentDateSlots')]//div[@class='PGU-ComboBoxOption'])[position()>1]")
        cls.Step_TimeOfVisit.click()
        Step_TimeOfVisitSelect.click()

    @classmethod
    def click_agree_and_submit_form(cls):
        cls.Step_DisclaimerAgree.click()
        cls.Step_SubmitForm.click()
        time.sleep(5)

    @classmethod
    def formstep(cls, num, name):
        pass