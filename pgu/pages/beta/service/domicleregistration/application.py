#!/usr/bin/python
# coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'alexander-momot'

import re

from assets.objects.elements import Button, Input, Text
from pgu.pages.beta.service import BaseFormPage


class ApplicationPage(BaseFormPage):

    Step0_ClaimExistsHeader = Text("//div[contains(@id,'form.FormStep0')]//h4[contains(text(), 'Внимание')]")
    Step0_ClaimExistsMessage = Text("//div[contains(@id,'form.FormStep0')]//div[contains(text()[2],'Дело уже существует')]")

    Step1_SelfOver18 = Button("//div[@id='form.FormStep1.Panel1.change1']//div[contains(@id,'SUBJECT1')]")

    Step2_Phone = Input("//input[@id='form.FormStep2.Panel1.Panel17.TELedit']")

    Step3_PassportData_Country_ClickZone = Button("//div[@id='form.FormStep3.Panel1.Panel16.Panel11.BIRTHCOUNTRY']/div[@class='PGU-ComboBox PGU-FieldBody search PGU-context-search_show']")
    Step3_PassportData_Country_InputZone = Input("//div[@id='form.FormStep3.Panel1.Panel16.Panel11.BIRTHCOUNTRY']//input[@name='search_query' and @type='text']")
    Step3_PassportData_Country_ListElement = Button("//div[@id='form.FormStep3.Panel1.Panel16.Panel11.BIRTHCOUNTRY']//div[contains(@class,'PGU-ComboBoxOption') and @data-value!='']")
    Step3_RegionInput = Input("//div[@id='form.FormStep3']//input[contains(@id,'BIRTHREGION')]")
    Step3_DisctrictInput = Input("//div[@id='form.FormStep3']//input[contains(@id,'BIRTHDISTRICT')]")
    Step3_BirthCityInput = Input("//div[@id='form.FormStep3']//input[contains(@id,'BIRTHCITY')]")

    Step4_OwnHouseRegistration = Button("//div[@id='form.FormStep6']//input[contains(@id,'HASREGISTRATION2')]/..")

    Step5_NewRegistrationAddress = Input("//div[@id='form.FormStep7']//input[@class='PGU-FiasDropdown-Search-Input js-PGU-FiasDropdown-Search-Input']")
    Step5_NewRegistrationAddress_Select = Button("(//div[@id='form.FormStep7']//div[@class='js-PGU-Fias-Option PGU-Fias-Option'])[2]")
    Step5_NewRegistrationAddress_AdditionalStreet = Input("//div[@id='form.FormStep7']//div[@class='row-Fias row-Fias-additionalStreet']//input")
    Step5_NewRegistrationAddress_House = Input("//div[@id='form.FormStep7']//div[@class='row-Fias row-Fias-house']//input")
    Step5_NewRegistrationAddress_Apartment = Input("//div[@id='form.FormStep7']//div[@class='row-Fias row-Fias-apartment']//input")
    Step5_NewRegistrationAddress_Index = Input("//div[@id='form.FormStep7']//div[contains(@class, 'row-Fias row-Fias-post_index')]//input")

    StepX_PassportReplace_Series = Input("//div[@id='form.FormStep4']//input[contains(@id,'PASP_SERIE')]")
    StepX_PassportReplace_Number = Input("//div[@id='form.FormStep4']//input[contains(@id,'PASP_NUMBER')]")
    StepX_PassportReplace_IssueDate = Input("//div[@id='form.FormStep4']//input[contains(@id,'PASP_DATE')]")
    StepX_PassportReplace_DeptCode = Input("//div[@id='form.FormStep4']//input[contains(@id,'PASP_CODE')]")
    StepX_PassportReplace_Issuer = Input("//div[@id='form.FormStep4']//input[contains(@id,'PASP_BY')]")

    Step9_LegalBasisOfLiving_Select = Button("//div[@id='form.FormStep11.Panel2']//div[@class='PGU-ComboBoxFace']")
    Step9_LegalBasisOfLiving_Option = Button("//div[@id='form.FormStep11.Panel2']//div[@class='PGU-ComboBoxOptions']//div[contains(@class, 'PGU-ComboBoxOption')][2]")
    Step9_LegalBasisOfLiving_Series = Input("(//div[@id='form.FormStep11']//input[contains(@id,'LDOC_SERIE')])[2]")
    Step9_LegalBasisOfLiving_Number = Input("(//div[@id='form.FormStep11']//input[contains(@id,'LDOC_NUMBER')])[2]")
    Step9_LegalBasisOfLiving_IssueDate = Input("(//div[@id='form.FormStep11']//input[contains(@id,'LDOC_DATE')])[2]")
    Step9_LegalBasisOfLiving_Issuer = Input("(//div[@id='form.FormStep11']//input[contains(@id,'LDOC_BY')])[2]")

    Step11_ReasonOfChange = Button("//div[@id='form.FormStep13']//div[contains(@id,'STATREASON')]//div[@class='PGU-ComboBoxFace']")
    Step11_ReasonOfChangeList = Button("//div[@id='form.FormStep13']//div[contains(@id,'STATREASON')]//div[contains(@class, 'PGU-ComboBoxOption') and @data-value != '']")
    Step11_CarrerTypeOfJob = Button("//div[@id='form.FormStep13']//div[@id='form.FormStep13.Panel2.STATBIZ']//div[@class='PGU-ComboBoxFace']")
    Step11_CarrerTypeList = Button("//div[@id='form.FormStep13']//div[@id='form.FormStep13.Panel2.STATBIZ']//div[contains(@class, 'PGU-ComboBoxOption') and @data-value != '']")
    Step11_TypeOfJob = Button("//div[@id='form.FormStep13']//div[contains(@id,'STATBIZSTATUS')]//div[@class='PGU-ComboBoxFace']")
    Step11_TypeOfJobList = Button("//div[@id='form.FormStep13']//div[contains(@id,'STATBIZSTATUS')]//div[contains(@class, 'PGU-ComboBoxOption') and @data-value != '']")
    Step11_Socialinsurance = Button("//div[@id='form.FormStep13']//div[contains(@id,'STATSOCIALPROFIT')]//div[@class='PGU-ComboBoxFace']")
    Step11_SocialinsuranceList = Button("//div[@id='form.FormStep13']//div[contains(@id,'STATSOCIALPROFIT')]//div[contains(@class, 'PGU-ComboBoxOption') and @data-value != '']")
    Step11_EducationLevel = Button("//div[@id='form.FormStep13']//div[contains(@id,'STATEDUCATION')]//div[@class='PGU-ComboBoxFace']")
    Step11_EducationLevelList = Button("//div[@id='form.FormStep13']//div[contains(@id,'STATEDUCATION')]//div[contains(@class, 'PGU-ComboBoxOption') and @data-value != '']")
    Step11_MarriedState = Button("//div[@id='form.FormStep13']//div[contains(@id,'STATMARSTATUS')]//div[@class='PGU-ComboBoxFace']")
    Step11_MarriedStateList = Button("//div[@id='form.FormStep13']//div[contains(@id,'STATMARSTATUS')]//div[contains(@class, 'PGU-ComboBoxOption') and @data-value != '']")

    Step13_DocumentsApplyAddress = Input("//div[@id='form.FormStep15']//input[@class='PGU-FiasDropdown-Search-Input js-PGU-FiasDropdown-Search-Input']")
    Step13_DocumentsApplyAddress_Select = Button("//div[@id='form.FormStep15']//div[@class='js-PGU-Fias-Option PGU-Fias-Option']")
    Step13_DocumentsApplyAddress_AdditionalStreet = Input("//div[@id='form.FormStep15']//div[@class='row-Fias row-Fias-additionalStreet']//input")
    Step13_DocumentsApplyAddress_House = Input("//div[@id='form.FormStep15']//div[@class='row-Fias row-Fias-house']//input")
    Step13_DocumentsApplyAddress_Apartment = Input("//div[@id='form.FormStep15']//div[@class='row-Fias row-Fias-apartment']//input")

    StepFinal_CheckBoxApproveLaw1 = Button("//div[@id='form.FormStep16']//div[contains(@id,'FieldCheckbox1')]/a")
    StepFinal_CheckBoxApproveLaw2 = Button("//div[@id='form.FormStep16']//div[contains(@id,'FieldCheckbox2')]/a")
    StepFinal_SubmitFormButton = Button("//div[@id='form.FormStep16']//a[contains(@id,'ButtonFinal')]")

    @classmethod
    def personal_data(cls, phonenum="70001234567"):
        cls.Step2_Phone.send_keys_by_xpath_delay(phonenum, 0.2)
        i = 0
        while re.sub("[^0-9]", "", cls.Step2_Phone.get_value()) != phonenum and i < 5:
            cls.Step2_Phone.get_value()
            cls.Step2_Phone.clear_backspace()
            cls.Step2_Phone.send_keys_by_xpath_delay(phonenum, 0.2)
            i += 1

    @classmethod
    def check_disclaimer_claim_exists(cls):
        if cls.Step0_ClaimExistsHeader.is_exists() or cls.Step0_ClaimExistsHeader.is_exists():
            pass

